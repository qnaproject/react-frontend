/* config-overrides.js */
const path = require("path");
module.exports = function override(config, env) {
  //do stuff with the webpack config...
  config.resolve = {
    alias: {
      "@ant-design/icons/lib/dist$": path.resolve(__dirname, "./src/icons.js")
    },
    extensions: [".jsx", ".js"]
  };
  return config;
};
