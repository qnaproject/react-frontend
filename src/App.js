import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import Home from "./components/Home";
import Post from "./components/Post";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import PublicProfile from "./components/PublicProfile";
import Profile from "./components/Profile";
import VerifyEmail from "./components/VerifyEmail";
import ResetForgottenPassword from "./components/ResetForgottenPassword";
import Article from "./components/Article";
import Cookie from "./components/misc/Cookie";
import Terms from "./components/misc/TermsAndConditions";
import Privacy from "./components/misc/PrivacyPolicy";
import Contact from "./components/misc/Contact";
import { LastLocationProvider } from "react-router-last-location";
import GoogleAnalytics from "./components/misc/GoogleAnalytics";
import P404 from "./components/misc/P404";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <LastLocationProvider>
          <Switch>
            <Route exact path="/" component={Home} />

            <Route
              exact
              path="/art"
              render={props => <Home category="art" />}
            />
            <Route
              exact
              path="/economy"
              render={props => <Home category="economy" />}
            />
            <Route
              exact
              path="/education"
              render={props => <Home category="education" />}
            />
            <Route
              exact
              path="/entertainment"
              render={props => <Home category="entertainment" />}
            />
            <Route
              exact
              path="/fiction"
              render={props => <Home category="fiction" />}
            />
            <Route
              exact
              path="/food"
              render={props => <Home category="food" />}
            />
            <Route
              exact
              path="/health"
              render={props => <Home category="health" />}
            />
            <Route
              exact
              path="/humanity"
              render={props => <Home category="humanity" />}
            />
            <Route
              exact
              path="/nature"
              render={props => <Home category="nature" />}
            />
            <Route
              exact
              path="/news"
              render={props => <Home category="news" />}
            />
            <Route
              exact
              path="/pets"
              render={props => <Home category="pets" />}
            />
            <Route
              exact
              path="/relationships"
              render={props => <Home category="relationships" />}
            />
            <Route
              exact
              path="/science"
              render={props => <Home category="science" />}
            />
            <Route
              exact
              path="/software"
              render={props => <Home category="software" />}
            />
            <Route
              exact
              path="/sports"
              render={props => <Home category="sports" />}
            />
            <Route
              exact
              path="/technology"
              render={props => <Home category="technology" />}
            />
            <Route
              exact
              path="/travel"
              render={props => <Home category="travel" />}
            />

            <Route path="/post/:postId/:postTitle" exact component={Post} />
            <Route
              path="/post/:postId/:postTitle/:answerId"
              exact
              render={props => <Post jumpToAnswer={true} />}
            />
            <Route path="/login" exact component={Login} />
            <Route path="/signup" exact component={SignUp} />
            <Route path="/user/:id" exact render={props => <PublicProfile />} />
            <Route path="/profile" exact render={() => <Profile />} />
            <Route path="/verify/:key" exact component={VerifyEmail} />
            <Route
              path="/reset/:key"
              exact
              component={ResetForgottenPassword}
            />
            <Route
              path="/article/:id/:articleTitle"
              exact
              component={Article}
            />
            <Route path="/cookie-policy" exact component={Cookie} />
            <Route path="/terms" exact component={Terms} />
            <Route path="/privacy" exact component={Privacy} />
            <Route path="" component={P404} />
          </Switch>
          <GoogleAnalytics />
        </LastLocationProvider>
      </BrowserRouter>
    );
  }
}

export default App;
