import React, { Component } from 'react';
import ReactGA from 'react-ga';

class GoogleAnalytics extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }

    componentDidMount() {
        ReactGA.initialize(process.env.REACT_APP_ANALYTICS_UA)
        ReactGA.pageview(window.location.pathname);
    }

    componentDidUpdate() {
        ReactGA.pageview(window.location.pathname);
    }

    render() { 
        return ( null );
    }
}
 
export default GoogleAnalytics;