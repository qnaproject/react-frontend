import React, { Component } from "react";

class Terms extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div style={{ padding: "1em" }}>
        <h1>Terms and Conditions of Clevereply</h1>
        <h2>Prohibited Content</h2>
        <p>The following is prohibited in any means</p>
        <ul>
          <li>Depiction of violence/violent behaviour/violent activities</li>
          <li>
            Promoting the sale or distribution of any kind of alcohol/alcoholic
            beverage.
          </li>
          <li>
            Discussing/Promoting/Sale of any kind of drugs, drug paraphernalia
            or narcotic substances or any illegal consumables.
          </li>
          <li>
            Nudity/Sexual content of any form, in any form including text,
            images, videos and external links.
          </li>
          <li>Gambling, Betting, Money laundering.</li>
          <li>
            Distributing/Promoting illegal content, warez, software cracks and
            hacks, hacktools, keyloggers or any malicious tool.
          </li>
          <li>
            Promoting your business/blog/website/product except when it is in an
            answer and is directly related to the question aksed and is not the
            intention to promote it but only to provide more details which may
            help the author of the question.
          </li>
          <li>
            Content in any language other than English except for individual
            words or small number of phrases. Primarily the content has to be in
            English.
          </li>
          <li>Shocking Content/Gore</li>
          <li>Excessive profanity.</li>
          <li>Malware or adware.</li>
          <li>
            Instructions to assemble/buy/sell weapons of any form or kind.
          </li>
          <li>
            Content regarding programs/tools which compensate users for clicking
            ads or offers, performing searches, surfing websites or reading
            emails
          </li>
          <li>
            Any other content that is illegal, promotes illegal activity or
            infringes on the legal rights of others
          </li>
        </ul>
        <p>
          <h2>General Terms</h2>
          <ul>
            <li>
              Any question aksed must belong to one of the categories on
              Clevereply.
            </li>
            <li>
              Any question/answer/comment may not be deleted after 2 days of
              posting it. This is to keep clevereply consistent. You may request
              for undeletable content to be deleted by contacting clevereply at
              contact@clevereply.com with your registered email address.
            </li>
            <li>
              You are responsible for what you post on clevereply. Any
              consequences rising from content submitted to clevereply shall be
              borne by the author and only the author.
            </li>
            <li>
              Posting any kind of prohibited content may result in content
              deletion along with points deduction. It may also result in the
              content poster's account being suspended temporarily or
              permanently terminated.
            </li>
            <li>
              Clevereply reserves all right in changing the terms and conditions
              at any time.
            </li>
            <li>
              Clevereply shall not be held liable for any
              consequences/loss/adverse effects whether direct or indirect
              arising out of any content submitted by any user on clevereply.
            </li>
          </ul>
        </p>
        <h2>DISCLAIMERS AND LIMITATION OF LIABILITY</h2>
        <ul>
          <li>
            CLEVEREPLY (the website clevereply.com, it's founder(s), employers,
            employees) IS PROVIDING YOU THE CLEVEREPLY PLATFORM, ALONG WITH OUR
            CONTENT AND MATERIALS AND THE OPPORTUNITY TO SHARE AND CONNECT WITH
            OTHERS, ON AN “AS IS” AND “AS AVAILABLE” BASIS, WITHOUT WARRANTY OF
            ANY KIND, EXPRESS OR IMPLIED. WITHOUT LIMITING THE FOREGOING,
            CLEVEREPLY ENTITIES EXPRESSLY DISCLAIM ANY AND ALL WARRANTIES AND
            CONDITIONS OF MERCHANTABILITY, TITLE, ACCURACY AND COMPLETENESS,
            UNINTERRUPTED OR ERROR-FREE SERVICE, FITNESS FOR A PARTICULAR
            PURPOSE, QUIET ENJOYMENT, NON-INFRINGEMENT, AND ANY WARRANTIES
            ARISING OUT OF COURSE OF DEALING OR TRADE USEAGE.
          </li>
          <li>
            We (the website clevereply.com, it's founder(s), employers,
            employees) shall not be held responsible for any content that
            appears on this Website. You agree to protect and defend us against
            all claims that is rising on this Website.
          </li>
          <li>
            CLEVEREPLY (the website clevereply.com, it's founder(s), employers,
            employees) does not provide any form of guarantee or warranty
            whatsoever.
          </li>
          <li>
            CLEVEREPLY (the website clevereply.com, it's founder(s), employers,
            employees) does not guarantee the availability of the platform or
            make any promises on performance or reliablility.
          </li>
        </ul>
      </div>
    );
  }
}

export default Terms;
