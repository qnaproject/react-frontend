import React, { Component } from "react";

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div style={{ padding: "2em" }}>
        <p>Contact Email: contact@clevereply.com</p>
      </div>
    );
  }
}

export default Contact;
