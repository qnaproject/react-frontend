import React, { Component } from 'react';
import {Helmet} from "react-helmet";

class P404 extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
                <Helmet>
                    <title>Page Not Found</title>
                    <meta name="robots" content="noindex"/>
                </Helmet>
                <div style={{textAlign:"center", padding:"2em"}}>
                <h1>
                    Page Not Found
                </h1>
                </div>
            </div>
         );
    }
}
 
export default P404;