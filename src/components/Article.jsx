import React, { Component } from "react";
import "./Article.css";
import axios from "axios";
import {
  Alert,
  Row,
  Button,
  Col,
  Icon,
  message,
  Avatar,
  Popover,
  Modal,
  Select,
  Tooltip,
  Rate
} from "antd";
import tinycolor from "tinycolor2";
import Header from "./Header";
import moment from "moment";
import { withCookies } from "react-cookie";
import { withRouter, Link } from "react-router-dom";
import { cloneDeep, map, remove, uniq } from "lodash";
import Comment from "./Comment";
import bookmark from "./res/bookmark.png";
import sanitizeHtml from "sanitize-html";
import PopupLogin from "./PopupLogin";
import articleLike from "./res/article_like.png";
import articleNoLike from "./res/article_nolike.png";
import * as utils from "./utils/utils.js";
import TermsMini from "./TermsMini";
import { Helmet } from "react-helmet";

class Article extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.backUrl = "";
    this.commentUids = [];
    window.addEventListener("scroll", this.scrollHandler);
  }

  getArticle = () => {
    var params = window.location.pathname.split("/");
    var aId = params[2];
    let cookieKey =
      window.location.pathname.split("/")[1] +
      ":" +
      window.location.pathname.split("/")[2];
    let cacheOverrideNounce = this.props.cookies.get(cookieKey);
    let headers = {
      "Content-type": "text/plain"
    };
    if (cacheOverrideNounce && cacheOverrideNounce.length > 0) {
      headers["cache-override-nounce"] = cacheOverrideNounce;
    }
    if (aId < 1) {
      this.setState({
        error: "Something went wrong"
      });
    } else {
      axios({
        url: this.backUrl + "getArticle",
        method: "POST",
        headers: headers,
        data: {
          a_id: parseInt(aId)
        }
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            this.setState({
              gotArticle: true,
              data: res.data.data,
              aId: aId
            });
            if (
              window &&
              window.location &&
              window.location.pathname &&
              window.location.pathname.split("/")[3] === "null"
            ) {
              let safeTitle = res.data.data.title
                .split(" ")
                .join("-")
                .toLowerCase()
                .split("?")
                .join("-")
                .split("'")
                .join("");
              if (safeTitle[safeTitle.length - 1] === "-") {
                safeTitle = safeTitle.slice(0, safeTitle.length - 1);
              }
              this.props.history.replace(
                encodeURI("/article/" + res.data.data.a_id + "/" + safeTitle)
              );
            }
          } else {
            if (res && res.data && res.data.msg) {
              this.setState({
                error: res.data.msg
              });
            } else {
              this.setState({
                error:
                  "Something went wrong while fetching article. Please try again later."
              });
            }
          }
        })
        .catch(e => {
          this.setState({
            error:
              "Something went wrong while fetching article. Please try again later."
          });
        });
    }
  };

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;

    this.getArticle();
  }

  componentDidUpdate() {
    if (
      this.state.gotArticle &&
      !this.gotDps &&
      this.commentUids &&
      this.commentUids.length > 0
    ) {
      this.gotDps = true;
      this.getAndSetDpUrlsAndPoints();
    }
    if (
      this.state.gotArticle &&
      !this.state.error &&
      !this.state.gotOtherInfo
    ) {
      this.setState({
        gotOtherInfo: true
      });
      let aId = this.state.data && this.state.data.a_id;
      let uId = this.state.data && this.state.data.u_id;
      if (aId && aId > 0) {
        axios({
          url: this.backUrl + "getOtherArticles",
          method: "POST",
          headers: {
            "Content-type": "text/plain"
          },
          data: {
            a_id: aId,
            category: this.getCategory(this.state.data.category)
          }
        })
          .then(res => {
            if (res && res.data && res.data.success) {
              this.setState({
                otherData: res.data.data
              });
            } else {
              if (res && res.data && res.data.msg) {
                this.setState({
                  otherError: res.data.msg
                });
              } else {
                this.setState({
                  otherError:
                    "Something went wrong while fetching other articles."
                });
              }
            }
          })
          .catch(e => {
            this.setState({
              otherError: "Something went wrong while fetching other articles."
            });
          });
      }
      if (uId && uId > 0) {
        axios({
          url: this.backUrl + "getOtherUserArticles",
          method: "POST",
          headers: {
            "Content-type": "text/plain"
          },
          data: {
            u_id: uId
          }
        })
          .then(res => {
            if (res && res.data && res.data.success) {
              remove(res.data.data, o => {
                return o.a_id === aId;
              });
              this.setState({
                otherUserData: res.data.data
              });
            } else {
              if (res && res.data && res.data.msg) {
                this.setState({
                  otherUserError: res.data.msg
                });
              } else {
                this.setState({
                  otherUserError:
                    "Something went wrong while fetching other user articles."
                });
              }
            }
          })
          .catch(e => {
            this.setState({
              otherUserError:
                "Something went wrong while fetching other user articles."
            });
          });
      }
      if (uId && uId > 0) {
        axios({
          url: this.backUrl + "getPublicUserDetails",
          method: "POST",
          headers: {
            "Content-type": "text/plain"
          },
          data: {
            u_id: uId
          }
        })
          .then(res => {
            if (res && res.data && res.data.success) {
              this.setState({
                otherPublicUserData: res.data.data
              });
            } else {
              if (res && res.data && res.data.msg) {
                this.setState({
                  otherPublicUserError: res.data.msg
                });
              } else {
                this.setState({
                  otherPublicUserError:
                    "Something went wrong while fetching other user details."
                });
              }
            }
          })
          .catch(e => {
            this.setState({
              otherPublicUserError:
                "Something went wrong while fetching other user details."
            });
          });
      }
    }
  }

  componentWillReceiveProps(prevProps) {
    if (prevProps.location !== this.props.location) {
      this.commentUids = [];
      this.gotDps = false;
      this.setState({
        data: null,
        otherData: null,
        otherPublicUserData: null,
        otherUserData: null,
        gotArticle: null,
        gotOtherInfo: null,
        error: null
      });
      this.getArticle();
    }
  }

  categoryColorMap = utils.categoryColorMap;

  getCategory = catNum => {
    var categoryList = utils.categoryList;
    return categoryList[catNum];
  };

  handleLike = () => {
    let sessionId = this.props.cookies.get("session_id");
    if (!sessionId) {
      this.setState({
        loginModalVisible: true
      });
      this.enableMainBlur(true);
      return;
    }
    if (!this.state.liked) {
      let tempData = cloneDeep(this.state.data);
      tempData.points++;
      this.setState({
        liked: true,
        data: tempData
      });
      axios({
        url: this.backUrl + "upvoteArticle",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          session_id: sessionId
        },
        data: {
          a_id: parseInt(this.state.aId)
        }
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            if (res.data.msg && res.data.msg === "You have already liked.") {
              message.warn(res.data.msg);
              let tempData = cloneDeep(this.state.data);
              tempData.points--;
              this.setState({
                liked: true,
                data: tempData
              });
            } else if (
              res.data.msg &&
              res.data.msg === "You cannot like your own article."
            ) {
              message.warn(res.data.msg);
              let tempData = cloneDeep(this.state.data);
              tempData.points--;
              this.setState({
                liked: false,
                data: tempData
              });
            } else {
            }
          } else {
            let tempData = cloneDeep(this.state.data);
            tempData.points--;
            this.setState({
              liked: false,
              data: tempData
            });
            if (res && res.data.msg) {
              message.error(res.data.msg);
            } else {
              message.error("Something went wrong. Please try again later.");
            }
          }
        })
        .catch(e => {
          let tempData = cloneDeep(this.state.data);
          tempData.points--;
          this.setState({
            liked: false,
            data: tempData
          });
          message.error("Something went wrong. Please try again later.");
        });
    }
  };

  deleteArticle = aId => {
    var that = this;
    Modal.confirm({
      title:
        "Are you sure you want to delete this article? This action cannot be undone.",
      onOk() {
        axios({
          url: that.backUrl + "deleteArticle",
          method: "POST",
          headers: {
            "Content-type": "text/plain",
            session_id: that.props.cookies.get("session_id")
          },
          data: {
            a_id: aId
          }
        })
          .then(res => {
            if (res && res.data && res.data.success) {
              var cookieKey =
                window.location.pathname.split("/")[1] +
                ":" +
                window.location.pathname.split("/")[2];
              this.props.cookies.set(cookieKey, moment.now().toString(), {
                path: "/",
                expires: moment()
                  .add(610, "seconds")
                  .toDate()
              });
              message.success(res.data.msg);
              setTimeout(function() {
                that.props.history.push("/");
              }, 1500);
            } else {
              if (res && res.data && res.data.msg) {
                if (res.data.msg === "You need to login to continue.") {
                  message.warn(res.data.msg);
                  let cookiesObj = this.props.cookies.getAll();
                  for (var key in cookiesObj) {
                    this.props.cookies.remove(key, { path: "/" });
                  }
                } else {
                  message.error(res.data.msg);
                }
              } else {
                message.success(
                  "Something went wrong while deleting this article. Please try again later."
                );
              }
            }
          })
          .catch(e => {
            message.success(
              "Something went wrong while deleting this article. Please try again later."
            );
          });
      }
    });
  };

  reportArticle() {
    if (this.props.cookies.get("session_id")) {
      this.setState({
        viewArticleReportModal: true
      });
    } else {
      message.warn("You need to login to continue.");
    }
  }

  handleArticleReportCancel() {
    this.setState({
      viewArticleReportModal: false,
      reportArticleCategory: null,
      reportArticleLoading: false
    });
  }

  handleReportArticleCategoryChange(val) {
    this.setState({
      reportArticleCategory: val
    });
  }

  handleArticleReportOk() {
    this.setState({
      reportArticleLoading: true
    });
    axios({
      url: this.backUrl + "reportPost",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id")
      },
      data: {
        qac_id: this.state.data && this.state.data.a_id,
        r_type: 4,
        report_category: this.state.reportArticleCategory
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            viewArticleReportModal: false,
            reportArticleCategory: null,
            reportArticleLoading: false
          });
          message.success(res.data.msg);
        } else {
          if (res && res.data && res.data.msg) {
            if (res.data.msg === "You need to login to continue.") {
              let cookiesObj = this.props.cookies.getAll();
              for (var key in cookiesObj) {
                this.props.cookies.remove(key, { path: "/" });
              }
            }
            this.setState({
              reportArticleLoading: false,
              reportArticleError: res.data.msg
            });
          } else {
            this.setState({
              reportArticleLoading: false,
              reportArticleError:
                "Something went wrong while reporting this article. Please try again later."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          reportArticleLoading: false,
          reportArticleError:
            "Something went wrong while reporting this article. Please try again later."
        });
      });
  }

  scrollHandler = () => {
    if (this.otherArticlesRef && this.articleContentRef) {
      if (!this.otherOffsetTop) {
        this.otherOffsetTop = this.otherArticlesRef.offsetTop;
      }
      if (
        this.otherArticlesRef.clientHeight <=
        window.innerHeight - this.otherOffsetTop
      ) {
        // if(!this.setFixed) {
        this.setFixed = true;
        let tempWidth = this.otherArticlesRef.clientWidth;
        this.otherArticlesRef.style.position = "fixed";
        this.otherArticlesRef.style.width =
          "calc(" + this.otherArticlesColRef.clientWidth + "px - 0em" + ")";
        this.otherArticlesRef.style.minWidth = 0;
        // }
      } else if (
        this.articleContentRef.clientHeight - this.articleContentRef.offsetTop >
          window.innerHeight &&
        this.articleContentRef.clientHeight >
          this.otherArticlesRef.clientHeight &&
        window.scrollY >=
          this.otherArticlesRef.clientHeight -
            window.innerHeight +
            this.otherOffsetTop
      ) {
        this.stageOneOther = true;
        this.stateTwoOther = false;
        let tempWidth = this.otherArticlesRef.clientWidth;
        this.otherArticlesRef.style.position = "fixed";
        this.otherArticlesRef.style.bottom = 0;
        this.otherArticlesRef.style.width =
          "calc(" + this.otherArticlesColRef.clientWidth + "px - 0em" + ")";
        this.otherArticlesRef.style.minWidth = 0;
      } else if (
        this.articleContentRef.clientHeight - this.articleContentRef.offsetTop >
          window.innerHeight &&
        this.articleContentRef.clientHeight >
          this.otherArticlesRef.clientHeight &&
        window.scrollY <
          this.otherArticlesRef.clientHeight -
            window.innerHeight +
            this.otherOffsetTop
      ) {
        this.stageOneOther = false;
        this.stateTwoOther = true;
        this.otherArticlesRef.style.position = "relative";
      }
    }

    let scrollY = window.scrollY;
    // if (scrollY > 400) {
    //   if (!this.state.showTopScroll) {
    //     this.setState({
    //       showTopScroll: true
    //     });
    //   }
    // } else if (scrollY <= 400) {
    //   if (this.state.showTopScroll) {
    //     this.setState({
    //       showTopScroll: false
    //     });
    //   }
    // }
  };

  addBookmark = () => {
    if (!this.props.cookies.get("session_id")) {
      this.setState({
        loginModalVisible: true
      });
      this.enableMainBlur(true);
      return;
    }
    if (this.state.aId) {
      axios({
        url: this.backUrl + "addBookmark",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          session_id: this.props.cookies.get("session_id")
        },
        data: {
          qa_id: parseInt(this.state.aId),
          type: 2
        }
      }).then(res => {
        if (res && res.data && res.data.success) {
          message.success(res.data.msg);
        } else {
          if (res && res.data && res.data.msg) {
            message.error(res.data.msg);
          } else {
            message.error(
              "Something went wrong while adding bookmark. Please try again later."
            );
          }
        }
      });
    }
  };

  getAndSetDpUrlsAndPoints = () => {
    axios({
      url: this.backUrl + "getUserDpUrlAndPoints",
      headers: {
        "Content-type": "text/plain"
      },
      method: "POST",
      data: {
        u_ids: uniq(this.commentUids)
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          var that = this;
          if (res.data.data.length > 0) {
            that.setState({
              userDpDataForComments: res.data.data
            });
          }
        } else {
          if (res && res.data && res.data.msg) {
          } else {
          }
        }
      })
      .catch(e => {});
  };

  getCommentUserDp = () => {
    return this.state.userDpDataForComments;
  };

  enableMainBlur = v => {
    if (v) {
      this.setState({ mainBlur: true });
    } else {
      this.setState({ mainBlur: false });
    }
  };

  render() {
    console.log(this.state.data);
    let article;
    let aData = this.state.data;
    let catColor;
    let catDarkColor;
    let articlePoints;
    this.commentUids = [];
    if (aData && aData.comments && aData.comments.length > 0) {
      map(aData.comments, o => {
        if (o.u_id) {
          this.commentUids.push(o.u_id);
        }
      });
    }
    if (this.state.gotArticle && !this.state.error) {
      let t_points = aData.points,
        points;
      if (t_points > 999999) {
        let point_string = parseInt(t_points / 1000000);

        points = point_string + "M";
      } else if (t_points > 999) {
        let point_string = parseInt(t_points / 1000);

        points = point_string + "K";
      } else {
        points = t_points;
      }
      catColor = this.categoryColorMap.get(this.getCategory(aData.category));

      catDarkColor = tinycolor(
        this.categoryColorMap.get(this.getCategory(aData.category))
      )
        .darken(0)
        .toHexString();
      var createdUTC = moment(aData.created).utc();
      var localTime = moment(createdUTC)
        .local()
        .format("MMMM Do YYYY, h:mm:ss a");
      articlePoints = (
        <div className="article-points-div">
          <Tooltip title="Like" placement="left">
            <div className={"heart-div"} onClick={this.handleLike}>
              <img
                className={
                  "article-like-heart-img" +
                  (this.state.liked ? " no-anim" : "")
                }
                src={this.state.liked ? articleLike : articleNoLike}
              />
            </div>
          </Tooltip>
          <div className="article-points">{points || 0}</div>

          <Tooltip title="Save for later." placement="left">
            <img
              onClick={this.addBookmark}
              className={"bookmark-icon" + this.state.dark}
              src={bookmark}
              alt="bookmark"
            />
          </Tooltip>
        </div>
      );
      article = (
        <div>
          <div>
            <h2 className={"a-title" + this.state.dark}>{aData.title}</h2>
            <div className="article-options-div">
              <Popover
                // mouseLeaveDelay={1}
                className="article-options-div-inner"
                placement="bottom"
                trigger="click"
                content={
                  <div>
                    {aData.u_id + "" ===
                    this.props.cookies.get("u_id") ? null : (
                      <div>
                        <Button
                          style={{
                            border: "none",
                            width: "7em"
                          }}
                          onClick={() => {
                            this.reportArticle();
                          }}
                        >
                          Report
                        </Button>
                      </div>
                    )}
                    {aData.u_id + "" === this.props.cookies.get("u_id") ||
                    this.props.cookies.get("role") > 0 ? (
                      <div>
                        <Button
                          style={{
                            border: "none",
                            width: "7em"
                          }}
                          type="danger"
                          onClick={() => {
                            this.deleteArticle(aData.a_id);
                          }}
                        >
                          Delete
                        </Button>
                      </div>
                    ) : null}
                  </div>
                }
              >
                <div className={"article-options-trigger" + this.state.dark}>
                  ...
                </div>
              </Popover>
            </div>
          </div>
          <div
            onClick={() => {
              this.props.history.push("/" + this.getCategory(aData.category));
            }}
            className={"q-info-post" + this.state.dark}
            style={{
              cursor: "pointer"
            }}
          >
            <span
              style={{
                backgroundColor: tinycolor(
                  this.categoryColorMap.get(this.getCategory(aData.category))
                )
                  .darken(0)
                  .toHexString()
              }}
              className="cat-span"
            />
            {this.getCategory(aData.category)}
          </div>
          <div className="vl" />
          <div className={"q-info-created" + this.state.dark}>
            {localTime.toString()}
          </div>
          <div className="vl" />
          <div
            className={"author" + this.state.dark}
            onClick={() => {
              if (aData.u_id) {
                this.props.history.push("/user/" + aData.u_id);
              }
            }}
          >
            {aData.name || "Deleted User"}
          </div>
          <div
            className={"article-content" + this.state.dark}
            dangerouslySetInnerHTML={{
              __html: sanitizeHtml(aData.content, {
                allowedTags: [
                  "p",
                  "strong",
                  "em",
                  "s",
                  "blockquote",
                  "ol",
                  "ul",
                  "li",
                  "a",
                  "img",
                  "pre",
                  "span",
                  "u"
                ],
                allowedAttributes: {
                  a: ["href", "rel", "target"],
                  img: ["src", "rel"],
                  pre: ["class", "spellcheck"],
                  span: ["class"]
                }
              })
            }}
          />
          <div className="article-points-div fallback">
            <div className={"heart-div"} onClick={this.handleLike}>
              <Tooltip title="Like" placement="left">
                <img
                  className="article-like-heart-img"
                  src={this.state.liked ? articleLike : articleNoLike}
                />
              </Tooltip>
            </div>
            <Tooltip title="Save for later." placement="left">
              <img
                onClick={this.addBookmark}
                className={"bookmark-icon-mobile-article" + this.state.dark}
                src={bookmark}
                alt="bookmark"
              />
            </Tooltip>
            <div className="article-points">{points}</div>
          </div>
          <Comment
            article={true}
            colorScheme={tinycolor("#1890ff")
              .lighten(30)
              .toHexString()}
            aId={aData.a_id}
            val={aData.comments}
            uId={aData.u_id}
            getCommentUserDp={this.getCommentUserDp}
          />
        </div>
      );
    } else if (this.state.error) {
      article = <Alert type="error" message={this.state.error} />;
    }

    let otherArticles, otherUserArticles, otherUserData;
    if (this.state.gotOtherInfo) {
      if (this.state.otherData && this.state.otherData.length > 0) {
        let that = this;
        otherArticles = map(this.state.otherData, function(item, i) {
          let safeTitle = item.title
            .split(" ")
            .join("-")
            .toLowerCase()
            .split("?")
            .join("-")
            .split("'")
            .join("");
          if (safeTitle[safeTitle.length - 1] === "-") {
            safeTitle = safeTitle.slice(0, safeTitle.length - 1);
          }
          let t_points = item.points,
            points;
          if (t_points > 999999) {
            let point_string = parseInt(t_points / 1000000);

            points = point_string + "M";
          } else if (t_points > 999) {
            let point_string = parseInt(t_points / 1000);

            points = point_string + "K";
          } else {
            points = t_points;
          }
          let pointColorClass = "one";
          if (t_points > 99) {
            pointColorClass = "two";
          }
          if (t_points > 999) {
            pointColorClass = "three";
          }
          return (
            <div className={"other-question-div" + that.state.dark} key={i}>
              <div style={{ lineHeight: "1.2" }}>
                <div
                  className={
                    "article-other-article-points" + " " + pointColorClass
                  }
                >
                  {points}
                </div>
                <Link to={encodeURI("/article/" + item.a_id + "/" + safeTitle)}>
                  {item.title}
                </Link>
              </div>
            </div>
          );
        });
      }
      if (this.state.otherData && this.state.otherData.length < 1) {
        otherArticles = (
          <div className={"article-other-user-data-div" + this.state.dark}>
            No other articles
          </div>
        );
      }
      if (this.state.otherUserData && this.state.otherUserData.length > 0) {
        otherUserArticles = map(this.state.otherUserData, function(item, i) {
          let safeTitle = item.title
            .split(" ")
            .join("-")
            .toLowerCase()
            .split("?")
            .join("-")
            .split("'")
            .join("");
          if (safeTitle[safeTitle.length - 1] === "-") {
            safeTitle = safeTitle.slice(0, safeTitle.length - 1);
          }
          let t_points = item.points,
            points;
          if (t_points > 999999) {
            let point_string = parseInt(t_points / 1000000);

            points = point_string + "M";
          } else if (t_points > 999) {
            let point_string = parseInt(t_points / 1000);

            points = point_string + "K";
          } else {
            points = t_points;
          }
          let pointColorClass = "one";
          if (t_points > 99) {
            pointColorClass = "two";
          }
          if (t_points > 999) {
            pointColorClass = "three";
          }
          return (
            <div className="other-question-div" key={i}>
              <div style={{ lineHeight: "1.2" }}>
                <div
                  className={"article-other-article-points " + pointColorClass}
                >
                  {points}
                </div>
                <Link to={encodeURI("/article/" + item.a_id + "/" + safeTitle)}>
                  {item.title}
                </Link>
              </div>
            </div>
          );
        });
      }
      if (this.state.otherUserData && this.state.otherUserData.length < 1) {
        otherUserArticles = (
          <div className={"article-other-user-data-div" + this.state.dark}>
            No other articles
          </div>
        );
      }

      if (this.state.otherPublicUserData) {
        let t_points = this.state.otherPublicUserData.points,
          points;
        if (t_points > 999999) {
          let point_string = ((1.0 * t_points) / 1000000.0).toFixed(3);
          let seperateVals = point_string.split(".");
          if (seperateVals[1]) {
            points = seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "M";
          } else {
            points = seperateVals[0] + "M";
          }
        } else if (t_points > 999) {
          let point_string = ((1.0 * t_points) / 1000.0).toFixed(3);
          let seperateVals = point_string.split(".");
          if (seperateVals[1]) {
            points = seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "K";
          } else {
            points = seperateVals[0] + "K";
          }
        } else {
          points = t_points;
        }

        let name = this.state.otherPublicUserData.fname;
        if (this.state.otherPublicUserData.lname) {
          name += " " + this.state.otherPublicUserData.lname;
        }
        let tier;
        if (t_points > 999999) {
          tier = 5;
        } else if (t_points > 99999) {
          tier = 4;
        } else if (t_points > 9999) {
          tier = 3;
        } else if (t_points > 999) {
          tier = 2;
        } else if (t_points > 99) {
          tier = 1;
        } else {
          tier = 0;
        }
        otherUserData = (
          <div>
            <h4 className={"other-question-heading" + this.state.dark}>
              Written by <span className="article-user-name">{name}</span>
            </h4>
            <div className="article-other-user-data-div">
              <div
                className="article-dp"
                onClick={() => {
                  if (this.state.data && this.state.data.u_id) {
                    this.props.history.push("/user/" + this.state.data.u_id);
                  }
                }}
              >
                {this.state.otherPublicUserData.dp_url ? (
                  <Avatar
                    size={64}
                    src={this.state.otherPublicUserData.dp_url}
                  />
                ) : (
                  <Avatar size={64} icon="user" />
                )}
              </div>
              <div className="article-user-points">{points}</div>
              <Rate count={tier} disabled defaultValue={tier} />
            </div>
            <div>
              <h4 className={"other-question-heading" + this.state.dark}>
                Other Articles from{" "}
                <span className="article-user-name">
                  {this.state.otherPublicUserData.fname}
                </span>
              </h4>
            </div>
          </div>
        );
      }
    }

    return (
      <div
        className="article-main-div"
        style={{
          backgroundColor:
            this.state.dark === " dark"
              ? "black"
              : tinycolor("#1890ff")
                  .lighten(35)
                  .toHexString(),
          filter: this.state.mainBlur ? "blur(5px)" : ""
        }}
      >
        <Helmet>
          <meta charSet="utf-8" />
          <title>
            {(this.state.data && this.state.data.title) || "Clevereply"}
          </title>
          {this.state.data && this.state.data.title ? (
            <meta property="og:title" content={this.state.data.title} />
          ) : null}
          <meta
            name="description"
            content={
              this.state.data &&
              sanitizeHtml(this.state.data.content, {
                allowedTags: [],
                allowedAttributes: {}
              })
            }
          />
          <meta
            name="og:description"
            content={
              this.state.data &&
              sanitizeHtml(this.state.data.content, {
                allowedTags: [],
                allowedAttributes: {}
              })
            }
          />
          {this.state.error &&
          this.state.error === "This article doesn't exist" ? (
            <meta name="robots" content="noindex" />
          ) : null}
        </Helmet>
        <Modal //report modal for article
          wrapClassName={"report-article-modal" + this.state.dark}
          style={{ zIndex: "999" }}
          title="Report Article"
          onOk={this.handleArticleReportOk.bind(this)}
          visible={this.state.viewArticleReportModal}
          onCancel={this.handleArticleReportCancel.bind(this)}
          okButtonProps={{
            disabled: this.state.reportArticleCategory ? false : true,
            loading: this.state.reportArticleLoading
            // onClick: () => {
            //   this.handleReportOk();
            // }
          }}
        >
          <div className="report-div">
            <div>What are you reporting?</div>
            <Select
              defaultValue={this.state.reportArticleCategory}
              value={this.state.reportArticleCategory}
              className="report-selector"
              onChange={this.handleReportArticleCategoryChange.bind(this)}
            >
              <Select.Option value="Hate Speech">Hate Speech</Select.Option>
              <Select.Option value="Political Propaganda">
                Political Propaganda
              </Select.Option>
              <Select.Option value="Harmful Intent">
                Harmful Intent
              </Select.Option>
              <Select.Option value="Sexually Explicit Content">
                Sexually Explicit Content
              </Select.Option>
              <Select.Option value="Advertising/Spam">
                Advertising/Spam
              </Select.Option>
              <Select.Option value="Misleading">Misleading</Select.Option>
              <Select.Option value="Content unrelated to question">
                Content unrelated to question
              </Select.Option>
              <Select.Option value="Wrong Category">
                Wrong Category
              </Select.Option>
            </Select>
          </div>
          {this.state.reportArticleError ? (
            <Alert
              style={{ marginTop: "1em" }}
              type="error"
              message={this.state.reportArticleError}
            />
          ) : null}
        </Modal>
        <Header blur={this.enableMainBlur} />
        <Row
          type="flex"
          style={{
            backgroundColor:
              this.state.dark === " dark"
                ? "black"
                : tinycolor("#1890ff")
                    .lighten(35)
                    .toHexString()
          }}
          className="article-main-row"
        >
          <Col
            xs={{ span: 24, order: 3 }}
            sm={{ span: 24, order: 3 }}
            md={{ span: 24, order: 3 }}
            lg={{ span: 6, order: 3 }}
            xl={6}
            xxl={8}
          >
            <div
              ref={ref => {
                this.otherArticlesColRef = ref;
              }}
            >
              {otherUserArticles || otherArticles ? (
                <div
                  ref={ref => {
                    this.otherArticlesRef = ref;
                  }}
                  className={"other-questions-div" + this.state.dark}
                >
                  <div
                    className={"other-questions-inner-div" + this.state.dark}
                  >
                    {otherUserData}
                    {otherUserArticles}
                    <h4 className={"other-question-heading" + this.state.dark}>
                      {"Other Articles from " +
                        this.getCategory(this.state.data.category)}
                    </h4>
                    {otherArticles}
                  </div>
                  <TermsMini />
                </div>
              ) : null}
            </div>
          </Col>
          <Col
            xs={{ span: 24, order: 2 }}
            sm={{ span: 24, order: 2 }}
            md={{ span: 24, order: 2 }}
            lg={{ span: 16, order: 2 }}
            xl={15}
            xxl={12}
          >
            <div
              ref={ref => {
                this.articleContentRef = ref;
              }}
              className={"article-content-div" + this.state.dark}
            >
              {article}
            </div>
          </Col>
          <Col xs={0} sm={0} md={0} lg={2} xl={3} xxl={4} order={1}>
            {articlePoints}
          </Col>
        </Row>
        <Row
          style={{
            backgroundColor:
              this.state.dark === " dark"
                ? "black"
                : "#1890ff"
                ? tinycolor("#1890ff")
                    .lighten(35)
                    .toHexString()
                : null
          }}
        >
          <Col>
            <div className="empty-space" />
          </Col>
        </Row>
        {/* <div
          className="go-to-top-button-div"
          style={{ visibility: this.state.showTopScroll ? null : "hidden" }}
        >
          <Button
            onClick={() => {
              window.scrollTo({
                top: 0,
                behavior: "smooth"
              });
            }}
            className={"go-to-top-button" + this.state.dark}
          >
            <Icon type="arrow-up" />
          </Button>
        </div> */}
        <Modal
          visible={this.state.loginModalVisible}
          footer={null}
          onCancel={() => {
            this.setState({ loginModalVisible: false });
            this.enableMainBlur(false);
          }}
        >
          <PopupLogin login />
        </Modal>
      </div>
    );
  }
}

export default withRouter(withCookies(Article));
