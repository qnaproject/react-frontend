import React, { Component } from "react";
import axios from "axios";
import { Alert } from "antd";
import "./VerifyEmail.css";
import { withRouter } from "react-router-dom";

class VerifyEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.backUrl = ""
  }

  componentDidMount = () => {
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    let key = window.location.pathname.split("/")[2];
    axios({
      url: this.backUrl + "verifyUser",
      method: "POST",
      headers: {
        "Content-type": "text/plain"
      },
      data: {
        verify_key: key
      }
    }).then(res => {
      if (res && res.data.success) {
        this.setState({
          successMsg: res.data.msg
        });
        setTimeout(() => {
          this.props.history.replace("/");
          this.props.history.push("/login");
        }, 3000);
      } else {
        if (res && res.data && res.data.msg) {
          this.setState({
            errorMsg: res.data.msg
          });
        } else {
          this.setState({
            errorMsg:
              "Something went wrong while verifying your email address. Please try again later."
          });
        }
      }
    });
  };

  render() {
    return (
      <div className="verify-email-main-div">
        <div className="verify-email-inner-div">
          {this.state.successMsg ? (
            <div>
              <Alert type="success" message={this.state.successMsg} />
              <p />
              <p />
              Redirecting you to the login page...
            </div>
          ) : null}
          {this.state.errorMsg ? (
            <Alert type="error" message={this.state.errorMsg} />
          ) : null}
        </div>
      </div>
    );
  }
}

export default withRouter(VerifyEmail);
