import React, { Component } from "react";
import "./Home.css";
import Feed from "./Feed";
import { Button, Row, Col, Modal, Input, Select, Alert } from "antd";
import Header from "./Header";
import tinycolor from "tinycolor2";
import { withCookies } from "react-cookie";
import { withRouter } from "react-router-dom";
import PopupLogin from "./PopupLogin";
import Leaderboard from "./Leaderboard";
import AskQuestion from "./AskQuestion";
import * as utils from "./utils/utils.js";
import Terms from "./Terms";
import { Helmet } from "react-helmet";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      feedType: 1,
      contentType: this.props.cookies.get("session_id")
        ? window.location.pathname.split("/").length > 1 &&
          window.location.pathname.split("/")[1] != ""
          ? 1
          : 3
        : 1
    };
    this.backUrl = "";
  }

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
  }

  componentDidUpdate() {}

  onFeedSelectorClick = e => {
    this.setState({
      feedType: e
    });
  };

  onContentSelectorClick = e => {
    this.setState({
      contentType: e
    });
  };

  goToLatestPosts = () => {
    this.setState({
      feedType: 2
    });
    window.scrollTo(0, 0);
  };

  askQuestion = () => {
    let sessionId = this.props.cookies.get("session_id");
    if (!sessionId) {
      this.setState({
        loginModalVisible: true,
        mainBlur: true
      });
      return;
    }
    this.setState({ askVisible: true, sessionId: sessionId });
  };

  handleCancel = () => {
    this.setState({
      askVisible: false,
      askError: null
    });
  };

  enableMainBlur = v => {
    if (v) {
      this.setState({ mainBlur: true });
    } else {
      this.setState({ mainBlur: false });
    }
  };

  render() {
    var catVals = [];
    var categoryColorMap = utils.categoryColorMap;
    var that = this;
    catVals.push(
      <Button
        onClick={() => {
          that.setState({
            viewMinCatRow: false,
            contentType:
              that.state.contentType === 3 ? 1 : that.state.contentType
          });
          that.props.history.push("/");
        }}
        className={"cat-row-button" + that.state.dark}
        style={{ color: that.state.dark === " dark" ? "white" : "black" }}
        block
        key={-1}
        id={"first-category-button"}
      >
        <span className="cat-span" />
        All
      </Button>
    );
    let wvals = window.location.pathname.split("/");
    let currentCat;
    if (wvals.length > 1 && wvals[1] && wvals[1] != "") {
      currentCat = wvals[1];
    }
    categoryColorMap.forEach(function(val, key) {
      catVals.push(
        <Button
          style={{
            backgroundColor: currentCat === key ? "#1890ff" : null,
            color: currentCat === key ? "white" : "rgba(0,0,0,.65)"
          }}
          onClick={() => {
            that.setState({
              viewMinCatRow: false,
              contentType:
                that.state.contentType === 3 ? 1 : that.state.contentType
            });
            that.props.history.push("/" + key);
          }}
          className={
            "cat-row-button" + (currentCat === key ? "" : that.state.dark)
          }
          block
          key={key}
          id={key === "Travel" ? "last-category-button" : null}
        >
          <span
            id={key === "Anon" ? "anon-id" : null}
            style={{
              backgroundColor: tinycolor(val)
                .darken(0)
                .toHexString()
            }}
            className="cat-span"
          />
          <span
            style={{
              color:
                that.state.dark === " dark"
                  ? "white"
                  : currentCat === key
                  ? "white"
                  : "#444444"
            }}
          >
            {key}
          </span>
        </Button>
      );
    });
    return (
      <div
        style={{ filter: this.state.mainBlur ? "blur(5px)" : "" }}
        className={"home-main-div" + this.state.dark}
      >
        <Helmet>
          <title>Clevereply</title>
          <meta
            name="description"
            content="Get clever replies to your every question! Clevereply is a place to ask questions and get clever answers. Share the joy of knowledge and have fun."
          />
          <meta
            property="og:description"
            content="Get clever replies to your every question! Clevereply is a place to ask questions and get clever answers."
          />
        </Helmet>
        <AskQuestion
          askVisible={this.state.askVisible}
          handleCancel={this.handleCancel}
          askError={this.state.askError}
        />
        <Row>
          <Col>
            <Header blur={this.enableMainBlur} />
          </Col>
        </Row>
        <Row type="flex">
          <div className={"home-content-selector-div" + this.state.dark}>
            <div
              className="feed-type-row-col"
              style={{
                width: this.props.cookies.get("session_id") ? "20em" : "13em"
              }}
            >
              {this.props.cookies.get("session_id") ? (
                <Button
                  className={"feed-type-row-button" + this.state.dark}
                  onClick={() => {
                    this.onContentSelectorClick(3);
                    currentCat && that.props.history.push("/");
                  }}
                >
                  <span
                    className={this.state.contentType === 3 ? "active" : ""}
                  >
                    Feed
                  </span>
                </Button>
              ) : null}
              <Button
                className={"feed-type-row-button" + this.state.dark}
                onClick={() => {
                  this.onContentSelectorClick(1);
                }}
              >
                <span className={this.state.contentType === 1 ? "active" : ""}>
                  Questions
                </span>
              </Button>
              <Button
                className={"feed-type-row-button" + this.state.dark}
                onClick={() => {
                  this.onContentSelectorClick(2);
                }}
              >
                <span className={this.state.contentType === 2 ? "active" : ""}>
                  Articles
                </span>
              </Button>
            </div>
            <div className="feed-type-selector">
              <Select
                className={"feed-type-selector-select" + this.state.dark}
                disabled={
                  this.state.contentType === 2 || this.state.contentType === 3
                }
                defaultValue={1}
                value={this.state.feedType}
                onChange={val => {
                  this.setState({ feedType: val });
                }}
              >
                <Select.Option value={1}>Trending</Select.Option>
                <Select.Option value={2}>Latest</Select.Option>
              </Select>
            </div>
            <div className="ask-a-question-button-div">
              <Button
                className="ask-a-question-button"
                size="small"
                type="danger"
                ghost
                block
                onClick={this.askQuestion.bind(this)}
              >
                Ask a question
              </Button>
            </div>
          </div>

          <Col xs={0} sm={6} md={6} lg={5} xl={4} xxl={8}>
            <Row className={"feed-sidebar-col" + this.state.dark}>
              <Col className="catvals-col">
                <div className="cat-row-button-group">{catVals}</div>
              </Col>
            </Row>
          </Col>

          <Col xs={24} sm={18} md={18} lg={19} xl={15} xxl={8}>
            <div
              className={
                "feed-row-col" +
                (this.state.contentType === 2
                  ? " articles"
                  : this.state.contentType === 3
                  ? " feed"
                  : "")
              }
            >
              <Feed
                categoryColorMap={categoryColorMap}
                contentType={this.state.contentType}
                feedType={this.state.feedType}
                category={this.props.category ? this.props.category : "general"}
                goToLatest={this.goToLatestPosts.bind(this)}
              />
            </div>
          </Col>

          <Col xs={0} sm={0} md={0} lg={0} xl={5} xxl={8}>
            <Leaderboard />
          </Col>
        </Row>
        <div
          className={
            this.state.options ? "home-ask-button" + this.state.dark : "none"
          }
          onClick={() => {
            this.askQuestion();
            this.setState({
              options: false
            });
          }}
        >
          <div className="home-ask-button-horizontal" />
          <div className="home-ask-button-vertical" />
        </div>
        <Button
          onClick={() => {
            this.setState({ viewMinCatRow: true, options: false });
          }}
          className={
            this.state.options ? "min-cat-button" + this.state.dark : "none"
          }
          icon="bars"
        />

        <div
          className={"home-option-button" + this.state.dark}
          onClick={() => {
            this.setState({ options: !this.state.options });
          }}
        >
          <div
            className={this.state.options ? "none" : "home-option-button-dots"}
          >
            <span className="home-options-dot1" />
            <span className="home-options-dot2" />
            <span className="home-options-dot3" />
          </div>
          <div
            className={
              this.state.options ? "home-option-button-horizontal" : "none"
            }
          />
          <div
            className={
              this.state.options ? "home-option-button-vertical" : "none"
            }
          />
        </div>
        <div
          style={{ display: this.state.viewMinCatRow ? "block" : "none" }}
          className="min-cat-vals-close-button"
        >
          <Button
            onClick={() => {
              this.setState({ viewMinCatRow: false });
            }}
            style={{ border: "none", boxShadow: "none" }}
            shape="circle"
            icon="close"
          />
        </div>
        <div
          onClick={() => {
            this.setState({ viewMinCatRow: false });
          }}
          className="min-cat-vals-wrapper"
          style={{ display: this.state.viewMinCatRow ? "block" : "none" }}
        />
        <div
          className={"min-cat-vals-div" + this.state.dark}
          style={{ display: this.state.viewMinCatRow ? "block" : "none" }}
        >
          <div className={"cat-row-button-group1" + this.state.dark}>
            {catVals}
          </div>
        </div>
        <Modal
          visible={this.state.loginModalVisible}
          footer={null}
          onCancel={() =>
            this.setState({ loginModalVisible: false, mainBlur: false })
          }
        >
          <PopupLogin login />
        </Modal>
        <Terms />
      </div>
    );
  }
}

export default withRouter(withCookies(Home));
