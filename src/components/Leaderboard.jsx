import React, { Component } from 'react';
import "./Leaderboard.css"
import axios from "axios"
import { withCookies } from "react-cookie";
import {map} from "lodash"
import { Avatar } from 'antd';
import { withRouter } from "react-router-dom";

class Leaderboard extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    componentDidMount() {
        this.setState({
            dark: (this.props.cookies.get("dark") === "true") ? " dark" : ""
        })
        this.backUrl =
            process.env.NODE_ENV === "production"
                ? process.env.REACT_APP_API_URL
                : process.env.REACT_APP_API_URL_DEV;
        axios({
            url:this.backUrl+"getLeaders",
            method: "POST",
            headers: {
                "Content-type":"text/plain"
            }
        })
        .then(res => {
            if(res && res.data && res.data.success && res.data.data && res.data.data.length > 0) {
                this.setState({
                    leaders: res.data.data
                })
            }
        })
    }

    render() { 
        let leaderContent = null
        if(this.state.leaders) {
            leaderContent = map(this.state.leaders[0], (item, i) => {
                var t_points = item.points, points
                if (t_points > 999999) {
                    let point_string = ((1.0 * t_points) / 1000000.0).toFixed(3);
                    let seperateVals = point_string.split(".");
                    if (seperateVals[1]) {
                        points =
                            seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "M";
                    } else {
                        points = seperateVals[0] + "M";
                    }
                } else if (t_points > 999) {
                    let point_string = ((1.0 * t_points) / 1000.0).toFixed(3);
                    let seperateVals = point_string.split(".");
                    if (seperateVals[1]) {
                        points =
                            seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "K";
                    } else {
                        points = seperateVals[0] + "K";
                    }
                } else {
                    points = t_points;
                }
                return <div style={i===9?({marginLeft:"-0.5em"}):(null)} className="leader-individual-div" onClick={()=>{if(item.u_id){this.props.history.push("user/"+item.u_id)}}} key={i}>
                   
                    <div 
                    className={"leader-rank"+this.state.dark}>
                    {i + 1}
                    </div> 
                    {(item.dp_url && item.dp_url.length > 0) ? 
                    <div className="leader-avatar"><Avatar src={item.dp_url}></Avatar></div> : 
                    <div className="leader-avatar"><Avatar icon="user"></Avatar></div>}
                    <div className={"leader-name"+this.state.dark}>{item.fname + " "}{item.lname}</div>
                    <div className="leader-points">{points}</div>
                
                </div>
            })
            leaderContent = (
              <div style={{position:"sticky", top:"0"}}>
                <div className={"leader-header"+this.state.dark}>Leaderboard</div><p/>
                {leaderContent}
              </div>
            );
        }
        return (<div className="leaderboard-div">{leaderContent}</div> );
    }
}

export default withRouter(withCookies(Leaderboard))