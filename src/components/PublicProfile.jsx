import React, { Component } from "react";
import Header from "./Header";
import "./PublicProfile.css";
import axios from "axios";
import { withRouter } from "react-router-dom";
import {
  Row,
  Col,
  Spin,
  Icon,
  Rate,
  Popover,
  Modal,
  Input,
  message,
  Button,
  Alert,
  Avatar
} from "antd";
import moment from "moment";
import messageIcon from "./res/message.svg";
import dpDefault from "./res/avatar-default.png";
import unknownUser from "./res/unknown-user.png";
import { map, reverse } from "lodash";
import sanitizeHtml from "sanitize-html";
import { withCookies } from "react-cookie";
import ArticleEditor from "./ArticleEditor";
import PopupLogin from "./PopupLogin";
import {Helmet} from "react-helmet";

class PublicProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: 1
    };
    this.backUrl = "";
  }

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    let uid = parseInt(window.location.pathname.split("/")[2]);
    this.setState({
      uid: uid
    });
    var urluid = window.location.pathname.split("/")[2];
    let cookieKey = "user:"+urluid;
    let cacheOverrideNounce = this.props.cookies.get(cookieKey)
    let headers = {
      "Content-type": "text/plain"
    }
    if(cacheOverrideNounce && cacheOverrideNounce.length > 0) {
      headers["cache-override-nounce"] = cacheOverrideNounce
    }
    let specialOverrideNounce = "", specialListingOverrideNounce =""
    if(this.props.cookies.get("session_id")) {
      specialOverrideNounce = moment.now().toString()
    }
    if(this.props.cookies.get("u_id") === (urluid+"")) {
      specialListingOverrideNounce = moment.now().toString()
    }
    axios({
      url: this.backUrl + "getPublicUserDetails",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id"),
        "cache-override-nounce": specialOverrideNounce
      },
      data: { u_id: uid }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            userDetails: res.data.data
          });
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              userDetailsError: res.data.msg
            });
          } else {
            this.setState({
              userDetailsError:
                "Something went wrong while fetching user details."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          userDetailsError: "Something went wrong while fetching user details."
        });
      });

    axios({
      url: this.backUrl + "getUserBio",
      method: "POST",
      headers: headers,
      data: { u_id: uid }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            userBio: res.data.data
          });
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              userBioError: res.data.msg
            });
          } else {
            this.setState({
              userBioError: "Something went wrong while fetching user details."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          userBioError: "Something went wrong while fetching user details."
        });
      });

    axios({
      url: this.backUrl + "getOtherPublicUserDetails",
      method: "POST",
      headers: headers,
      data: { u_id: uid }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            userOtherDetails: res.data.data
          });
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              userOtherDetailsError: res.data.msg
            });
          } else {
            this.setState({
              userOtherDetailsError:
                "Something went wrong while fetching user details."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          userOtherDetailsError:
            "Something went wrong while fetching user details."
        });
      });

    axios({
      url: this.backUrl + "getAllUserArticles",
      method: "POST",
      headers: {
        "Content-Type":"text/plain",
        "cache-override-nounce": specialListingOverrideNounce
    },
      data: { u_id: uid }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            userArticles: reverse(res.data.data)
          });
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              userArticlesError: res.data.msg
            });
          } else {
            this.setState({
              userArticlesError: "Something went wrong while fetching articles."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          userOtherDetailsError:
            "Something went wrong while fetching user details."
        });
      });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.location.pathname !== nextProps.location.pathname) {
      this.setState({
        userDetails: { ...this.state.userDetails, points: null }
      });
      this.componentDidMount();
    }
  }

  handleBioClick = () => {
    this.setState({
      content: 1
    });
  };

  handleQuestionsClick = () => {
    this.setState({
      content: 2
    });
    var urluid = window.location.pathname.split("/")[2];
    let specialListingOverrideNounce = ""
    if(this.props.cookies.get("u_id") === (urluid+"")) {
      specialListingOverrideNounce = moment.now().toString()
    }
    if (!this.state.questions) {
      axios({
        url: this.backUrl + "getAllUserQuestions",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          "cache-override-nounce": specialListingOverrideNounce
        },
        data: {
          u_id: this.state.uid
        }
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            this.setState({
              questions: reverse(res.data.data)
            });
          } else {
            if (res && res.data && res.data.msg) {
              this.setState({
                questionsError: res.data.msg
              });
            } else {
              this.setState({
                questionsError:
                  "Something went wrong while fetching user questions. Please try again later."
              });
            }
          }
        })
        .catch(e => {
          this.setState({
            questionsError:
              "Something went wrong while fetching user questions. Please try again later."
          });
        });
    }
  };

  handleAnswersClick = () => {
    this.setState({
      content: 3
    });
    var urluid = window.location.pathname.split("/")[2];
    let specialListingOverrideNounce = ""
    if(this.props.cookies.get("u_id") === (urluid+"")) {
      specialListingOverrideNounce = moment.now().toString()
    }
    if (!this.state.answers) {
      axios({
        url: this.backUrl + "getAllUserAnswers",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          "cache-override-nounce": specialListingOverrideNounce
        },
        data: {
          u_id: this.state.uid
        }
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            this.setState({
              answers: reverse(res.data.data)
            });
          } else {
            if (res && res.data && res.data.msg) {
              this.setState({
                answersError: res.data.msg
              });
            } else {
              this.setState({
                answersError:
                  "Something went wrong while fetching user questions. Please try again later."
              });
            }
          }
        })
        .catch(e => {
          this.setState({
            answersError:
              "Something went wrong while fetching user questions. Please try again later."
          });
        });
    }
  };

  handleMessageClick = () => {
    if (!this.props.cookies.get("session_id")) {
      this.setState({
        loginModalVisible: true
      });
      return;
    }
    this.setState({
      messageModalVisible: true,
      messageDisabled: false
    });
  };

  handleMessageModalCancel = () => {
    this.setState({
      messageModalVisible: false
    });
  };

  handleSendMessage = () => {
    let sessionId = this.props.cookies.get("session_id");
    if (!sessionId) {
      this.setState({
        messageError: "You need to login to continue."
      });
      return;
    }
    if (!this.state.messageText || this.state.messageText.length < 1) {
      this.setState({
        messageError: "You cannot send an empty message."
      });
      return;
    }
    if (this.state.messageText && this.state.messageText.length > 1000) {
      this.setState({
        messageError: "Your message is too long."
      });
      return;
    }
    this.setState({
      messageDisabled: true
    });
    axios({
      url: this.backUrl + "addMessage",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id")
      },
      data: {
        message: this.state.messageText,
        to_user_id: this.state.uid
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            messageModalVisible: false
          });
          message.success(res.data.msg || "Message sucessfully sent.");
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              messageError: res.data.msg,
              messageDisabled: false
            });
          } else {
            this.setState({
              messageError:
                "Something went wrong while sending message. Please try again later.",
              messageDisabled: false
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          messageError:
            "Something went wrong while sending message. Please try again later.",
          messageDisabled: false
        });
      });
  };

  handleMessageChange = val => {
    this.setState({
      messageText: val.target.value
    });
  };

  handleBgChooser = () => {
    this.setState({
      bgChooserVisible: true
    });
    if (!this.gotBgs) {
      this.gotBgs = true;
      axios({
        url: this.backUrl + "getAllBackgroundUrls",
        method: "POST",
        headers: {
          "Content-type": "text/plain"
        }
      }).then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            bgData: res.data.data
          });
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              bgError: res.data.msg
            });
          } else {
            this.setState({
              bgError: "Something went wrong. Please try again later."
            });
          }
        }
      });
    }
  };

  handleBgChange = (bgId, bgUrl) => {
    this.setState({
      userBio: { ...this.state.userBio, bg_url: bgUrl }
    });
    axios({
      url: this.backUrl + "updateUserBackground",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id")
      },
      data: {
        bg_id: parseInt(bgId)
      }
    }).then(res => {
      if (res && res.data && !res.data.success) {
        if (res.data.msg) {
          message.error(res.data.msg);
        } else {
          message.error("Something went wrong. Please try again later.");
        }
      }
    });
  };

  followUser =(uid) => {
    if (!this.props.cookies.get("session_id")) {
      this.setState({
        loginModalVisible: true
      });
      return;
    }
    if(uid && uid > 0) {
      axios({
        url: this.backUrl+"follow",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          "session_id": this.props.cookies.get("session_id")
        },
        data: {
          "type":1,
          "fu_id": uid
        }
      })
      .then(res => {
        if( res && res.data && res.data.success) {
            this.setState({
              userDetails: {
                ...this.state.userDetails,
                following: true
              }
            })
        } else if(res && res.data && res.data.msg) {
          message.error(res.data.msg)
        } else {
          message.error("Something went wrong. Please try again later.")
        }
      })
    }
  }

  unfollowUser =(uid) => {
    if (!this.props.cookies.get("session_id")) {
      this.setState({
        loginModalVisible: true
      });
      return;
    }
    if(uid && uid > 0) {
      axios({
        url: this.backUrl+"unFollow",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          "session_id": this.props.cookies.get("session_id")
        },
        data: {
          "type":1,
          "fu_id": uid
        }
      })
      .then(res => {
        if( res && res.data && res.data.success) {
          this.setState({
            userDetails: {
              ...this.state.userDetails,
              following: false
            }
          })
        } else if(res && res.data && res.data.msg) {
          message.error(res.data.msg)
        } else {
          message.error("Something went wrong. Please try again later.")
        }
      })
    }
  }


  render() {
    var dp_url,
      gender,
      bg_url,
      points,
      flag,
      created,
      description,
      error,
      numArticles,
      numQuestions,
      numAnswers,
      name,
      content,
      articles,
      questions,
      answers,
      rate,
      messageLink;
    if (this.state.userDetailsError) {
      error = this.state.userDetailsError;
      dp_url = unknownUser;
    } else {
      if (this.state.userDetails) {
        if (this.state.userDetails.created) {
          var createdUTC = moment(this.state.userDetails.created).utc();
          var nowUtc = moment(moment()).utc();
          var duration = moment.duration(-nowUtc.diff(createdUTC));
          created = "Joined " + duration.humanize(true);
        }
        dp_url = this.state.userDetails.dp_url
          ? this.state.userDetails.dp_url
          : dpDefault;
        if (this.state.userDetails.points > -1) {
          let t_points = this.state.userDetails.points;
          let tier;

          if (t_points > 999999) {
            tier = 5;
          } else if (t_points > 99999) {
            tier = 4;
          } else if (t_points > 9999) {
            tier = 3;
          } else if (t_points > 999) {
            tier = 2;
          } else if (t_points > 99) {
            tier = 1;
          } else {
            tier = 0;
          }
          rate = (
            <Popover
              content={
                <table>
                  <tbody>
                    <tr>
                      <td>Points > 99</td>
                      <td>
                        <Rate count={1} disabled defaultValue={1} />
                      </td>
                    </tr>
                    <tr>
                      <td>Points > 999</td>
                      <td>
                        <Rate count={2} disabled defaultValue={2} />
                      </td>
                    </tr>
                    <tr>
                      <td>Points > 9999</td>
                      <td>
                        <Rate count={3} disabled defaultValue={3} />
                      </td>
                    </tr>
                    <tr>
                      <td>Points > 99999</td>
                      <td>
                        <Rate count={4} disabled defaultValue={4} />
                      </td>
                    </tr>
                    <tr>
                      <td>Points > 999999</td>
                      <td>
                        <Rate count={5} disabled defaultValue={5} />
                      </td>
                    </tr>
                  </tbody>
                </table>
              }
            >
              {""}
              <Rate count={tier} disabled defaultValue={tier} />
            </Popover>
          );
          if (t_points > 999999) {
            let point_string = ((1.0 * t_points) / 1000000.0).toFixed(3);
            let seperateVals = point_string.split(".");
            if (seperateVals[1]) {
              points =
                seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "M";
            } else {
              points = seperateVals[0] + "M";
            }
          } else if (t_points > 999) {
            let point_string = ((1.0 * t_points) / 1000.0).toFixed(3);
            let seperateVals = point_string.split(".");
            if (seperateVals[1]) {
              points =
                seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "K";
            } else {
              points = seperateVals[0] + "K";
            }
          } else {
            points = t_points;
          }
        }

        gender = this.state.userDetails.gender
          ? "Gender - " + this.state.userDetails.gender
          : null;
        name = (
          <div className="user-name">
            {this.state.userDetails.fname +
              " " +
              (this.state.userDetails.lname
                ? this.state.userDetails.lname
                : "")}
          </div>
        );
        let nameNoStyle =
          this.state.userDetails.fname +
          " " +
          (this.state.userDetails.lname ? this.state.userDetails.lname : "");

        if (this.state.userBio) {
          bg_url = this.state.userBio.bg_url;
           {
            description = this.state.userBio.description ? (
              <div className={"bio-description" + this.state.dark}>
                {this.state.userBio.description}
              </div>
            ) : (
              <div className="bio-description">No description provided</div>
            );
          }
          flag = (
            <div className="bio-flag">
              {this.state.userBio.country ? (
                <img
                  className="bio-flag-img"
                  title={this.state.userBio.country}
                  alt={this.state.userBio.country}
                  src={
                    "https://www.countryflags.io/" +
                    this.state.userBio.country +
                    "/flat/64.png"
                  }
                />
              ) : null}
            </div>
          );
        } else if (this.state.userBioError) {
          description = (
            <div className={"bio-description" + this.state.dark}>
              {this.state.userBioError}
            </div>
          );
        } else {
          description = (
            <Spin
              indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
            />
          );
        }

        if (this.state.questions) {
          if (this.state.questions.length < 1) {
            questions = "This user has not asked any questions.";
          } else {
            var that = this;
            questions = map(this.state.questions, function(item, i) {
              var createdUTC = moment(item.created).utc();
              var localTime = moment(createdUTC)
                .local()
                .format("MMMM Do YYYY, h:mm:ss a");
              return (
                <div
                  key={i}
                  className={"bio-individual-question" + that.state.dark}
                  onClick={() => {
                    let safeTitle = item.title
                      .split(" ")
                      .join("-")
                      .toLowerCase()
                      .split("?")
                      .join("-")
                      .split("'")
                      .join("");
                    if (safeTitle[safeTitle.length - 1] === "-") {
                      safeTitle = safeTitle.slice(0, safeTitle.length - 1);
                    }
                    that.props.history.push(
                      encodeURI("/post/" + item.q_id + "/" + safeTitle)
                    );
                  }}
                >
                  <div
                    className={
                      "bio-individual-question-title" + that.state.dark
                    }
                  >
                    {item.title}
                  </div>
                  <div className={"bio-answer-date" + that.state.dark}>
                    {localTime}
                  </div>
                </div>
              );
            });
          }
        } else if (this.state.questionsError) {
          questions = this.state.questionsError;
        } else {
          questions = (
            <Spin
              indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
            />
          );
        }
        var that = this;
        if (this.state.answers) {
          if (this.state.answers.length < 1) {
            answers = "This user has not answered any questions.";
          } else {
            answers = map(this.state.answers, function(item, i) {
              var createdUTC = moment(item.created).utc();
              var localTime = moment(createdUTC)
                .local()
                .format("MMMM Do YYYY, h:mm:ss a");
              return (
                <div
                  key={i}
                  className={"bio-individual-answer" + that.state.dark}
                  onClick={() => {
                    let safeTitle = item.title
                      .split(" ")
                      .join("-")
                      .toLowerCase()
                      .split("?")
                      .join("-")
                      .split("'")
                      .join("");
                    if (safeTitle[safeTitle.length - 1] === "-") {
                      safeTitle = safeTitle.slice(0, safeTitle.length - 1);
                    }
                    that.props.history.push(
                      encodeURI(
                        "/post/" + item.q_id + "/" + safeTitle + "/" + item.a_id
                      )
                    );
                  }}
                >
                  <div
                    className={
                      "bio-individual-answer-question-title" + that.state.dark
                    }
                  >
                    {item.title}
                  </div>
                  <div
                    style={{
                      marginRight: "0.5em",
                      marginBottom: "0.2em",
                      display: "inline-block"
                    }}
                  >
                    {that.state.userDetails.dp_url ? (
                      <Avatar src={that.state.userDetails.dp_url} />
                    ) : (
                      <Avatar icon="user" />
                    )}
                  </div>
                  <div
                    className={"bio-individual-answer-name" + that.state.dark}
                  >
                    {that.state.userDetails.fname +
                      " " +
                      (that.state.userDetails.lname||"")}{" "}
                  </div>
                  <div className={"bio-answer-date" + that.state.dark}>
                    {localTime}
                  </div>
                  <div
                    className={"bio-individual-answer-answer" + that.state.dark}
                    dangerouslySetInnerHTML={{
                      __html: sanitizeHtml(item.answer, {
                        allowedTags: [
                          "p",
                          "strong",
                          "em",
                          "s",
                          "blockquote",
                          "ol",
                          "ul",
                          "li",
                          "a",
                          "img",
                          "pre",
                          "span"
                        ],
                        allowedAttributes: {
                          a: ["href", "rel", "target"],
                          img: ["src", "rel"],
                          pre: ["class", "spellcheck"],
                          span: ["class"]
                        }
                      })
                    }}
                  />
                </div>
              );
            });
          }
        } else if (this.state.answersError) {
          answers = this.state.answersError;
        } else {
          answers = (
            <Spin
              indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
            />
          );
        }

        if (this.state.userArticles) {
          if (this.state.userArticles.length < 1) {
            articles = "This user has not written any articles.";
          } else {
            articles = map(this.state.userArticles, function(item, i) {
              var createdUTC = moment(item.created).utc();
              var localTime = moment(createdUTC)
                .local()
                .format("MMMM Do YYYY, h:mm:ss a");
              return (
                <div
                  key={i}
                  className={"bio-individual-answer" + that.state.dark}
                  onClick={() => {
                    let safeTitle = item.title
                      .split(" ")
                      .join("-")
                      .toLowerCase()
                      .split("?")
                      .join("-")
                      .split("'")
                      .join("");
                    if (safeTitle[safeTitle.length - 1] === "-") {
                      safeTitle = safeTitle.slice(0, safeTitle.length - 1);
                    }
                    that.props.history.push(
                      encodeURI("/article/" + item.a_id + "/" + safeTitle)
                    );
                  }}
                >
                  <div
                    className={
                      "bio-individual-answer-question-title" + that.state.dark
                    }
                  >
                    {item.title}
                  </div>
                  <div
                    style={{
                      marginRight: "0.5em",
                      marginBottom: "0.2em",
                      display: "inline-block"
                    }}
                  >
                    {that.state.userDetails.dp_url ? (
                      <Avatar src={that.state.userDetails.dp_url} />
                    ) : (
                      <Avatar icon="user" />
                    )}
                  </div>
                  <div
                    className={"bio-individual-answer-name" + that.state.dark}
                  >
                    {that.state.userDetails.fname +
                      " " +
                      (that.state.userDetails.lname||"")}{" "}
                  </div>
                  <div className={"bio-answer-date" + that.state.dark}>
                    {localTime}
                  </div>
                  <div
                    className={"bio-individual-answer-answer" + that.state.dark}
                    dangerouslySetInnerHTML={{
                      __html: sanitizeHtml(item.content, {
                        allowedTags: [
                          "p",
                          "strong",
                          "em",
                          "s",
                          "blockquote",
                          "ol",
                          "ul",
                          "li",
                          "a",
                          "img",
                          "pre",
                          "span"
                        ],
                        allowedAttributes: {
                          a: ["href", "rel", "target"],
                          img: ["src", "rel"],
                          pre: ["class", "spellcheck"],
                          span: ["class"]
                        }
                      })
                    }}
                  />
                </div>
              );
            });
          }
        } else if (this.state.answersError) {
          articles = this.state.answersError;
        } else {
          articles = (
            <Spin
              indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
            />
          );
        }

        if (this.state.userOtherDetails) {
          numAnswers = (
            <div className="num-answers-outer-div">
              <table className="num-answers-table">
                <tbody>
                  <tr>
                    <td style={{ width: "50%" }} alight="left">
                      <div>Answers</div>
                    </td>
                    <td style={{ width: "50%" }} alighn="right">
                      <div>{this.state.userOtherDetails.num_answers}</div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          );
          numQuestions = (
            <div className="num-questions-outer-div">
              <table className="num-answers-table">
                <tbody>
                  <tr>
                    <td style={{ width: "50%" }} alight="left">
                      <div>Questions</div>
                    </td>
                    <td style={{ width: "50%" }} alighn="right">
                      <div>{this.state.userOtherDetails.num_questions}</div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          );
          numArticles = (
            <div className="num-questions-outer-div">
              <table className="num-answers-table">
                <tbody>
                  <tr>
                    <td style={{ width: "50%" }} alight="left">
                      <div>Articles</div>
                    </td>
                    <td style={{ width: "50%" }} alighn="right">
                      <div>{this.state.userOtherDetails.num_articles}</div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          );
        }

        messageLink = "Message";

        switch (this.state.content) {
          case 1: {
            content = (
              <div className={"article-editor-div" + this.state.dark}>
                {this.state.content === 1 &&
                this.state.uid + "" === this.props.cookies.get("u_id") ? (
                  <ArticleEditor uId={this.state.qAuthor} />
                ) : null}
                {articles}
              </div>
            );
            break;
          }
          case 2: {
            content = questions;
            break;
          }
          case 3: {
            content = answers;
            break;
          }
          default: {
            content = articles;
          }
        }
      }
    }

    let bgImageContent = "";
    if (this.state.bgError) {
      bgImageContent = <Alert type="error" msg={this.state.bgError} />;
    } else if (this.state.bgData) {
      bgImageContent = map(this.state.bgData, (item, i) => {
        return (
          <div
            key={i}
            onClick={() => {
              this.handleBgChange(item.bg_id, item.bg_url);
            }}
            className="bg-individual-image-div"
            style={{ backgroundImage: 'url("' + item.bg_url + '")' }}
          />
        );
      });

      bgImageContent = (
        <div style={{ marginTop: "1em", overflowY: "auto" }}>
          {bgImageContent}
        </div>
      );
    } else {
      bgImageContent = (
        <div style={{ textAlign: "center", width: "100%" }}>
          <Spin
            indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
          />
        </div>
      );
    }

    let bgImageButton;
    if (this.state.uid && this.props.cookies.get("u_id")) {
      if (this.state.uid + "" === this.props.cookies.get("u_id")) {
        bgImageButton = (
          <div className="change-cover-div">
            <span onClick={this.handleBgChooser}>
              <Icon type="edit" /> Change Cover
            </span>
          </div>
        );
      }
    }

    return (
      <React.Fragment>
        <Helmet><title>Clevereply</title></Helmet>
        <Header translucent={true} />
        <div className="public-profile-main-div">
          <Row className="profile-first-row">
            <Col span={24} className={"first-col" + this.state.dark}>
              <div className={"bio-first-col" + this.state.dark}>
                <div className="dp-outer-div">
                  <div
                    className="user-data-dp-div"
                    style={{ backgroundImage: 'url("' + dp_url + '")' }}
                  >
                    {this.state.uid &&
                    this.state.uid + "" ===
                      this.props.cookies.get("u_id") ? null : (
                      this.state.userDetailsError?null:(<div className="public-profile-send-message">
                        <span
                          onClick={this.handleMessageClick.bind(this)}
                          className={"message-span profile" + this.state.dark}
                        >
                          <img
                            className="message profile"
                            src={messageIcon}
                            alt="message"
                          />
                        </span>
                        {this.state.userDetails &&
                        this.state.userDetails.following ? (
                          <div
                          onClick={()=>{this.unfollowUser(this.state.uid)}}
                            className={
                              "message-span profile follow following" + this.state.dark
                            }
                          >
                            Following
                            <div className="follow-tick-mark">
                              <Icon type="check" />
                            </div>
                          </div>
                        ) : (
                          <div
                          onClick={()=>{this.followUser(this.state.uid)}}
                            className={
                              "message-span profile follow" + this.state.dark
                            }
                          >
                            Follow
                          </div>
                        )}
                      </div>)
                    )}
                  </div>
                </div>
                {description}
                {flag}
                <div className={"profile-user-created"+this.state.dark}>{created}</div>
                <div className={"profile-links" + this.state.dark}>
                  <div
                    onClick={this.handleBioClick.bind(this)}
                    className="bio-link"
                  >
                    {numArticles}
                  </div>
                  <div
                    onClick={this.handleQuestionsClick.bind(this)}
                    className="num-questions"
                  >
                    {numQuestions}
                  </div>
                  <div
                    onClick={this.handleAnswersClick.bind(this)}
                    className="num-answers"
                  >
                    {numAnswers}
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <div className={"public-profile-main-wrapper" + this.state.dark} />
        <Row>
          <Col span={24}>
            <div
              className="top-cover-content-div"
              style={{
                backgroundImage: 'url("' + bg_url + '")',
                backgroundColor: !bg_url ? "#5f9ea0" : null
              }}
            >
              {bgImageButton}
              <div className="top-cover-content-div-inner">
                <div
                  className="user-data-dp-div-mobile"
                  style={{ backgroundImage: 'url("' + dp_url + '")' }}
                >
                  {/* {flag} */}
                  {this.state.uid &&
                    this.state.uid + "" ===
                      this.props.cookies.get("u_id") ? null : (
                      <div className="public-profile-send-message">
                        <span
                          onClick={this.handleMessageClick.bind(this)}
                          className={"message-span profile" + this.state.dark}
                        >
                          <img
                            className="message profile"
                            src={messageIcon}
                            alt="message"
                          />
                        </span>
                        {this.state.userDetails &&
                        this.state.userDetails.following ? (
                          <div
                          onClick={()=>{this.unfollowUser(this.state.uid)}}
                            className={
                              "message-span profile follow following" + this.state.dark
                            }
                          >
                            Following
                            <div className="follow-tick-mark">
                              <Icon type="check" />
                            </div>
                          </div>
                        ) : (
                          <div
                          onClick={()=>{this.followUser(this.state.uid)}}
                            className={
                              "message-span profile follow" + this.state.dark
                            }
                          >
                            Follow
                          </div>
                        )}
                        {/* <div
                          onClick={this.handleMessageClick.bind(this)}
                          className="public-profile-send-message-div"
                        >
                          {messageLink}
                        </div> */}
                      </div>
                    )}
                </div>
                {name}
                {points ? (
                  <div className="user-points">
                    <div className="user-points-points">{points}</div> <div className="user-rating">{rate}</div>
                  </div>
                ) : null}
                <div className="profile-description-mobile">
                {description}
                {flag}
                </div>
              </div>
            </div>
            <Button
              className="bio-links-mobile"
              style={{
                width: "33.333333%",
                padding: "0.2em",
                textAlign: "center",
                borderRadius: "0",
                width: "calc(100%  /3)"
              }}
            >
              <div
                onClick={this.handleBioClick.bind(this)}
                className="bio-link"
              >
                {numArticles}
              </div>
            </Button>
            <Button
              className="bio-links-mobile"
              style={{
                width: "33.333333%",
                padding: "0.2em",
                textAlign: "center",
                borderRadius: "0",
                width: "calc(100%  /3)"
              }}
            >
              <div
                onClick={this.handleQuestionsClick.bind(this)}
                className="num-questions"
              >
                {numQuestions}
              </div>
            </Button>
            <Button
              className="bio-links-mobile"
              style={{
                width: "33.333333%",
                padding: "0.2em",
                textAlign: "center",
                borderRadius: "0",
                width: "calc(100%  /3)"
              }}
            >
              <div
                onClick={this.handleAnswersClick.bind(this)}
                className="num-answers"
              >
                {numAnswers}
              </div>
            </Button>
          </Col>
          <Col span={24} className={"bio-content-row" + this.state.dark}>
            <div className="bio-content">{content}</div>
            <div className="bio-content">{error}</div>
          </Col>
        </Row>
        <Modal
          onCancel={this.handleMessageModalCancel}
          onOk={this.handleSendMessage}
          okText="Send"
          visible={this.state.messageModalVisible}
          confirmLoading={this.state.messageDisabled}
          title="Send message"
        >
          <Input.TextArea
            disabled={this.state.messageDisabled}
            autosize
            placeholder="Type your message"
            value={this.state.messageText}
            onChange={this.handleMessageChange}
          />
          <p />
          {this.state.messageError ? (
            <Alert type="error" message={this.state.messageError} />
          ) : null}
        </Modal>
        <Modal />
        <Modal
          visible={this.state.loginModalVisible}
          footer={null}
          onCancel={() => this.setState({ loginModalVisible: false })}
        >
          <PopupLogin login />
        </Modal>
        <Modal
          title={"Cover Theme"}
          footer={null}
          width={"90%"}
          visible={this.state.bgChooserVisible}
          onCancel={() => {
            this.setState({ bgChooserVisible: false });
          }}
        >
          {bgImageContent}
        </Modal>
      </React.Fragment>
    );
  }
}

export default withRouter(withCookies(PublicProfile));
