import React from "react";
import "./Vote.css";
import axios from "axios";
import { withCookies } from "react-cookie";
import { message, Modal } from "antd";
import PopupLogin from "./PopupLogin"

class Vote extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.backUrl = "";
  }

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    this.setState({
      voteCount: this.props.voteCount || 0,
      voteOriginalCount: this.props.voteCount || 0,
      aId: this.props.aId
    });
  }

  updateVal(type) {
    if (this.state.aId) {
      let voteOriginalCount = this.state.voteOriginalCount
      let voteCount = this.state.voteCount
      if (type === +1) {
        if (this.state.voteOriginalCount === this.state.voteCount) {
          this.setState({
            voteCount: this.state.voteCount + 1,
            glowUp: true,
            glowDown: false
          });
        } else {
          this.setState({
            voteCount: this.state.voteCount + 2,
            glowUp: true,
            glowDown: false
          });
        }
      } else if (type === -1) {
        if (this.state.voteOriginalCount === this.state.voteCount) {
          this.setState({
            voteCount: this.state.voteCount - 1,
            glowDown: true,
            glowUp: false
          });
        } else {
          this.setState({
            voteCount: this.state.voteCount - 2,
            glowDown: true,
            glowUp: false
          });
        }
      }
      axios({
        url: this.backUrl + "voteAnswer",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          session_id: this.props.cookies.get("session_id")
        },
        data: JSON.stringify({
          a_id: this.state.aId,
          vote_type: type
        })
      })
        .then(res => {
          if (res.data && res.data.success) {
            if (type === +1) {
              if (voteOriginalCount === voteCount) {
                
              } else {
                
              }
            } else if (type === -1) {
              if (voteOriginalCount === voteCount) {
                
              } else {
                
              }
            }
          } else {
            if (res.data && res.data.msg) {
              if (res.data.msg === "You need to login to continue.") {
                message.warn(res.data.msg);
                if (type === +1) {
                  if (voteOriginalCount === voteCount) {
                    this.setState({
                      voteCount: this.state.voteCount - 1,
                      glowUp: false,
                      glowDown: false
                    });
                  } else {
                    this.setState({
                      voteCount: this.state.voteCount - 2,
                      glowUp: false,
                      glowDown: false
                    });
                  }
                } else if (type === -1) {
                  if (voteOriginalCount === voteCount) {
                    this.setState({
                      voteCount: this.state.voteCount + 1,
                      glowDown: false,
                      glowUp: false
                    });
                  } else {
                    this.setState({
                      voteCount: this.state.voteCount + 2,
                      glowDown: false,
                      glowUp: false
                    });
                  }
                }
                let cookiesObj = this.props.cookies.getAll();
                for (var key in cookiesObj) {
                  this.props.cookies.remove(key, { path: "/" });
                }
              } else if (res.data.msg === "You have already up-voted.") {
                message.warning(res.data.msg);
                this.setState({
                  voteOriginalCount: this.state.voteOriginalCount - 1,
                  voteCount: this.state.voteCount - 1
                });
              } else if (res.data.msg === "You have already down-voted.") {
                message.warning(res.data.msg);
                this.setState({
                  voteOriginalCount: this.state.voteOriginalCount + 1,
                  voteCount: this.state.voteCount - 1
                });
              } else {
                message.error(res.data.msg);
                if (type === +1) {
                  if (voteOriginalCount === voteCount) {
                    this.setState({
                      voteCount: this.state.voteCount - 1,
                      glowUp: false,
                      glowDown: false
                    });
                  } else {
                    this.setState({
                      voteCount: this.state.voteCount - 2,
                      glowUp: false,
                      glowDown: false
                    });
                  }
                } else if (type === -1) {
                  if (voteOriginalCount === voteCount) {
                    this.setState({
                      voteCount: this.state.voteCount + 1,
                      glowDown: false,
                      glowUp: false
                    });
                  } else {
                    this.setState({
                      voteCount: this.state.voteCount + 2,
                      glowDown: false,
                      glowUp: false
                    });
                  }
                }
              }
            } else {
              message.error("Something went wrong. Please try again later.");
              if (type === +1) {
                if (voteOriginalCount === voteCount) {
                  this.setState({
                    voteCount: this.state.voteCount - 1,
                    glowUp: false,
                    glowDown: false
                  });
                } else {
                  this.setState({
                    voteCount: this.state.voteCount - 2,
                    glowUp: false,
                    glowDown: false
                  });
                }
              } else if (type === -1) {
                if (voteOriginalCount === voteCount) {
                  this.setState({
                    voteCount: this.state.voteCount + 1,
                    glowDown: false,
                    glowUp: false
                  });
                } else {
                  this.setState({
                    voteCount: this.state.voteCount + 2,
                    glowDown: false,
                    glowUp: false
                  });
                }
              }
            }
          }
        })
        .catch(e => {
          message.error("Something went wrong. Please try again later.");
          if (type === +1) {
            if (voteOriginalCount === voteCount) {
              this.setState({
                voteCount: this.state.voteCount - 1,
                glowUp: false,
                glowDown: false
              });
            } else {
              this.setState({
                voteCount: this.state.voteCount - 2,
                glowUp: false,
                glowDown: false
              });
            }
          } else if (type === -1) {
            if (voteOriginalCount === voteCount) {
              this.setState({
                voteCount: this.state.voteCount + 1,
                glowDown: false,
                glowUp: false
              });
            } else {
              this.setState({
                voteCount: this.state.voteCount + 2,
                glowDown: false,
                glowUp: false
              });
            }
          }
        });
    }
  }

  callUpdate(type) {
    if (!this.props.cookies.get("session_id")) {
      this.setState({
        loginModalVisible: true
      })
      this.props.blur && this.props.blur(true)
      return;
    }
    if (this.props.uId === this.props.aUid + "") {
      message.warning("You cannot vote your own answer.");
      return;
    }
    if (type === +1) {
      if (
        this.state.voteOriginalCount === this.state.voteCount ||
        this.state.voteOriginalCount - this.state.voteCount === 1
      ) {
        //do it
        this.updateVal(+1);
      }
    }
    if (type === -1) {
      if (
        this.state.voteOriginalCount === this.state.voteCount ||
        this.state.voteCount - this.state.voteOriginalCount === 1
      ) {
        //do it
        this.updateVal(-1);
      }
    }
  }

  render() {
    var vc = this.state.voteCount ? this.state.voteCount : 0;
    if(this.props.feed) {
      return <div style={{marginTop:"1em"}}>
        <div className="feed-upvote-div"  onClick={() => this.callUpdate(+1)}>
        <div
          className={this.state.glowUp ? "feed-arrow-up-glow" : ("feed-arrow-up"+this.state.dark)}
          
        ></div>
        <div className={"feed-upvote-div-text"+(this.state.glowUp?" glow":"")}>
        Upvote
        </div>
        </div>
        <div
          className={this.state.glowDown ? "feed-arrow-down-glow" : ("feed-arrow-down"+this.state.dark)}
          onClick={() => this.callUpdate(-1)}
        />
      </div>
    } else
    return (
      <React.Fragment>
        <div
          className={this.state.glowUp ? "arrow-up-glow" : ("arrow-up"+this.state.dark)}
          onClick={() => this.callUpdate(+1)}
        />
        <div style={{ paddingBottom: "2em" }} />
        <div
          className={
            vc < 0
              ? ("answer-vote-negative"+this.state.dark)
              : vc === 0
              ? ("answer-vote"+this.state.dark)
              : ("answer-vote-positive"+this.state.dark)
          }
        >
          {vc}
        </div>
        <div style={{ paddingBottom: "2em" }} />
        <div
          className={this.state.glowDown ? "arrow-down-glow" : ("arrow-down"+this.state.dark)}
          onClick={() => this.callUpdate(-1)}
        />
        <Modal
          visible={this.state.loginModalVisible}
          footer={null}
          onCancel={() => {this.setState({ loginModalVisible: false })
          this.props.blur && this.props.blur(false)
        }}
        >
          <PopupLogin login />
        </Modal>
      </React.Fragment>
    );
  }
}

export default withCookies(Vote);
