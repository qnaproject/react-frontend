import React, { Component } from "react";
import "./Terms.css";

class Terms extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <div className="terms-main-div">
        <div className="terms-inner-div">
            <a href="/terms">Terms and Conditions</a> <span className="terms-divider-span">|</span>
            <a href="/privacy">Privacy Policy</a> <span className="terms-divider-span">|</span>
            <a href="/contact">Contact</a>
            <p>site design / logo &copy; 2019 clevereply.com</p>
        </div>
    </div>;
  }
} 

export default (Terms)