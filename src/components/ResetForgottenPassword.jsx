import React, { Component } from "react";
import "./ResetForgottenPassword.css";
import axios from "axios";
import { Alert, Spin, Icon, Input, Button } from "antd";

class ResetForgottenPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.backUrl = ""
  }

  componentDidMount() {
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    let key = window.location.pathname.split("/")[2];
    axios({
      url: this.backUrl + "verifyPasswordResetKey",
      method: "POST",
      headers: {
        "Content-type": "text/plain"
      },
      data: {
        verify_key: key
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            doneInitial: true,
            key: key
          });
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              error: res.data.msg
            });
          } else {
            this.setState({
              error: "Something went wrong. Please try again later."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          error: "Something went wrong. Please try again later."
        });
      });
  }

  handlePasswordChange = val => {
    this.setState({
      password: val.target.value,
      dataError: null
    });
  };

  handleConfirmPasswordChange = val => {
    this.setState({
      confirmPassword: val.target.value,
      dataError: null
    });
  };

  resetPassword = () => {
    if (!this.state.password) {
      this.setState({
        dataError: "Please enter a valid password"
      });
      return;
    }
    if (!this.state.confirmPassword) {
      this.setState({
        dataError: "Please confirm the password"
      });
      return;
    }
    if (this.state.password !== this.state.confirmPassword) {
      this.setState({
        dataError: "The passwords do not match."
      });
      return;
    }
    this.setState({
      disabled: true
    });
    axios({
      url: this.backUrl + "resetPassword",
      method: "POST",
      headers: {
        "Content-type": "text/plain"
      },
      data: {
        password: this.state.password,
        re_password: this.state.confirmPassword,
        verify_key: this.state.key
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          setTimeout(() => {
            this.props.history.replace("/");
            this.props.history.push("/login");
          }, 3000);
          this.setState({
            success: res.data.msg
          });
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              dataError: res.data.msg,
              disabled: false
            });
          } else {
            this.setState({
              dataError: "Something went wrong. Please try again later.",
              disabled: false
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          dataError: "Something went wrong. Please try again later.",
          disabled: false
        });
      });
  };

  render() {
    let content;
    if (this.state.success) {
      content = (
        <div>
          <Alert type="success" message={this.state.success} />
          <p />
          <p />
          Redirecting you to the login page...
        </div>
      );
    } else if (!this.state.error && this.state.doneInitial) {
      content = (
        <div>
          <h2> Reset Password</h2>
          <div className="reset-forgot-pwd-input-div">
            <Input.Password
              onPressEnter={this.resetPassword}
              disabled={this.state.disabled}
              value={this.state.password}
              onChange={this.handlePasswordChange}
              placeholder="New password"
            />
          </div>
          <div className="reset-forgot-pwd-input-div">
            <Input.Password
              onPressEnter={this.resetPassword}
              disabled={this.state.disabled}
              value={this.state.confirmPassword}
              onChange={this.handleConfirmPasswordChange}
              placeholder="Confirm password"
            />
          </div>
          <div>
            <Button onClick={this.resetPassword}>Reset</Button>
          </div>
        </div>
      );
    } else if (!this.state.error && !this.state.doneInitial) {
      content = (
        <center>
          <Spin
            indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
          />
        </center>
      );
    }

    return (
      <div className="reset-forgot-pwd-main-div">
        <div className="reset-forgot-pwd-inner-div">
          {this.state.error ? (
            <div>
              <Alert type="error" message={this.state.error} />
            </div>
          ) : null}
          {content}
          {this.state.dataError ? (
            <div style={{ paddingTop: "1em" }}>
              <Alert type="error" message={this.state.dataError} />
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

export default ResetForgottenPassword;
