import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import { Row, Col, Input, Icon, Button, Alert, message } from "antd";
import { withCookies } from "react-cookie";
import axios from "axios";
import { withLastLocation } from "react-router-last-location";
import "./Login.css";
import moment from "moment"

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: false };
    this.backUrl = ""
  }

  componentDidMount() {
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    if (this.props.cookies.get("session_id")) {
      this.props.history.push(
        this.props.lastLocation && this.props.lastLocation.pathname
          ? this.props.lastLocation.pathname
          : "/"
      );
    }
  }

  emailChange = e => {
    this.setState({email: e.target.value, error: null, enableResendVerifcationLink: false, successMsg: null });
  };

  passwordChange = e => {
    this.setState({ ...this.state, password: e.target.value, error: null, successMsg: null });
  };

  clearEmail = () => {
    this.setState({
      ...this.state,
      email: null,
      error: null,
      successMsg: null
    });
    this.emailRef.focus();
  };

  setError = e => {
    this.setState({
      ...this.state,
      error: e ? e : "Something went wrong. Please try again later."
    });
  };

  login = e => {
    if (!this.state.email) {
      this.setState({
        ...this.state,
        error: "Please enter a valid email."
      });
      return;
    }
    if (this.state.email && this.state.email.length < 1) {
      this.setState({
        ...this.state,
        error: "Please enter a valid email."
      });
      return;
    }
    if (!this.state.password) {
      this.setState({
        ...this.state,
        error: "Please enter a valid password."
      });
      return;
    }
    if (this.state.password && this.state.password.length < 8) {
      this.setState({
        ...this.state,
        error: "Please enter a valid password."
      });
      return;
    }
    if (this.state.password && this.state.password.length > 20) {
      this.setState({
        ...this.state,
        error: "Please enter a valid password."
      });
      return;
    }
    this.setState({
      loading: true,
      error: null,
      successMsg: null
    });
    axios({
      url: this.backUrl + "login",
      method: "POST",
      headers: {
        "Content-type": "text/plain"
      },
      data: {
        email: this.state.email,
        password: this.state.password,
        keep_logged_in: true
      }
    })
      .then(res => {
        if (res.data) {
          if (res.data.success) {
            if(res.data.data) {
              this.props.cookies.set("session_id", res.data.data.session_id, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
            this.props.cookies.set("name", res.data.data.name, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
            this.props.cookies.set("pro", res.data.data.pro, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
            this.props.cookies.set("u_id", res.data.data.UId, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
            this.props.cookies.set("role", res.data.data.Role, {
              path: "/", expires: moment().add(30, 'days').toDate(), secure: true
            });
            this.props.history.push(
              this.props.lastLocation && this.props.lastLocation.pathname
                ? this.props.lastLocation.pathname
                : "/"
            );
            } else {
              message.info(res.data.msg)
              this.setState({
                loading: false
              });
            }
          } else {
            if (res.data.msg) {
              if (
                res.data.msg ===
                "You have not verified your email address. Please verify to log in."
              ) {
                this.setState({
                  enableResendVerifcationLink: true
                });
              }
              this.setState({
                loading: false,
                error: res.data.msg
              });
            } else {
              this.setState({
                loading: false,
                error: "Something went wrong. Please try again later"
              });
            }
          }
        } else {
          this.setState({
            loading: false,
            error: "Something went wrong. Please try again later"
          });
        }
      })
      .catch(e => {
        this.setState({
          loading: false,
          error: "Something went wrong. Please try again later"
        });
      });
  };

  handleForgotPassword = () => {
    if (this.state.email && this.state.email.length > 0) {
      this.setState({
        successMsg: null,
        error: null
      })
      message.loading("Please wait.", 0);
      axios({
        url: this.backUrl + "sendPasswordResetEmail",
        method: "POST",
        headers: {
          "Content-type": "text/plain"
        },
        data: {
          email: this.state.email
        }
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            message.destroy();
            this.setState({
              successMsg: res.data.msg
            });
          }
        })
        .catch(e => {});
    } else {
      this.setState({
        error: "Please enter a valid email."
      });
    }
  };

  handleResendVerificationEmail = () => {
    if(this.state.disableResendMail) {
      return
    }
    if (!this.state.email || this.state.email.length < 1) {
      this.setState({
        error: "Please enter a valid email."
      });
      return;
    }
    this.setState({
      successMsg: null,
      error: null,
      disableResendMail: true
    })
    axios({
      url: this.backUrl + "resendVerificationMail",
      headers: {
        "Content-type": "text/plain"
      },
      method: "POST",
      data: {
        email: this.state.email
      }
    })
    .then(res => {
      if(res && res.data && res.data.success) {
        this.setState({
          successMsg: res.data.msg,
          disableResendMail: true
        });
      } else {
        if(res && res.data && res.data.msg) {
          this.setState({
            error: res.data.msg,
            disableResendMail: false
          });
        } else {
          this.setState({
            error:
              "Something went wrong while sending verification link. Please try again later.",
            disableResendMail: false
          });
        }
      }
    })
    .catch(e => {
      this.setState({
        error:
          "Something went wrong while sending verification link. Please try again later.",
        disableResendMail: false
      });
    })
  };

  render() {
    const suffixIcon = this.state.email ? (
      <Icon type="close-circle" onClick={this.clearEmail} />
    ) : null;
    return (
      <Row type="flex" justify="center" className="login-row" align="middle">
        <Col
          xs={24}
          sm={24}
          md={18}
          lg={16}
          xl={14}
          xxl={8}
          className="login-col"
        >
          <h1 className="welcome-text">Welcome Back</h1>
          <div className="login-button-div">
            <Input
              autoFocus
              className="login-button"
              required
              placeholder="Enter your email"
              prefix={<Icon type="user" />}
              value={this.state.email}
              onChange={this.emailChange}
              suffix={suffixIcon}
              ref={ref => (this.emailRef = ref)}
              onPressEnter={this.login.bind(this)}
            />
            <Input.Password
              className="login-button"
              required
              placeholder="Enter your password"
              prefix={<Icon type="lock" />}
              onChange={this.passwordChange}
              onPressEnter={this.login.bind(this)}
            />
          </div>

          <a href="javascript:;" onClick={this.handleForgotPassword.bind(this)}>
            <p>Forgot Password?</p>
          </a>
          <Link to="/SignUp">
            <p>
              Don't have an account? Click here to register (it only takes a
              moment)
            </p>
          </Link>
          <Button loading={this.state.loading} onClick={this.login.bind(this)}>
            Log In
          </Button>

          {this.state.error ? (
            <div style={{ padding: "1em" }}>
              <Alert type="error" message={this.state.error} />
            </div>
          ) : null}
          <p/>
          <div>
            {this.state.enableResendVerifcationLink ? (
              <a
                href="javascript:;"
                onClick={this.handleResendVerificationEmail.bind(this)}
              >
                <p>Resend email verification link?</p>
              </a>
            ) : null}
          </div>
          {this.state.successMsg ? (
            <div style={{ padding: "1em" }}>
              <Alert type="success" message={this.state.successMsg} />
            </div>
          ) : null}
        </Col>
      </Row>
    );
  }
}

export default withRouter(withCookies(withLastLocation(Login)));
