import React, { Component } from "react";
import FacebookLogin1 from 'react-facebook-login/dist/facebook-login-render-props';
import "./FacebookLogin.css";
import axios from "axios"
import { withCookies } from "react-cookie";
import {message, Icon} from "antd"
import ficon from "./res/f_icon_reversed.svg"
import moment from "moment"

class FacebookLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
 
  componentDidMount() {
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
  }

  onSuccess = response => {
    message.loading("Please wait", 0)
    this.login(response.accessToken);
  };

  login = token => {
    axios({
      url: this.backUrl + "continueWithFacebook",
      method: "POST",
      headers: {
        "Content-type": "text/plain"
      },
      data: {
        "token":token
      }
    })
      .then(res => {
        message.destroy()
        if (res.data) {
          if (res.data.success) {
            if(res.data.data) {
              this.props.cookies.set("session_id", res.data.data.session_id, {
                path: "/"
              });
              this.props.cookies.set("name", res.data.data.name, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
              this.props.cookies.set("pro", res.data.data.pro, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
              this.props.cookies.set("u_id", res.data.data.UId, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
              this.props.cookies.set("role", res.data.data.Role, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
              window.location.reload()
            } else {
              message.info(res.data.msg)
            }
          } else {
            if (res.data.msg) {
              message.error(
                res.data.msg
              );
            } else {
              message.error(
                "Something went wrong. Please try again later"
              );
            }
          }
        } else {
          message.error(
            res.data.msg || "Something went wrong. Please try again later"
          );
        }
      })
      .catch(e => {
        message.destroy()
        message.error("Something went wrong. Please try again later");
      });
  };

  responseFacebook = (response) => {
    if(response && response.accessToken && response.email && response.userID) {
      this.onSuccess(response)
    } else {
      message.error("Something went wrong. Please try again later");
    }
  }

  render() {
    return (
        <FacebookLogin1
        callback={this.responseFacebook}
        fields="name,email"
          appId={process.env.REACT_APP_API_FACEBOOK_CLIENT_ID}
          render={renderProps => (
            <div className={"facebook-button"+(renderProps.isSdkLoaded?"":" disabled")}>
            <div onClick={()=>{(renderProps.isSdkLoaded && renderProps.onClick())}} className="facebook-login-inner-div">
            <img className="facebook-login-icon" src={ficon}/>
            <span>Continue with Facebook</span></div></div>
          )}
        />
    );
  }
}

export default withCookies(FacebookLogin);
