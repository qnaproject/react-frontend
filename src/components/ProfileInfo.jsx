import React, { Component } from "react";
import {
  Button,
  Badge,
  Alert,
  Popover,
  message,
  Drawer,
  Spin,
  Icon,
  Input,
  Row,
  Col,
  Modal,
  Switch,
  Avatar
} from "antd";
import ButtonGroup from "antd/lib/button/button-group";
import "./ProfileInfo.css";
import { withRouter } from "react-router-dom";
import { withCookies } from "react-cookie";
import arrow from "./res/down-open-arrow.svg";
import notif from "./res/notif.svg";
import messageIcon from "./res/message.svg";
import axios from "axios";
import { map, cloneDeep, sortBy, filter, reverse } from "lodash";
import moment from "moment";
import PopupLogin from "./PopupLogin";
import upvoteIcon from "./res/upvote_notif_icon.png";
import downvoteIcon from "./res/downvote_notif_icon.png";
import commentIcon from "./res/comment_notif_icon.png";
import likeIcon from "./res/like_notif_icon.png";
import answerIcon from "./res/answer_notif_icon.png";
import deleteIcon from "./res/delete_notif_icon.png";
import followIcon from "./res/follow_notif_icon.png";
import sendButton from "./res/send-button.png";

class ProfileInfo extends Component {
  constructor(props) {
    super(props);
    this.convoRef = null;
    this.state = {};
    this.backUrl = "";
  }

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : "",
      darkChecked: this.props.cookies.get("dark") === "true" ? true : false,
      showConsent: ((this.props.cookies.get("consented") === "true")|| this.props.cookies.get("session_id")) ? false : true
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    var sessionId = this.props.cookies.get("session_id");
    if (sessionId && sessionId.length > 0) {
      if (!this.props.cookies.get("name") || !this.props.cookies.get("u_id")) {
        let cookiesObj = this.props.cookies.getAll();
        for (var key in cookiesObj) {
          this.props.cookies.remove(key, { path: "/" });
        }
        window.location.reload();
      }
      this.setState({
        name: this.props.cookies.get("name"),
        sessionId: this.props.cookies.get("session_id")
      });
      this.getMessagesAndNotifications(sessionId);
    }
  }

  componentDidUpdate() {
    if (this.convoRef) {
      this.convoRef.scrollIntoView();
      this.convoRef = null;
    }
  }

  componentWillUnmount() {
    if(this.notifTimer) {
      clearTimeout(this.notifTimer)
      this.notifTimer = null
    }
  }

  getMessagesAndNotifications = sessionId => {
    axios({
      url: this.backUrl + "getAllMessages",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: sessionId
      },
      data: {}
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          var unreadCount = filter(res.data.data, function(o) {
            return !o.msg_read;
          });
          var sortedMsmg = sortBy(res.data.data, function(o) {
            return o.created;
          });
          this.setState({
            messageList: reverse(sortedMsmg),
            unreadMessageCount: unreadCount ? unreadCount.length : 0
          });
        } else {
          if (res && res.data && res.data.msg) {
            if (res.data.msg === "You need to login to continue.") {
              let cookiesObj = this.props.cookies.getAll();
              for (var key in cookiesObj) {
                this.props.cookies.remove(key, { path: "/" });
              }
              window.location.reload();
            }
            this.setState({
              messageError: res.data.msg
            });
          } else {
            this.setState({
              messageError:
                "Something went wrong while fetching messages. Please try again later."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          ...this.state,
          messageError:
            "Something went wrong while fetching messages. Please try again later."
        });
      });

    axios({
      url: this.backUrl + "getNotifications",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: sessionId
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          let unread = filter(res.data.data, function(o) {
            return !o.notif_read;
          });
          let unreadCount = unread.length;
          var sortedNotifs = sortBy(res.data.data, function(o) {
            return o.created;
          });
          this.setState({
            userNotifications: reverse(sortedNotifs),
            notificationUnreadCount: unreadCount
          });
        } else {
          if (res && res.data && res.data.msg) {
            if (res.data.msg === "You need to login to continue.") {
              let cookiesObj = this.props.cookies.getAll();
              for (var key in cookiesObj) {
                this.props.cookies.remove(key, { path: "/" });
              }
              window.location.reload();
            }
            this.setState({
              notificationsError: res.data.msg
            });
          } else {
            this.setState({
              notificationsError:
                "Something went wrong while fetching notifications."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          notificationsError:
            "Something went wrong while fetching notifications."
        });
      });

    this.notifTimer = setTimeout(() => {
      this.getMessagesAndNotifications(sessionId);
    }, 25000);
  };

  logout = () => {
    this.setState({
      logoutButtonDisabled: true
    });
    var sessionId = this.props.cookies.get("session_id");
    if (sessionId && sessionId.length > 0) {
      axios({
        method: "POST",
        url: this.backUrl + "logout",
        headers: {
          "Content-type": "text/plain",
          session_id: sessionId
        },
        body: {}
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            let cookiesObj = this.props.cookies.getAll();
            for (var key in cookiesObj) {
              this.props.cookies.remove(key, { path: "/" });
            }
            // this.props.history.push("/");
            setTimeout(() => {
              window.location.reload();
            }, 500);
          } else if (res && res.data && res.data.msg) {
            message.error(res.data.msg);
          } else {
            message.error("Something went wrong, please try again later.");
          }
        })
        .catch(e => {
          message.error("Something went wrong, please try again later.");
        });
    } else {
      let cookiesObj = this.props.cookies.getAll();
      for (var key in cookiesObj) {
        this.props.cookies.remove(key, { path: "/" });
      }
      // setTimeout(() => {
      //   window.location.reload();
      // }, 100);
    }
    this.setState({
      logoutButtonDisabled: false
    });
  };

  openConvo = (from_user_id, name) => {
    var messageListCopy = [];
    map(this.state.messageList, function(item, i) {
      if (item.from_user_id === from_user_id) {
        var temp = cloneDeep(item);
        temp.msg_read = true;
        messageListCopy.push(temp);
      } else {
        messageListCopy.push(item);
      }
    });
    var unreadCount = filter(messageListCopy, function(o) {
      return !o.msg_read;
    });
    this.setState({
      convoVisible: true,
      convoName: name,
      convoFromId: from_user_id,
      messageList: messageListCopy,
      unreadMessageCount: unreadCount ? unreadCount.length : 0
    });
    axios({
      url: this.backUrl + "getConversation",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id")
      },
      data: {
        from_user_id: from_user_id
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          var sortedConvo = sortBy(res.data.data, function(item) {
            return item.created;
          });
          this.setState({
            convo: sortedConvo
          });
        } else {
          if (res && res.data && res.data.msg) {
            if (res.data.msg === "You need to login to continue.") {
              let cookiesObj = this.props.cookies.getAll();
              for (var key in cookiesObj) {
                this.props.cookies.remove(key, { path: "/" });
              }
            }
            this.setState({
              convoError: res.data.msg
            });
          } else {
            this.setState({
              convoError:
                "Something went wrong while fetching the conversation. Please try again later."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          convoError:
            "Something went wrong while fetching the conversation. Please try again later."
        });
      });
  };

  sendMessage = e => {
    var msg = this.state.sendAMessageValue;
    if (msg && msg.length > 0 && (!msg.match(/^\s*$/))) {
      this.setState({
        sendAMessageDisabled: true
      });
      axios({
        url: this.backUrl + "addMessage",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          session_id: this.props.cookies.get("session_id")
        },
        data: {
          to_user_id: this.state.convoFromId,
          message: msg
        }
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            var tempConvo = cloneDeep(this.state.convo);
            var localTime = moment();
            tempConvo.push({
              to_user_id: this.state.convoFromId,
              message: msg,
              created: localTime
            });
            this.setState({
              sendAMessageDisabled: false,
              sendAMessageValue: null,
              convo: tempConvo
            });
          } else {
            if (res && res.data && res.msg) {
              if (res.data.msg === "You need to login to continue.") {
                message.warn(res.data.msg);
                let cookiesObj = this.props.cookies.getAll();
                for (var key in cookiesObj) {
                  this.props.cookies.remove(key, { path: "/" });
                }
              } else {
                message.error(res.data.msg);
              }
              this.setState({
                sendAMessageDisabled: false
              });
            } else {
              message.error(
                "Something went wrong while sending the message. Please try again later."
              );
              this.setState({
                sendAMessageDisabled: false
              });
            }
          }
        })
        .catch(e => {
          message.error(
            "Something went wrong while sending the message. Please try again later."
          );
          this.setState({
            ...this.state,
            sendAMessageDisabled: false
          });
        });
    }
  };

  handleSendAMessageValue(e) {
    this.setState({
      sendAMessageValue: e.target.value
    });
  }

  handleNotificationClick(n_id, notification_type, qac_id, a_id) {
    if (this.state.sessionId) {
      axios({
        url: this.backUrl + "setNotificationAsRead",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          session_id: this.state.sessionId
        },
        data: {
          n_id: n_id
        }
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            var newNotifications = cloneDeep(this.state.userNotifications);
            map(newNotifications, function(item) {
              if (item.n_id === n_id) {
                item.notif_read = true;
              }
            });
            let unread = filter(newNotifications, function(o) {
              return !o.notif_read;
            });
            let unreadCount = unread.length;
            this.setState({
              userNotifications: newNotifications,
              notificationUnreadCount: unreadCount
            });
          }
          switch (notification_type) {
            case 2: {
              var cookieKey = "post:"+qac_id;
              this.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(610, 'seconds').toDate()})
              this.props.history.push("/post/" + qac_id + "/null/" + a_id);
              break;
            }
            case 3: {
              var cookieKey = "post:"+qac_id;
              this.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(610, 'seconds').toDate()})
              this.props.history.push("/post/" + qac_id + "/null/" + a_id);
              break;
            }
            case 6: {
              var cookieKey = "post:"+qac_id;
              this.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(610, 'seconds').toDate()})
              this.props.history.push("/post/" + qac_id + "/null/" + a_id);
              break;
            }
            case 7: {
              var cookieKey = "post:"+qac_id;
              this.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(610, 'seconds').toDate()})
              this.props.history.push("/post/" + qac_id + "/null/" + a_id);
              break;
            }
            case 12: {
              var cookieKey = "article:"+qac_id;
              this.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(610, 'seconds').toDate()})
              this.props.history.push("/article/" + qac_id + "/null/");
              break;
            }
            case 13: {
              var cookieKey = "article:"+qac_id;
              this.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(610, 'seconds').toDate()})
              this.props.history.push("/article/" + qac_id + "/null/");
              break;
            }
            case 14: {
              this.props.history.push("/user/" + qac_id);
              break;
            }
            default: {
              break;
            }
          }
        })
        .catch(e => {});
    }
  }

  markAllMessagesAsRead = () => {
    if (this.state.sessionId) {
      let tempMsgList = cloneDeep(this.state.messageList);
      let newMsgList = map(tempMsgList, item => {
        item.msg_read = true;
        return item;
      });
      this.setState({
        messageList: newMsgList,
        unreadMessageCount: 0
      });
      axios({
        url: this.backUrl + "markAllMessagesAsRead",
        method: "POST",
        headers: {
          session_id: this.state.sessionId
        }
      });
    } else {
      message.error("You need to login to continue.");
    }
  };

  markAllNotificationsAsRead = () => {
    if (this.state.sessionId) {
      let tempNotifList = cloneDeep(this.state.userNotifications);
      let newNotifList = map(tempNotifList, item => {
        item.notif_read = true;
        return item;
      });
      this.setState({
        userNotifications: newNotifList,
        notificationUnreadCount: 0
      });
      axios({
        url: this.backUrl + "markAllNotificationsAsRead",
        method: "POST",
        headers: {
          session_id: this.state.sessionId
        }
      });
    } else {
      message.error("You need to login to continue.");
    }
  };

  handleDarkChange = v => {
    if (v) {
      this.props.cookies.set("dark", true, { path: "/" });
      this.setState({
        darkChecked: true
      });
    } else {
      this.props.cookies.set("dark", false, { path: "/" });
      this.setState({
        darkChecked: false
      });
    }
    window.location.reload();
  };

  getNotifIcon = i => {
    switch (i) {
      case 2: {
        return answerIcon;
      }

      case 3:
      case 13: {
        return commentIcon;
      }
      case 6: {
        return upvoteIcon;
      }
      case 7: {
        return downvoteIcon;
      }
      case 8:
      case 9:
      case 10:
      case 11: {
        return deleteIcon;
      }
      case 12: {
        return likeIcon;
      }
      case 14: {
        return followIcon;
      }
      default: {
        return;
      }
    }
  };

  render() {
    const notLoggedIn = (
      <ButtonGroup className="first-row-button-group">
        <Button
          ghost
          type="primary"
          className="first-row-button"
          onClick={() => {
            this.setState({
              loginModalVisible: true
            });
            this.props.blur && this.props.blur(true);
          }}
        >
          Login
        </Button>
        <Button
          ghost
          type="primary"
          className="first-row-button"
          onClick={() => {
            this.setState({
              signupModalVisible: true
            });
            this.props.blur && this.props.blur(true);
          }}
        >
          Sign Up
        </Button>
      </ButtonGroup>
    );
    var messageContent, notifContent;
    if (this.state.sessionId && this.state.messageList) {
      if (this.state.messageList.length === 0) {
        messageContent = (
          <div style={{ padding: "0.5em" }}>
            You do not have any new messages.
          </div>
        );
      } else {
        var that = this;
        messageContent = map(this.state.messageList, function(item, i) {
          var createdUTC = moment(item.created).utc();
          var nowUtc = moment(moment()).utc();
          var duration = moment.duration(-nowUtc.diff(createdUTC));
          var readClassName = item.msg_read
            ? "individual-message"
            : "individual-message-unread";
          return (
            <div
              key={i}
              className={readClassName}
              onClick={() => {
                that.openConvo(item.from_user_id, item.name);
              }}
            >
              <div className="message-list-avatar-div">
                {item.dp_url ? (
                  <Avatar src={item.dp_url} />
                ) : (
                  <Avatar icon="user" />
                )}
              </div>
              <div className="message-list-details-div">
                <span className="message-name">{item.name}</span>
                <span className="message-created-date">
                  {duration.humanize(true)}
                </span>
                <div style={{ fontWeight: "400" }}>{item.message}</div>
              </div>
            </div>
          );
        });
      }
    }
    if (this.state.sessionId && this.state.userNotifications) {
      if (this.state.userNotifications.length === 0) {
        notifContent = <div style={{padding:"0.5em"}}>You do not have any new notifications.</div>;
      } else {
        var that = this;
        notifContent = map(this.state.userNotifications, function(item, i) {
          var createdUTC = moment(item.created).utc();
          var nowUtc = moment(moment()).utc();
          var duration = moment.duration(-nowUtc.diff(createdUTC));
          var notifClass = item.notif_read
            ? "notif-content-inner-div"
            : "notif-unread-content-inner-div";
          return (
            <div
              className={notifClass}
              onClick={() => {
                that.handleNotificationClick(
                  item.n_id,
                  item.notification_type,
                  item.qac_id,
                  item.a_id
                );
              }}
              key={i}
            >
              <Row type="flex">
                <Col xs={2} sm={2}>
                  <div>
                    <img
                      className="notif-icon-img"
                      src={that.getNotifIcon(item.notification_type)}
                    />
                  </div>
                </Col>
                <Col xs={22} sm={22}>
                  <div>
                    <div className="notif-created-date">
                      {duration.humanize(true)}
                    </div>
                    <div>{item.message}</div>
                    {item.preview ? (
                      <div className={"notif-preview-div"}>{item.preview}</div>
                    ) : null}
                  </div>
                </Col>
              </Row>
            </div>
          );
        });
      }
    }
    const loggedIn = (
      <div className="profile-main-div">
        <div className={"first-row-name-group" + this.state.dark}>
          Hi, {this.state.name}
          <span>
            <Popover
              overlayStyle={{ position: "fixed" }}
              trigger="click"
              placement="bottom"
              content={
                <React.Fragment>
                  <div>
                    <Button
                      disabled={this.props.cookies.get("u_id") ? false : true}
                      className="user-action-button"
                      onClick={() => {
                        this.props.history.push(
                          "/user/" + this.props.cookies.get("u_id")
                        );
                      }}
                    >
                      Profile
                    </Button>
                  </div>
                  <div>
                    <Button
                      className="user-action-button"
                      onClick={() => {
                        this.props.history.push("/profile");
                      }}
                    >
                      Account
                    </Button>
                  </div>
                  <div>
                    <Button
                      className="user-action-button"
                      onClick={this.logout}
                      disabled={this.state.logoutButtonDisabled}
                    >
                      Logout
                    </Button>
                  </div>
                  <hr
                    style={{
                      borderTop: "none",
                      borderBottom: "1px solid #aaaaaa"
                    }}
                  />
                  <div
                    className="dark-mode-switch-div"
                    style={{ textAlign: "center  " }}
                  >
                    <Switch
                      checked={this.state.darkChecked}
                      onChange={this.handleDarkChange}
                      checkedChildren="Dark"
                      unCheckedChildren="Light"
                    />
                  </div>
                </React.Fragment>
              }
            >
              <span className={"arrow-span" + this.state.dark}>
                <img className="arrow" src={arrow} alt="settings" />
              </span>
            </Popover>
          </span>
        </div>
        <div className="notif-icon-div">
          <Badge
            count={this.state.messageList ? this.state.unreadMessageCount : 0}
            className="notif-icon"
          >
            <Popover
              overlayStyle={{ position: "fixed" }}
              trigger="click"
              placement="bottom"
              mouseLeaveDelay={1}
              overlayClassName="message-popover"
              content={
                <div className="message-outer-div">
                  {this.state.unreadMessageCount ? (
                    <div
                      onClick={this.markAllMessagesAsRead}
                      className="mark-all-as-read"
                    >
                      Mark all as read
                    </div>
                  ) : null}
                  {messageContent}
                </div>
              }
              zIndex={10}
            >
              <span>
                <span className={"message-span" + this.state.dark}>
                  <img className="message" src={messageIcon} alt="message" />
                </span>
              </span>
            </Popover>
          </Badge>
          <Badge
            count={
              this.state.notificationUnreadCount
                ? this.state.notificationUnreadCount
                : 0
            }
            className="notif-icon"
          >
            <Popover
            autoAdjustOverflow={true}
              overlayStyle={{ position: "fixed" }}
              trigger="click"
              placement="bottom"
              overlayClassName="notif-popover"
              mouseLeaveDelay={1}
              content={
                <div>
                  {this.state.notificationUnreadCount ? (
                    <div
                      onClick={this.markAllNotificationsAsRead}
                      className="mark-all-as-read"
                    >
                      Mark all as read
                    </div>
                  ) : null}
                  <div className="notif-outer-div"> {notifContent}</div>
                </div>
              }
            >
              <span>
                <span className={"notif-span" + this.state.dark}>
                  <img className="notif" src={notif} alt="settings" />
                </span>
              </span>
            </Popover>
          </Badge>
        </div>
      </div>
    );
    const content =
      this.state.name && this.state.sessionId ? loggedIn : notLoggedIn;
    var convo;
    if (!this.state.convo && !this.state.convoError) {
      convo = [];
      convo.push(<div key={1} className="last-empty-element" />);
      convo.push(
        <Spin
          key={2}
          indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
        />
      );
    } else if (this.state.convoError) {
      convo = <Alert type="error" message={this.state.convoError} />;
    } else {
      convo = [];
      convo.push(<div key={-1} className="last-empty-element" />);
      var tempConvo = map(this.state.convo, function(item, i) {
        var createdUTC = moment(item.created).utc();
        var localTime = moment(createdUTC)
          .local()
          .format("MMMM Do YYYY, h:mm:ss a");
        return (
          <div
            key={i}
            className={
              that.state.convoFromId === item.from_user_id
                ? "convo-message-from-div" + that.state.dark
                : "convo-message-to-div" + that.state.dark
            }
          >
            <div className="convo-message-name">
              {that.state.convoFromId === item.from_user_id ? (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    that.props.history.push("/user/" + that.state.convoFromId);
                  }}
                >
                  {item.name}
                </div>
              ) : (
                "You"
              )}
            </div>
            <div>{item.message}</div>
            <div className="convo-message-time">{localTime}</div>
          </div>
        );
      });
      convo.push(tempConvo);
      convo.push(
        <div
          key={9999999}
          ref={ref => {
            this.convoRef = ref;
          }}
          className="last-empty-element"
        />
      );
    }
    return (
      <div>
        <div>
          <Drawer
            visible={this.state.convoVisible}
            title={
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.state.convoFromId &&
                    this.props.history.push("/user/" + this.state.convoFromId);
                }}
              >
                {this.state.convoName}
              </div>
            }
            placement="right"
            closable={true}
            zIndex={999}
            maskClosable={true}
            className={"convo-drawer" + this.state.dark}
            onClose={() => {
              this.setState({
                convoVisible: false,
                convo: null,
                convoFromId: null,
                convoName: null,
                sendAMessageValue: null
              });
            }}
          >
            <div className="convo-drawer">
              <div className="messages-convo">{convo}</div>
              <div className="type-a-message-div">
                <span>
                  <Input.TextArea
                    autosize={{ minRows: 3, maxRows: 3 }}
                    className="type-a-message"
                    placeholder="Type a message (enter to send)"
                    onPressEnter={this.sendMessage}
                    value={this.state.sendAMessageValue}
                    onChange={this.handleSendAMessageValue.bind(this)}
                    disabled={this.state.sendAMessageDisabled}
                  />
                </span>
              </div>
              <div>
                  <img className="message-send-button" src={sendButton} onClick={this.sendMessage}></img>
                </div>
            </div>
          </Drawer>
        </div>
        {content}
        <Modal
          visible={this.state.loginModalVisible}
          footer={null}
          onCancel={() => {
            this.setState({ loginModalVisible: false });
            this.props.blur && this.props.blur(false);
          }}
        >
          <PopupLogin login />
        </Modal>
        <Modal
          visible={this.state.signupModalVisible}
          footer={null}
          onCancel={() => {
            this.setState({ signupModalVisible: false });
            this.props.blur && this.props.blur(false);
          }}
        >
          <PopupLogin signup />
        </Modal>
        {this.state.showConsent?<div className="compliance-consent">
          This website uses cookies. By using this website, you agree to the 
          <a href="/cookie-policy"> Cookie Policy</a>{", "} 
          <a href="/terms">Terms and Conditions</a>{" and "} 
            <a href="/privacy">Privacy Policy</a> 
            <span className="consent-accept-span" style={{display:"inline-block", paddingLeft:"1em"}}>
            <Button type="primary" onClick={()=>{
              this.props.cookies.set("consented","true", { path: "/", expires: moment().add(2, 'days').toDate(), secure: false })
              this.setState({showConsent: false})
              }}>Accept</Button></span>
        </div>:null}
      </div>
    );
  }
}

export default withRouter(withCookies(ProfileInfo));
