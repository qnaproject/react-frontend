import React, { Component } from "react";
import axios from "axios";
import _ from "lodash";
import "./Feed.css";
import { Spin, Icon, Alert, Modal, Avatar, Button, message } from "antd";
import moment from "moment";
import { withRouter } from "react-router-dom";
import tinycolor from "tinycolor2";
import fire from "./res/fire.svg";
import { map, uniqBy, sortBy, indexOf, pull } from "lodash";
import Masonry from "react-masonry-css";
import { withCookies } from "react-cookie";
import sanitizeHtml from "sanitize-html";
import Vote from "./Vote";
import * as utils from "./utils/utils.js"

class Feed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: true,
      data: []
    };
    this.backUrl = "";
  }

  categoryColorMap = utils.categoryColorMap

  getFeed = () => {
    if (!this.props.contentType) {
      this.setState({
        error: "Invalid content selection."
      });
      return;
    } else if (this.props.contentType === 3) {
      if (this.state.valid) {
        /*axios call here*/
        axios({
          url: this.backUrl + "getUserFeed",
          method: "POST",
          headers: {
            "Content-type": "text/plain",
            session_id: this.props.cookies.get("session_id")
          },
          data: null
        })
          .then(res => {
            if (res && res.data) {
              if (!res.data.success) {
                if (res.data.msg === "You need to login to continue.") {
                  let cookiesObj = this.props.cookies.getAll();
                  for (var key in cookiesObj) {
                    this.props.cookies.remove(key, { path: "/" });
                  }
                }
                if (
                  res.data.msg ===
                  "You haven't followed any users or categories"
                ) {
                  this.setState({
                    valid: true,
                    warn: res.data.msg,
                    data: []
                  });
                } else {
                  this.setState({
                    valid: false,
                    error:
                      res.data.msg ||
                      "Something went wrong while fetching the feed.",
                    data: []
                  });
                }
              } else {
                this.setState({
                  gotFeed: true,
                  valid: true,
                  data: sortBy(uniqBy(res.data.data, "answer"), "created")
                });
              }
            } else {
              this.setState({
                valid: false,
                data: [],
                error: "Something went wrong while fetching the feed."
              });
            }
          })
          .catch(err => {
            this.setState({
              valid: false,
              data: [],
              error: "Something went wrong while fetching the feed."
            });
          });
      }
    } else if (this.props.contentType === 2) {
      if (this.state.valid) {
        const postData = JSON.stringify({
          category:
            this.props.category[0].toUpperCase() + this.props.category.slice(1)
        });
        /*axios call here*/
        axios({
          url: this.backUrl + "getLatestArticles",
          method: "POST",
          headers: {
            "Content-type": "text/plain"
          },
          data: postData
        })
          .then(res => {
            if (res && res.data) {
              if (!res.data.success) {
                if (res.data.msg === "You need to login to continue.") {
                  let cookiesObj = this.props.cookies.getAll();
                  for (var key in cookiesObj) {
                    this.props.cookies.remove(key, { path: "/" });
                  }
                }
                this.setState({
                  valid: false,
                  error:
                    res.data.msg ||
                    "Something went wrong while fetching the feed.",
                  data: []
                });
              } else {
                this.setState({
                  gotFeed: true,
                  valid: true,
                  data: res.data.data
                });
              }
            } else {
              this.setState({
                valid: false,
                data: [],
                error: "Something went wrong while fetching the feed."
              });
            }
          })
          .catch(err => {
            this.setState({
              valid: false,
              data: [],
              error: "Something went wrong while fetching the feed."
            });
          });
      }
    } else {
      if (this.props.feedType && this.props.feedType > 0) {
        var feedTypeString = "";
        switch (this.props.feedType) {
          case 1: {
            feedTypeString = "getTrendingPosts";
            break;
          }
          case 2: {
            feedTypeString = "getLatestPosts";
            break;
          }
          default: {
            this.setState({
              valid: false,
              data: []
            });
          }
        }
        if (this.state.valid) {
          const postData = JSON.stringify({
            category:
              this.props.category[0].toUpperCase() +
              this.props.category.slice(1)
          });
          /*axios call here*/
          axios({
            url: this.backUrl + feedTypeString,
            method: "POST",
            headers: {
              "Content-type": "text/plain"
            },
            data: postData
          })
            .then(res => {
              if (res && res.data) {
                if (!res.data.success) {
                  if (res.data.msg === "You need to login to continue.") {
                    let cookiesObj = this.props.cookies.getAll();
                    for (var key in cookiesObj) {
                      this.props.cookies.remove(key, { path: "/" });
                    }
                  }
                  this.setState({
                    valid: false,
                    error:
                      res.data.msg ||
                      "Something went wrong while fetching the feed.",
                    data: []
                  });
                } else {
                  this.setState({
                    gotFeed: true,
                    valid: true,
                    data: res.data.data
                  });
                }
              } else {
                this.setState({
                  valid: false,
                  data: [],
                  error: "Something went wrong while fetching the feed."
                });
              }
            })
            .catch(err => {
              this.setState({
                valid: false,
                data: [],
                error: "Something went wrong while fetching the feed."
              });
            });
        }
      } else {
        this.setState({
          error: "Invalid feed type."
        });
      }
    }
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.contentType !== this.props.contentType) {
      this.setState({
        data: [],
        gotFeed: false,
        warn: null,
        error: null
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.contentType !== this.props.contentType) {
      this.getFeed();
      return;
    }
    if (prevProps.feedType !== this.props.feedType) {
      this.setState({
        data: [],
        gotFeed: false,
        error: null,
        warn: null
      });
      this.getFeed();
    } else if (prevProps.category !== this.props.category) {
      this.setState({
        data: [],
        gotFeed: false,
      });
      this.getFeed();
    }
    if (this.feedIndividualRefs && this.feedIndividualRefs.length > 0) {
      map(this.feedIndividualRefs, (o, i) => {
        if (o && o.clientHeight > 250) {
          o.style.cursor = "pointer";
          this.feedIndividualMaskRefs[i].style.display = "block";
          this.feedIndividualMaskRefs[i].style.visibility = "visible";
          if (this.voteRefs && this.voteRefs[i]) {
            this.voteRefs[i].style.display = "none";
            this.voteRefs[i].style.visibility = "hidden";
          }
        }
      });
    }
  }

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    this.getFeed();
  }
  getPostLink = (id, title) => {
    if (!id || !title) {
      return;
    }
    let safeTitle = title
      .split(" ")
      .join("-")
      .toLowerCase()
      .split("?")
      .join("-")
      .split("'")
      .join("");
    if (safeTitle[safeTitle.length - 1] === "-") {
      safeTitle = safeTitle.slice(0, safeTitle.length - 1);
    }
    return encodeURI("/post/" + id + "/" + safeTitle);
  };
  goToPost = (id, title) => {
    let safeTitle = title
      .split(" ")
      .join("-")
      .toLowerCase()
      .split("?")
      .join("-")
      .split("'")
      .join("");
    if (safeTitle[safeTitle.length - 1] === "-") {
      safeTitle = safeTitle.slice(0, safeTitle.length - 1);
    }
    this.props.history.push(encodeURI("/post/" + id + "/" + safeTitle));
    return false;
  };

  getArticleLink = (id, title) => {
    if (!id || !title) {
      return;
    }
    let safeTitle = title
      .split(" ")
      .join("-")
      .toLowerCase()
      .split("?")
      .join("-")
      .split("'")
      .join("");
    if (safeTitle[safeTitle.length - 1] === "-") {
      safeTitle = safeTitle.slice(0, safeTitle.length - 1);
    }
    return encodeURI("/article/" + id + "/" + safeTitle);
  };

  goToArticle = (id, title) => {
    if (!id || !title) {
      return;
    }
    let safeTitle = title
      .split(" ")
      .join("-")
      .toLowerCase()
      .split("?")
      .join("-")
      .split("'")
      .join("");
    if (safeTitle[safeTitle.length - 1] === "-") {
      safeTitle = safeTitle.slice(0, safeTitle.length - 1);
    }
    this.props.history.push("/article/" + id + "/" + safeTitle);
  };

  followCategory = cat => {
    if (cat && cat.length > 0) {
      axios({
        url: this.backUrl + "follow",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          session_id: this.props.cookies.get("session_id")
        },
        data: {
          type: 0,
          f_cat: cat
        }
      }).then(res => {
        if (res && res.data && res.data.success) {
          if (this.state.followingCats) {
            let temp = this.state.followingCats;
            temp.push(cat);
            this.setState({
              followingCats: temp
            });
          } else {
            this.setState({
              followingCats: [cat]
            });
          }
        } else if (res && res.data && res.data.msg) {
          message.error(res.data.msg);
        } else {
          message.error("Something went wrong. Please try again later.");
        }
      });
    }
  };

  unfollowCategory = cat => {
    if (cat && cat.length > 0) {
      axios({
        url: this.backUrl + "unFollow",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          session_id: this.props.cookies.get("session_id")
        },
        data: {
          type: 0,
          f_cat: cat
        }
      }).then(res => {
        if (res && res.data && res.data.success) {
          if (this.state.followingCats) {
            let temp = this.state.followingCats;
            pull(temp, cat);
            this.setState({
              followingCats: temp
            });
          }
        } else if (res && res.data && res.data.msg) {
          message.error(res.data.msg);
        } else {
          message.error("Something went wrong. Please try again later.");
        }
      });
    }
  };

  handleFollowCatVisible = () => {
    this.setState({
      followCatVisible: true,
      gotFollowCats: false
    });
    axios({
      url: this.backUrl + "getFollowingCategories",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id")
      }
    }).then(res => {
      if (res && res.data && res.data.success) {
        this.setState({
          followingCats: res.data.data,
          gotFollowCats: true
        });
      }
    });
  };

  render() {
    var post = this.goToPost;
    var vals;
    if (this.state.valid && this.state.data && this.state.gotFeed) {
      var that = this;
      if (this.props.contentType === 3) {
        this.feedIndividualRefs = [];
        this.feedIndividualMaskRefs = [];
        that.voteRefs = [];
        vals = _.map(this.state.data, function(item, i) {
        //   var createdUTC = moment(item.created).utc();
        // var nowUtc = moment(moment()).utc();
        // var duration = moment.duration(-nowUtc.diff(createdUTC));
          return (
            <div
              key={i}
              ref={ref => {
                that.feedIndividualRefs[i] = ref;
              }}
              className={"feed-individual-answer" + that.state.dark}
              onClick={e => {
                e.target.style.maxHeight = "10000em";
                e.currentTarget.style.maxHeight = "10000em";
                if (
                  that.feedIndividualMaskRefs &&
                  that.feedIndividualMaskRefs[i]
                ) {
                  that.feedIndividualMaskRefs[i].style.visibility = "hidden";
                  that.feedIndividualRefs[i].style.cursor = "auto";
                  if (that.voteRefs && that.voteRefs[i]) {
                    that.voteRefs[i].style.display = "block";
                    that.voteRefs[i].style.visibility = "visible";
                  }
                }
              }}
            >
              <div
                className={"q-title feed" + that.state.dark}
                onClick={() => {
                  let safeTitle = item.title
                    .split(" ")
                    .join("-")
                    .toLowerCase()
                    .split("?")
                    .join("-")
                    .split("'")
                    .join("");
                  if (safeTitle[safeTitle.length - 1] === "-") {
                    safeTitle = safeTitle.slice(0, safeTitle.length - 1);
                  }
                  that.props.history.push(
                    encodeURI("/post/" + item.q_id + "/" + safeTitle)
                  );
                }}
              >
                {item.title}
              </div>
              <div
                style={{
                  marginTop: "1em",
                  marginRight: "0.5em",
                  marginBottom: "0.2em",
                  display: "inline-block"
                }}
              >
                {item.dp_url ? (
                  <Avatar src={item.dp_url} />
                ) : (
                  <Avatar icon="user" />
                )}
              </div>
              <div onClick={()=>{item.u_id && that.props.history.push("/user/"+item.u_id)}} 
              className={"feed-individual-answer-name" + that.state.dark}>
                {item.fname + " " + (item.lname?item.lname:"")}{" "}
              </div>
              {/* <div className={"feed-created" + that.state.dark}>{duration.humanize(true)}</div> */}
              <div
                className={"feed-individual-answer-answer" + that.state.dark}
                dangerouslySetInnerHTML={{
                  __html: sanitizeHtml(item.answer, {
                    allowedTags: [
                      "p",
                      "strong",
                      "em",
                      "s",
                      "blockquote",
                      "ol",
                      "ul",
                      "li",
                      "a",
                      "img",
                      "pre",
                      "span"
                    ],
                    allowedAttributes: {
                      a: ["href", "rel", "target"],
                      img: ["src", "rel"],
                      pre: ["class", "spellcheck"],
                      span: ["class"]
                    }
                  })
                }}
              />
              <div
                className={"feed-individual-answer-mask" + that.state.dark}
                ref={ref => {
                  that.feedIndividualMaskRefs[i] = ref;
                }}
              ><div className={"feed-individual-answer-mask-dots"+that.state.dark}>...</div>
              </div>
              <div
                ref={ref => {
                  that.voteRefs[i] = ref;
                }}
              >
                <Vote feed={true} aId={item.a_id} />
              </div>
            </div>
          );
        });
        if (vals.length > 0) {
          vals = (
            <div>
              <Button
                ghost
                className="on-feed-edit-feed"
                type="primary"
                onClick={this.handleFollowCatVisible}
              >
                <Icon type="setting" />
                Edit Feed
              </Button>
              <div
                style={{ verticalAlign: "middle", marginTop: "-0.2em" }}
                className="vl"
              />
              <Button
                ghost
                style={{ marginLeft: "-0.6em" }}
                className="on-feed-edit-feed"
                type="primary"
                onClick={() => {
                  this.setState({ gotFeed: false });
                  this.getFeed();
                }}
              >
                Refresh
              </Button>
              {vals}
            </div>
          );
        }

        // vals = vals.sort(function (a, b) {
        //   return b.created - a.created;
        // });
      } else if (this.props.contentType === 2) {
        vals = _.map(this.state.data[0], function(item, i) {
          // var createdUTC = moment(item.created).utc();
          // var localTime = moment(createdUTC)
          // .local()
          // .format("MMMM Do YYYY, h:mm:ss a");
          return (
            <div
              style={{
                color: "black",
                // backgroundImage:'url("' + item.preview_url + '")'
                // borderWidth: "1px",
                // borderStyle:"solid",
                // borderColor: tinycolor(
                //   that.categoryColorMap.get(item.category)
                // )
                //   .darken(20)
                //   .toHexString()
              }}
              className={"feed-article-div" + that.state.dark}
              key={i}
              onClick={e => {
                that.goToArticle(item.a_id, item.title);
                e.preventDefault();
              }}
            >
            {item.preview_url?<div>
              <img className="article-feed-preview-img" src={item.preview_url}/>
            </div>:null}
              {/* <a href={that.getPostLink(item.a_id, item.title)}> */}
              <a href={that.getArticleLink(item.a_id, item.title)}>
                <h2 className={"q-title article" + that.state.dark}>
                  {item.title}
                </h2>
              </a>
              <hr />
              {/* </a> */}
              <div
                className={"q-info article"+that.state.dark}
                style={{
                  borderColor: tinycolor(
                    that.categoryColorMap.get(item.category)
                  )
                    .darken(0)
                    .toHexString(),
                  borderWidth: "1px",
                  borderStyle:"solid",
                  color:"rgba(0, 0, 0, 0.65)"
                }}
              >
                {item.category}
              </div>
              <div className="vl" />
              <div className={"q-info-created"+that.state.dark} style={{textTransform:"capitalize"}}>{item.fname?(item.fname+" "+(item.lname?item.lname:"")):"Deleted User"}</div>
            </div>
          );
        });
        if (vals.length > 0) {
          vals = (
            <Masonry
              breakpointCols={{
                default: 3,
                1100: 3,
                700: 2,
                500: 1
              }}
              className="my-masonry-grid"
              columnClassName="my-masonry-grid_column"
            >
              {vals}
            </Masonry>
          );
        }

        // vals = vals.sort(function (a, b) {
        //   return b.created - a.created;
        // });
      } else {
        switch (this.props.feedType) {
          case 1: {
            vals = _.map(this.state.data, function(item, i) {
              var createdUTC = moment(item.created).utc();
              var localTime = moment(createdUTC)
                .local()
                .format("MMMM Do YYYY, h:mm:ss a");
                let t_points = item.activity
                let points
                if (t_points > 999999) {
                  let point_string = ((1.0 * t_points) / 1000000.0).toFixed(3);
                  let seperateVals = point_string.split(".");
                  if (seperateVals[1]) {
                    points =
                      seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "M";
                  } else {
                    points = seperateVals[0] + "M";
                  }
                } else if (t_points > 999) {
                  let point_string = ((1.0 * t_points) / 1000.0).toFixed(3);
                  let seperateVals = point_string.split(".");
                  if (seperateVals[1]) {
                    points =
                      seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "K";
                  } else {
                    points = seperateVals[0] + "K";
                  }
                } else {
                  points = t_points;
                }
              return (
                <div
                  className={"feed-question-div" + that.state.dark}
                  key={i}
                  onClick={e => {
                    post(item.q_id, item.title);
                    e.preventDefault();
                  }}
                >
                  <a href={that.getPostLink(item.q_id, item.title)}>
                    <h2 className={"q-title" + that.state.dark}>
                      {item.title}
                    </h2>
                  </a>
                  <div
                    className={"q-info"+that.state.dark}
                    style={{
                      borderColor: tinycolor(
                        that.categoryColorMap.get(item.category)
                      )
                        .darken(0)
                        .toHexString(),
                      borderWidth: "1px",
                      borderStyle:"solid",
                      color:"rgba(0, 0, 0, 0.65)"
                    }}
                  >
                    {item.category}
                  </div>
                  <div className="vl" />
                  <div className="fire-icon-div">
                    <img
                      className={"fire-icon" + that.state.dark}
                      src={fire}
                      alt="trending"
                    />
                  </div>
                  <div
                    className={
                      "question-trending-activity-span" + that.state.dark
                    }
                  >
                    {points}
                  </div>
                  <div className="vl" />
                  <div className={"q-info-created" + that.state.dark}>
                    {localTime.toString()}
                  </div>
                </div>
              );
            });
            vals = vals.sort(function(a, b) {
              return a.activity - b.activity;
            });
            if (vals.length > 0) {
              vals.push(
                <div
                  key={999}
                  onClick={() => {
                    this.props.goToLatest();
                  }}
                  className={"view-latest-posts-button" + that.state.dark}
                >
                  View latest posts
                </div>
              );
            }
            break;
          }
          case 2: {
            vals = _.map(this.state.data[0], function(item, i) {
              var createdUTC = moment(item.created).utc();
              var localTime = moment(createdUTC)
                .local()
                .format("MMMM Do YYYY, h:mm:ss a");
              return (
                <div
                  className={"feed-question-div" + that.state.dark}
                  key={i}
                  onClick={
                    (e) => {
                        post(item.q_id, item.title);
                        e.preventDefault();
                      }
                  }
                >
                <a href={that.getPostLink(item.q_id, item.title)}>
                  <h2 className={"q-title" + that.state.dark}>{item.title}</h2>
                </a>
                  <div
                    className={"q-info"+that.state.dark}
                    style={{
                      borderColor: tinycolor(
                        that.categoryColorMap.get(item.category)
                      )
                        .darken(10)
                        .toHexString(),
                      borderWidth:"1px",
                      borderStyle:"solid",
                      color:"rgba(0, 0, 0, 0.65)"
                    }}
                  >
                    {item.category}
                  </div>
                  <div className="vl" />
                  <div className={"q-info-created" + that.state.dark}>
                    {localTime.toString()}
                  </div>
                </div>
              );
            });
            vals = vals.sort(function(a, b) {
              return a.activity - b.activity;
            });
            break;
          }
          default: {
            vals = [];
          }
        }
      }
    }
    if (this.state.valid && vals && vals.length === 0 && this.state.gotFeed) {
      if (this.props.contentType === 3) {
        vals = (
          <div
            className={"no-articles" + this.state.dark}
            style={{ textAlign: "center" }}
          >
            There are currently no posts to show in your feed.{" "}
            <span
              style={{ color: "#1890ff", cursor: "pointer" }}
              onClick={this.handleFollowCatVisible}
            >
              Follow more categories
            </span>{" "}
            or users for more posts.
          </div>
        );
      } else if (this.props.contentType === 2) {
        vals = (
          <div
            className={"no-articles" + this.state.dark}
            style={{ textAlign: "center" }}
          >
            {this.props.category
              ? "There are no articles to show in " + this.props.category
              : "There are no articles to show."}
          </div>
        );
      } else {
        let feedTypeStr = this.props.feedType === 1 ? "trending" : "latest";
        vals = (
          <div style={{ textAlign: "center" }}>
            {this.props.category
              ? "There are no " +
                feedTypeStr +
                " questions to show in " +
                this.props.category
              : "There are no " + feedTypeStr + " questions to show."}
            <div>
              {this.props.feedType === 1 ? (
                <div
                  onClick={() => {
                    this.props.goToLatest();
                  }}
                  className="view-latest-posts-button"
                >
                  View latest posts
                </div>
              ) : null}
            </div>
          </div>
        );
      }
    }

    if (this.state.valid && !this.state.gotFeed && !this.state.warn) {
      /*loading animation here*/
      var loadingAnim = (
        <center>
          <Spin
            indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
          />
        </center>
      );
    }

    if (this.props.contentType === 3 && this.state.warn) {
      vals = (
        <div
          className={"no-feed-init" + this.state.dark}
          style={{ textAlign: "center" }}
        >
          <div className="no-feed-init-content">
            <div>Follow some categories to get started.</div>
            <p />
            <div>
              <Button type="primary" onClick={this.handleFollowCatVisible}>
                Setup Feed
              </Button>
            </div>
          </div>
        </div>
      );
    }

    let followVals = [];
    this.props.categoryColorMap.forEach((val, key) => {
      let i = indexOf(this.state.followingCats, key);
      followVals.push(
        <div className="follow-cat-individual" key={key}>
          {key}
          <div className="follow-cat-individual-follow-button-div">
            <Button
              type={i > -1 ? "default" : "primary"}
              onClick={
                i > -1
                  ? () => {
                      this.unfollowCategory(key);
                    }
                  : () => {
                      this.followCategory(key);
                    }
              }
            >
              {i > -1 ? "Unfollow" : "Follow"}
            </Button>
          </div>
        </div>
      );
    });
    if (followVals.length > 0 && this.state.gotFollowCats) {
      followVals = (
        <div>
          Note: To follow a user, go to the user's profile and click
          
          <div style={{textAlign:"center", color:"#000000a6"}} className={"message-span profile follow" + this.state.dark}>
            Follow
          </div>
          <Masonry
            breakpointCols={{
              default: 4,
              1100: 4,
              870: 3,
              685: 2,
              450: 1
            }}
            className="my-masonry-grid"
            columnClassName="my-masonry-grid_column"
          >
            {followVals}
          </Masonry>
        </div>
      );
    } else {
      followVals = (
        <center>
          <Spin
            indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
          />
        </center>
      );
    }

    return (
      <React.Fragment>
        <div className="feed-content">
          {this.state.error ? (
            <Alert type="error" message={this.state.error} />
          ) : null}
          {loadingAnim}
          {vals}
        </div>
        <Modal
          className="follow-categories"
          visible={this.state.followCatVisible}
          onCancel={() => {
            this.setState({ followCatVisible: false, warn: null });
            this.getFeed()
          }}
          footer={null}
          title="Follow"
        >
          {followVals}
        </Modal>
      </React.Fragment>
    );
  }
}

export default withRouter(withCookies(Feed));
