import React, { Component } from "react";
import { Row, Col, Button, Input, Icon, Select, Checkbox, Alert } from "antd";
import "./SignUp.css";
import axios from "axios";
import { withRouter, Link } from "react-router-dom";

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.backUrl = "";
  }

  componentDidMount() {
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
  }

  handleGenderChange = e => {
    this.setState({
      ...this.state,
      gender: e
    });
  };

  passwordChange = e => {
    this.setState({
      ...this.state,
      password: e.target.value
    });
  };

  handleFname = e => {
    this.setState({
      ...this.state,
      fname: e.target.value
    });
  };

  handleLname = e => {
    this.setState({
      ...this.state,
      lname: e.target.value
    });
  };

  handleEmail = e => {
    this.setState({
      ...this.state,
      email: e.target.value
    });
  };

  handleCheckbox = e => {
    this.setState({
      ...this.state,
      checked: e.target.checked
    });
  };

  signup = () => {
    if (!this.state.fname) {
      this.setState({
        ...this.state,
        error: "Please enter a valid first name"
      });
      return;
    }
    if (this.state.fname.length > 15) {
      this.setState({
        ...this.state,
        error: "First name too long. Max 15 characters"
      });
      return;
    }
    if (this.state.lname && this.state.lname.length > 15) {
      this.setState({
        ...this.state,
        error: "Last name too long. Max 15 characters"
      });
      return;
    }
    if (!this.state.email) {
      this.setState({
        ...this.state,
        error: "Please enter a valid email"
      });
      return;
    }
    if (!this.state.password) {
      this.setState({
        ...this.state,
        error: "Please enter a valid email"
      });
      return;
    }
    if (!this.state.checked) {
      this.setState({
        ...this.state,
        error: "You have not accepted the terms"
      });
      return;
    }
    this.setState({
      ...this.state,
      loading: true,
      error: null
    });
    axios({
      method: "POST",
      url: this.backUrl + "createUser",
      headers: {
        "Content-Type": "text/plain"
      },
      data: {
        fname: this.state.fname,
        lname: this.state.lname,
        email: this.state.email,
        gender: this.state.gender,
        password: this.state.password
      }
    })
      .then(e => {
        if (e.data && e.data.success) {
          this.setState({
            ...this.state,
            done: true,
            successMessage: e.data.msg,
            loading: false
          });
        } else if (e.data && e.data.msg) {
          this.setState({
            ...this.state,
            loading: false,
            error: e.data.msg
          });
        } else {
          this.setState({
            ...this.state,
            loading: false,
            error: "Something went wrong. Please try again later."
          });
        }
      })
      .catch(e => {
        this.setState({
          ...this.state,
          loading: false,
          error: "Something went wrong. Please try again later."
        });
      });
  };

  render() {
    return (
      <Row
        type="flex"
        justify="center"
        className="signup-row"
        align="middle"
      >
        <Col
          xs={24}
          sm={24}
          md={18}
          lg={16}
          xl={14}
          xxl={8}
          className="signup-col"
        >
          <h1 className="welcome-text">Sign Up</h1>
          <Input
            disabled={this.state.done}
            autoFocus
            required
            className="signup-field"
            placeholder="First Name"
            onChange={this.handleFname}
          />
          <br />
          <Input
            disabled={this.state.done}
            className="signup-field"
            placeholder="Last Name (optional)"
            onChange={this.handleLname}
          />
          <br />
          <Input
            disabled={this.state.done}
            required
            className="signup-field"
            placeholder="Email"
            onChange={this.handleEmail}
          />
          <br />
          <Input.Password
            disabled={this.state.done}
            className="signup-field"
            required
            placeholder="Enter a password"
            prefix={<Icon type="lock" />}
            onChange={this.passwordChange}
          />
          <br />
          <Select
            disabled={this.state.done}
            className="gender-select"
            placeholder="Gender"
            onChange={this.handleGenderChange}
          >
            <Select.Option value="Male">Male</Select.Option>
            <Select.Option value="Female">Female</Select.Option>
            <Select.Option value="Other">Other</Select.Option>
            <Select.Option value="">Prefer not to say</Select.Option>
          </Select>
          <br />
          <div className="terms-div">
            <Checkbox
              disabled={this.state.done}
              onChange={this.handleCheckbox}
            >
              I agree to the{" "}
              <a href="#" target="_blank" rel="noopener noreferrer">
                terms and conditions
              </a>
              <span>, </span>
              <a href="#" target="_blank" rel="noopener noreferrer">
                privacy policy
              </a>
              <span> </span>
              and<span> </span>
              <a href="#" target="_blank" rel="noopener noreferrer">
                cookie policy
              </a>
            </Checkbox>
          </div>
          <Link to="/Login">
            <p>
              Already have an account? Click here to login
            </p>
          </Link>
          <Button
            disabled={this.state.done}
            style={{ margin: "1em" }}
            onClick={this.signup}
            loading={this.state.loading}
          >
            Create Account
          </Button>
          {this.state.error ? (
            <div style={{ padding: "1em" }}>
              <Alert type="error" message={this.state.error} />
            </div>
          ) : null}
          {this.state.done ? (
            <div style={{ padding: "1em" }}>
              <Alert type="success" message={this.state.successMessage} />
            </div>
          ) : null}
        </Col>
      </Row>
    );
  }
}

export default withRouter(SignUp);
