import React, { Component } from "react";
import { withCookies } from "react-cookie";
import axios from "axios";
import Resizer from "react-image-file-resizer";
import moment from "moment";
import {
  Spin,
  Icon,
  Row,
  Button,
  message,
  Modal,
  Input,
  Select,
  Alert,
  Upload,
  Avatar,
} from "antd";
import Header from "./Header";
import "./Profile.css";
import unknownUser from "./res/unknown-user.png";
import { withRouter } from "react-router-dom";
import { map, sortBy, reverse, remove, cloneDeep } from "lodash";
import Password from "antd/lib/input/Password";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selector: 1,
      bookmarkCat: 1,
      followCat: 1
    };
    this.backUrl = "";
    this.countryToCode = {
      Bangladesh: "BD",
      Belgium: "BE",
      "Burkina Faso": "BF",
      Bulgaria: "BG",
      "Bosnia and Herzegovina": "BA",
      Barbados: "BB",
      "Wallis and Futuna": "WF",
      "Saint Barthelemy": "BL",
      Bermuda: "BM",
      Brunei: "BN",
      Bolivia: "BO",
      Bahrain: "BH",
      Burundi: "BI",
      Benin: "BJ",
      Bhutan: "BT",
      Jamaica: "JM",
      "Bouvet Island": "BV",
      Botswana: "BW",
      Samoa: "WS",
      "Bonaire, Saint Eustatius and Saba ": "BQ",
      Brazil: "BR",
      Bahamas: "BS",
      Jersey: "JE",
      Belarus: "BY",
      Belize: "BZ",
      Russia: "RU",
      Rwanda: "RW",
      Serbia: "RS",
      "East Timor": "TL",
      Reunion: "RE",
      Turkmenistan: "TM",
      Tajikistan: "TJ",
      Romania: "RO",
      Tokelau: "TK",
      "Guinea-Bissau": "GW",
      Guam: "GU",
      Guatemala: "GT",
      "South Georgia and the South Sandwich Islands": "GS",
      Greece: "GR",
      "Equatorial Guinea": "GQ",
      Guadeloupe: "GP",
      Japan: "JP",
      Guyana: "GY",
      Guernsey: "GG",
      "French Guiana": "GF",
      Georgia: "GE",
      Grenada: "GD",
      "United Kingdom": "GB",
      Gabon: "GA",
      "El Salvador": "SV",
      Guinea: "GN",
      Gambia: "GM",
      Greenland: "GL",
      Gibraltar: "GI",
      Ghana: "GH",
      Oman: "OM",
      Tunisia: "TN",
      Jordan: "JO",
      Croatia: "HR",
      Haiti: "HT",
      Hungary: "HU",
      "Hong Kong": "HK",
      Honduras: "HN",
      "Heard Island and McDonald Islands": "HM",
      Venezuela: "VE",
      "Puerto Rico": "PR",
      "Palestinian Territory": "PS",
      Palau: "PW",
      Portugal: "PT",
      "Svalbard and Jan Mayen": "SJ",
      Paraguay: "PY",
      Iraq: "IQ",
      Panama: "PA",
      "French Polynesia": "PF",
      "Papua New Guinea": "PG",
      Peru: "PE",
      Pakistan: "PK",
      Philippines: "PH",
      Pitcairn: "PN",
      Poland: "PL",
      "Saint Pierre and Miquelon": "PM",
      Zambia: "ZM",
      "Western Sahara": "EH",
      Estonia: "EE",
      Egypt: "EG",
      "South Africa": "ZA",
      Ecuador: "EC",
      Italy: "IT",
      Vietnam: "VN",
      "Solomon Islands": "SB",
      Ethiopia: "ET",
      Somalia: "SO",
      Zimbabwe: "ZW",
      "Saudi Arabia": "SA",
      Spain: "ES",
      Eritrea: "ER",
      Montenegro: "ME",
      Moldova: "MD",
      Madagascar: "MG",
      "Saint Martin": "MF",
      Morocco: "MA",
      Monaco: "MC",
      Uzbekistan: "UZ",
      Myanmar: "MM",
      Mali: "ML",
      Macao: "MO",
      Mongolia: "MN",
      "Marshall Islands": "MH",
      Macedonia: "MK",
      Mauritius: "MU",
      Malta: "MT",
      Malawi: "MW",
      Maldives: "MV",
      Martinique: "MQ",
      "Northern Mariana Islands": "MP",
      Montserrat: "MS",
      Mauritania: "MR",
      "Isle of Man": "IM",
      Uganda: "UG",
      Tanzania: "TZ",
      Malaysia: "MY",
      Mexico: "MX",
      Israel: "IL",
      France: "FR",
      "British Indian Ocean Territory": "IO",
      "Saint Helena": "SH",
      Finland: "FI",
      Fiji: "FJ",
      "Falkland Islands": "FK",
      Micronesia: "FM",
      "Faroe Islands": "FO",
      Nicaragua: "NI",
      Netherlands: "NL",
      Norway: "NO",
      Namibia: "NA",
      Vanuatu: "VU",
      "New Caledonia": "NC",
      Niger: "NE",
      "Norfolk Island": "NF",
      Nigeria: "NG",
      "New Zealand": "NZ",
      Nepal: "NP",
      Nauru: "NR",
      Niue: "NU",
      "Cook Islands": "CK",
      Kosovo: "XK",
      "Ivory Coast": "CI",
      Switzerland: "CH",
      Colombia: "CO",
      China: "CN",
      Cameroon: "CM",
      Chile: "CL",
      "Cocos Islands": "CC",
      Canada: "CA",
      "Republic of the Congo": "CG",
      "Central African Republic": "CF",
      "Democratic Republic of the Congo": "CD",
      "Czech Republic": "CZ",
      Cyprus: "CY",
      "Christmas Island": "CX",
      "Costa Rica": "CR",
      Curacao: "CW",
      "Cape Verde": "CV",
      Cuba: "CU",
      Swaziland: "SZ",
      Syria: "SY",
      "Sint Maarten": "SX",
      Kyrgyzstan: "KG",
      Kenya: "KE",
      "South Sudan": "SS",
      Suriname: "SR",
      Kiribati: "KI",
      Cambodia: "KH",
      "Saint Kitts and Nevis": "KN",
      Comoros: "KM",
      "Sao Tome and Principe": "ST",
      Slovakia: "SK",
      "South Korea": "KR",
      Slovenia: "SI",
      "North Korea": "KP",
      Kuwait: "KW",
      Senegal: "SN",
      "San Marino": "SM",
      "Sierra Leone": "SL",
      Seychelles: "SC",
      Kazakhstan: "KZ",
      "Cayman Islands": "KY",
      Singapore: "SG",
      Sweden: "SE",
      Sudan: "SD",
      "Dominican Republic": "DO",
      Dominica: "DM",
      Djibouti: "DJ",
      Denmark: "DK",
      "British Virgin Islands": "VG",
      Germany: "DE",
      Yemen: "YE",
      Algeria: "DZ",
      "United States": "US",
      Uruguay: "UY",
      Mayotte: "YT",
      "United States Minor Outlying Islands": "UM",
      Lebanon: "LB",
      "Saint Lucia": "LC",
      Laos: "LA",
      Tuvalu: "TV",
      Taiwan: "TW",
      "Trinidad and Tobago": "TT",
      Turkey: "TR",
      "Sri Lanka": "LK",
      Liechtenstein: "LI",
      Latvia: "LV",
      Tonga: "TO",
      Lithuania: "LT",
      Luxembourg: "LU",
      Liberia: "LR",
      Lesotho: "LS",
      Thailand: "TH",
      "French Southern Territories": "TF",
      Togo: "TG",
      Chad: "TD",
      "Turks and Caicos Islands": "TC",
      Libya: "LY",
      Vatican: "VA",
      "Saint Vincent and the Grenadines": "VC",
      "United Arab Emirates": "AE",
      Andorra: "AD",
      "Antigua and Barbuda": "AG",
      Afghanistan: "AF",
      Anguilla: "AI",
      "U.S. Virgin Islands": "VI",
      Iceland: "IS",
      Iran: "IR",
      Armenia: "AM",
      Albania: "AL",
      Angola: "AO",
      Antarctica: "AQ",
      "American Samoa": "AS",
      Argentina: "AR",
      Australia: "AU",
      Austria: "AT",
      Aruba: "AW",
      India: "IN",
      "Aland Islands": "AX",
      Azerbaijan: "AZ",
      Ireland: "IE",
      Indonesia: "ID",
      Ukraine: "UA",
      Qatar: "QA",
      Mozambique: "MZ"
    };
  }

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    let sId = this.props.cookies.get("session_id");
    let uId = this.props.cookies.get("u_id");
    if (!sId) {
      this.setState({
        error: "You need to login to continue."
      });
      this.props.history.push("/login");
      return;
    } else {
      this.setState({
        sessionId: sId,
        uId: uId
      });
      this.callGetUserDetails(sId);
      this.callGetUserBio(uId);
    }
  }

  componentDidUpdate() {}

  callGetUserDetails(sId) {
    axios({
      url: this.backUrl + "getUserDetails",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: sId
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            userData: res.data.data,
            gotData: true
          });
        } else {
          if (res && res.data && res.data.msg) {
            if (res.data.msg === "You need to login to continue.") {
              message.warn(res.data.msg);
              let cookiesObj = this.props.cookies.getAll();
              for (var key in cookiesObj) {
                this.props.cookies.remove(key, { path: "/" });
              }
            }
            this.setState({
              error: res.data.msg
            });
          } else {
            this.setState({
              error: "Something went wrong while fetching profile information."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          error: "Something went wrong while fetching profile information."
        });
      });
  }

  callGetUserBio(uId) {
    let specialOverrideNounce = moment.now().toString()
    axios({
      url: this.backUrl + "getUserBio",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        "cache-override-nounce": specialOverrideNounce
      },
      data: {
        u_id: parseInt(uId)
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            bioData: res.data.data
          });
        } else {
          if (res && res.data && res.data.msg) {
            if (res.data.msg === "You need to login to continue.") {
              message.warn(res.data.msg);
              let cookiesObj = this.props.cookies.getAll();
              for (var key in cookiesObj) {
                this.props.cookies.remove(key, { path: "/" });
              }
            }
            this.setState({
              bioError: res.data.msg
            });
          } else {
            this.setState({
              bioError: "Something went wrong while fetching bio."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          bioError: "Something went wrong while fetching bio."
        });
      });
  }

  handleDescriptionChange = val => {
    this.setState({
      editableDescription: val.target.value,
      bioEditError: null
    });
  };

  handleCountryChange = val => {
    this.setState({
      editableCountry: val,
      bioEditError: null
    });
  };

  handleProfileButtonClick = () => {
    this.setState({
      selector: 3
    });
  };

  handleBioButtonClick = () => {
    this.setState({
      selector: 1
    });
  };

  handleBookmarkButtonClick = () => {
    this.setState({
      selector: 2
    });
    if(!this.gotBookmarks) {
      axios({
        url: this.backUrl+"getAllUserBookmarks",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          "session_id": this.props.cookies.get("session_id")
        }
      })
      .then(res => {
        this.gotBookmarks = true
        if(res && res.data && res.data.success) {
          this.setState({
            bookmarks: reverse(res.data.data)
          })
        } else {
          if(res && res.data && res.data.msg) {
            this.setState({
              bookmarkError: res.data.msg
            })
          } else {
            this.setState({
              bookmarkError: "Something went wrong while fetching bookmarks."
            })
          }
        }
      })
      .catch(e => {
        this.gotBookmarks = true
        this.setState({
          bookmarkError: "Something went wrong while fetching bookmarks."
        })
      })
    }
  };

  handleClickEdit = () => {
    if (this.state.selector === 3) {
      this.setState({
        disabled: false,
        infoEditError: null,
        profileEditModalVisible: true,
        editableFname: this.state.userData && this.state.userData.fname,
        editableLname: this.state.userData && this.state.userData.lname,
        editableGender: this.state.userData && this.state.userData.gender,
        editableDpUrl: this.state.userData && this.state.userData.dp_url
      });
    } else if (this.state.selector === 1) {
      this.countrySelectVals = map(this.countryToCode, function(val, key) {
        return (
          <Select.Option value={key} key={key}>
            {key + " (" + val + ")"}
          </Select.Option>
        );
      });
      this.countrySelectVals.push(
        <Select.Option value={""} key={999}>
          --prefer not to say--
        </Select.Option>
      );
      this.countrySelectVals = sortBy(this.countrySelectVals, function(o) {
        return o.props.value;
      });
      let cString;
      var that = this;
      if (
        this.state.bioData &&
        this.state.bioData.country &&
        this.state.bioData.country
      ) {
        map(this.countryToCode, function(val, key) {
          if (that.state.bioData.country === val) {
            cString = key;
          }
        });
      }

      this.setState({
        bioEditModalVisible: true,
        editableCountry: cString,
        editableDescription:
          this.state.bioData && this.state.bioData.description,
        bioEditError: null
      });
    }
  };

  handleFnameChange = val => {
    this.setState({
      infoEditError: null,
      editableFname: val.target.value
    });
  };

  handleLnameChange = val => {
    this.setState({
      infoEditError: null,
      editableLname: val.target.value
    });
  };

  handleGenderChange = val => {
    this.setState({
      infoEditError: null,
      editableGender: val
    });
  };

  profileEditOk = () => {
    this.setState({
      disabled: true
    });
    let that = this;
    if (this.state.imageToUpload) {
      this.setState({
        imageLoading: true
      });
      axios({
        url: this.backUrl + "uploadAvatar",
        method: "POST",
        headers: {
          "Content-type": "multipart/form-data",
          session_id: that.state.sessionId
        },
        data: this.state.imageToUpload
      })
        .then(res => {
          that.setState({
            imageLoading: false
          });
          if (res && res.data && res.data.success) {
            axios({
              url: that.backUrl + "updateUser",
              method: "POST",
              headers: {
                "Content-type": "text/plain",
                session_id: that.state.sessionId
              },
              data: {
                fname: that.state.editableFname,
                lname: that.state.editableLname,
                gender: that.state.editableGender,
                dp_url: res.data.data
              }
            })
              .then(res => {
                if (res && res.data && res.data.success) {
                  let uuid = that.props.cookies.get("u_id")
                  if(uuid && uuid.length > 0) {
                    var cookieKey = "user:"+uuid;
                    that.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(1200, 'seconds').toDate()})
                  }
                  message.success("Profile successfully updated");
                  that.setState({
                    profileEditModalVisible: false,
                    disabled: false
                  });
                  that.callGetUserDetails(that.state.sessionId);
                  window.location.reload();
                } else {
                  if (res && res.data && res.data.msg) {
                    that.setState({
                      disabled: false,
                      infoEditError: res.data.msg
                    });
                  } else {
                    that.setState({
                      disabled: false,
                      infoEditError:
                        "Something went wrong while updating profile. Please try again later."
                    });
                  }
                }
              })
              .catch(e => {
                that.setState({
                  disabled: false,
                  infoEditError:
                    "Something went wrong while updating profile. Please try again later."
                });
              });
          } else {
            message.error(
              res.data && res.data.msg
                ? res.data.msg
                : "Something went wrong while uploading image. Please try again later."
            );
            this.setState({
              disabled: false
            });
          }
        })
        .catch(e => {
          message.error(
            "Something went wrong while uploading image. Please try again later."
          );
          this.setState({
            disabled: false
          });
        });
    } else {
      axios({
        url: this.backUrl + "updateUser",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          session_id: this.state.sessionId
        },
        data: {
          fname: this.state.editableFname,
          lname: this.state.editableLname,
          gender: this.state.editableGender,
          dp_url: this.state.editableDpUrl
        }
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            let uuid = that.props.cookies.get("u_id")
            if(uuid && uuid.length > 0) {
              var cookieKey = "user:"+uuid;
              that.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(1200, 'seconds').toDate()})
            }
            message.success("Profile successfully updated");
            that.setState({
              profileEditModalVisible: false,
              disabled: false
            });
            that.callGetUserDetails(that.state.sessionId);
            // window.location.reload();
          } else {
            if (res && res.data && res.data.msg) {
              that.setState({
                disabled: false,
                infoEditError: res.data.msg
              });
            } else {
              that.setState({
                disabled: false,
                infoEditError:
                  "Something went wrong while updating profile. Please try again later."
              });
            }
          }
        })
        .catch(e => {
          that.setState({
            disabled: false,
            infoEditError:
              "Something went wrong while updating profile. Please try again later."
          });
        });
    }
  };

  uploadImage = file => {};

  bioEditOk = () => {
    this.setState({
      disabled: true
    });
    axios({
      url: this.backUrl + "callAddOrUpdateUserBio",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.state.sessionId
      },
      data: {
        country: this.state.editableCountry,
        description: this.state.editableDescription
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          let uuid = this.props.cookies.get("u_id")
          if(uuid && uuid.length > 0) {
            var cookieKey = "user:"+uuid;
            this.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(1200, 'seconds').toDate()})
        }
          message.success(res.data.msg);
          this.setState({
            bioEditModalVisible: false,
            bioEditError: null,
            disabled: false
          });
          this.callGetUserBio(this.state.uId);
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              disabled: false,
              bioEditError: res.data.msg
            });
          } else {
            this.setState({
              disabled: false,
              bioEditError:
                "Something went wrong while updating bio. Please try again later."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          disabled: false,
          bioEditError:
            "Something went wrong while updating bio. Please try again later."
        });
      });
  };

  beforeImageUpload = file => {
    const isJPG = file.type === "image/jpeg";
    if (!isJPG) {
      message.error("You can only upload JPG images");
      return false;
    }
    const isLt2M = file.size / 1024 / 1024 < 10;
    if (!isLt2M) {
      message.error("Image must smaller than 10MB");
      return false;
    }
    var that = this;
    Resizer.imageFileResizer(
      file,
      300,
      300,
      "JPEG",
      100,
      0,
      uri => {
        var reader = new FileReader();
        reader.onload = function(e) {
          var img = uri.match(/,(.*)$/)[1];
          that.setState({
            editableImageData: e.target.result,
            imageToUpload: img
          });
        };
        reader.readAsDataURL(file);
        return false;
      },
      "base64"
    );
  };

  handleChangePassword = () => {
    this.setState({
      passwordModalVisible: true,
      passwordChangeFieldsDisabled: false,
      oldPassword: null,
      newPassword: null,
      newConfirmPassword: null
    });
  };

  handleNewPasswordChange = val => {
    this.setState({
      newPassword: val.target.value,
      changePasswordError: null
    });
  };

  handleOldPasswordChange = val => {
    this.setState({
      oldPassword: val.target.value,
      changePasswordError: null
    });
  };

  handleNewConfirmPasswordChange = val => {
    this.setState({
      newConfirmPassword: val.target.value,
      changePasswordError: null
    });
  };

  handlePasswordChange = () => {
    if (
      !this.state.oldPassword ||
      (this.state.oldPassword && this.state.oldPassword.length < 8)
    ) {
      this.setState({
        changePasswordError: "Please enter a valid old password"
      });
      return;
    }
    if (!this.state.newPassword) {
      this.setState({
        changePasswordError: "Please enter a valid new password"
      });
      return;
    }

    if (
      (this.state.newPassword && this.state.newPassword.length < 8) ||
      this.state.oldPassword.length > 20
    ) {
      this.setState({
        changePasswordError:
          "Invalid Password. Password must have Minimum 8 and Maximum 20 characters."
      });
      return;
    }

    if (this.state.oldPassword === this.state.newPassword) {
      this.setState({
        changePasswordError: "New password cannot be same as old password."
      });
      return;
    }

    if (!this.state.newConfirmPassword) {
      this.setState({
        changePasswordError: "Please confirm your new password"
      });
      return;
    }

    if (this.state.newPassword !== this.state.newConfirmPassword) {
      this.setState({
        changePasswordError: "The new passwords do not match."
      });
      return;
    }

    this.setState({
      passwordChangeFieldsDisabled: true
    });

    axios({
      url: this.backUrl + "changePassword",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.state.sessionId
      },
      data: {
        old_password: this.state.oldPassword,
        new_password: this.state.newPassword,
        repeat_new_password: this.state.newConfirmPassword
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          message.success(
            "Your account password has been sucessfully changed. Please login again"
          );
          let cookiesObj = this.props.cookies.getAll();
          for (var key in cookiesObj) {
            this.props.cookies.remove(key, { path: "/" });
          }
          setTimeout(() => {
            this.props.history.push("/login");
          }, 1000);
        } else {
          if (res && res.data.msg) {
            this.setState({
              changePasswordError: res.data.msg
            });
          } else {
            this.setState({
              changePasswordError:
                "Something went wrong while changing password. Please try again later."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          changePasswordError:
            "Something went wrong while changing password. Please try again later."
        });
      });
  };

  deleteBookmark = (b_id)=> {
    if(b_id > 0) {
      axios({
        url:this.backUrl+"deleteBookmark",
        method: "POST",
        headers: {
          "Content-type":"text/plain",
          "session_id":this.props.cookies.get("session_id")
        },
        data: {
          "b_id":b_id
        }
      })
      .then((res)=>{
        if(res && res.data && res.data.success) {
          this.gotBookmarks = false;
          this.handleBookmarkButtonClick()
        }
      })
    }
  }

  handleProButtonClick = () => {
    this.setState({
      selector: 4
    })
    if(!this.gotProDetails) {
     
    }
  }

  handleFollowingButtonClick = () => {
    this.setState({
      selector: 5
    })
    if(!this.gotFollowingUsers) {
      this.gotFollowingUsers = true
      axios({
        url:this.backUrl+"getFollowingUsers",
        method:"POST",
        headers: {
          "Content-type":"text/plain",
          "session_id": this.props.cookies.get("session_id")
        }
      })
      .then(res => {
        if(res && res.data && res.data.success) {
          this.setState({
            followData: res.data.data
          })
        } else {
          if(res && res.data && res.data.msg) {
            this.setState({
              followDataError: res.data.msg
            })
          } else {
            this.setState({
              followDataError: "Something went wrong. Please try again later."
            })
          }
        }
      })
      .catch(e => {
        this.setState({
          followDataError: "Something went wrong. Please try again later."
        })
      })
      axios({
        url:this.backUrl+"getAllUserFollowers",
        method:"POST",
        headers: {
          "Content-type":"text/plain",
          "session_id": this.props.cookies.get("session_id")
        }
      })
      .then(res => {
        if(res && res.data && res.data.success) {
          this.setState({
            followData1: res.data.data
          })
        } else {
          if(res && res.data && res.data.msg) {
            this.setState({
              followData1Error: res.data.msg
            })
          } else {
            this.setState({
              followData1Error: "Something went wrong. Please try again later."
            })
          }
        }
      })
      .catch(e => {
        this.setState({
          followData1Error: "Something went wrong. Please try again later."
        })
      })
    }
  }

  unfollowUser =(uid) => {
    if(uid && uid > 0) {
      axios({
        url: this.backUrl+"unFollow",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          "session_id": this.props.cookies.get("session_id")
        },
        data: {
          "type":1,
          "fu_id": uid
        }
      })
      .then(res => {
        if( res && res.data && res.data.success) {
          let tempFollowData = cloneDeep(this.state.followData)
          remove(tempFollowData, o => {
            return o.u_id === uid
          })
          this.setState({
            followData: tempFollowData
          })
        } else if(res && res.data && res.data.msg) {
          message.error(res.data.msg)
        } else {
          message.error("Something went wrong. Please try again later.")
        }
      })
    }
  }

  handleDeleteAccount =() => {
    message.config({
  top: 50,
  duration: 2,
  maxCount: 3,
});
    Modal.confirm({
      title:"Delete Account",
      content: (<div>
        <div>
        This will delete your account permanently. This action cannot be undone.
        </div><p/>
        <div>
          <Password onChange={(e) => {this.setState({delAccPassword:e.target.value})}} name="confirm-password" autocomplete="new-password" placeholder={"Enter your password."}></Password>
        </div>
      </div>),
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onCancel:(e)=>{
        this.setState({delAccPassword: null})
        e()
      },
      onOk:(e) => {
        if(!this.state.delAccPassword) {
          message.error("Please enter your password.")
          return
        }
        let uuid = this.props.cookies.get("u_id")
        if(uuid && uuid.length > 0) {
          var cookieKey = "user:"+uuid;
          this.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(1200, 'seconds').toDate()})
        }
        
        axios({
          url: this.backUrl+"deleteUser",
          method: "POST",
          headers: {
            "session_id": this.props.cookies.get("session_id")
          },
          data: {
            "password": this.state.delAccPassword
          }
        })
        .then(res => {
          if(res && res.data && res.data.success) {
            message.success("Successfully deleted account")
            setTimeout(() => {
              let cookiesObj = this.props.cookies.getAll();
            for (var key in cookiesObj) {
              this.props.cookies.remove(key, { path: "/" });
            }
            window.location.reload();
            }, 3000)
          } else {
            if(res && res.data && res.data.msg) {
              message.error(res.data.msg)
            } else {
              message.error("Something went wrong. Please try again later.")
            }
          }
        })
      }
    })
  }

  render() {
    let userInfo, bioInfo, bookmarkInfo, content, bioActive, userActive, bookmarkActive, proInfo, followingActive, followingInfo,followingInfo1,followingInfo2

    if (this.state.userData && !this.state.error) {
      let dpUrl = this.state.userData.dp_url
        ? this.state.userData.dp_url
        : unknownUser;
      userInfo = (
        <div className={"profile-all"+this.state.dark}>
          <Button
            onClick={this.handleClickEdit}
            type="primary"
            disabled={this.state.error || !this.state.gotData ? true : false}
            className="profile-edit-profile-bio"
          >
            <Icon type="edit" />
            {"Edit Account"}
          </Button>
            <div
              className="profile-dp"
              style={{ backgroundImage: 'url("' + dpUrl + '")' }}
            />
        
          <div className="profile-divider">
            <div className="profile-title">First Name</div>
            <div className="profile-name">
              {this.state.userData.fname}
            </div>
          </div>
          <div className="profile-divider">
            <div className="profile-title">Last Name</div>
            <div className="profile-name">
              {this.state.userData.lname}
            </div>
          </div>
          <div className="profile-divider">
            <div className="profile-title">Email</div>
            <div>{this.state.userData.email}</div>
          </div>
          <div className="profile-divider">
            <div className="profile-title">Password</div>
          <Button
            onClick={this.handleChangePassword}
            disabled={this.state.error || !this.state.gotData ? true : false}
            className={"profile-other-operations-button"+this.state.dark}
          >
            <Icon type="lock" />
            Change Password
            </Button>
          </div>
          <div className="profile-divider">
            <div className="profile-title">Gender</div>
            <div className="profile-gender">
              {this.state.userData.gender}
            </div>
          </div>
          <div className="profile-divider">
          <div className="profile-title">Points</div>
          <div>{this.state.userData.points}</div>
          </div>
          <div 
          onClick={() => {this.handleDeleteAccount()}}
          style={{color:"rgba(255,0,0,0.8)", fontSize:"0.8em", cursor:"pointer"}}>
          Permanently Delete Account
          </div>
          
          {/* 
                  <td align="right">Pro:</td>
                  <td> {this.state.userData.pro ? "Yes" : "No"}</td>
               */}
        </div>
      );
    } else if (this.state.error) {
      userInfo = <div className={"profile-all" + this.state.dark}>{this.state.error}</div>;
    } else {
      userInfo = (
        <div className={"profile-all" + this.state.dark}>
        <Spin
          indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
          /></div>
      );
    }

    if (
      this.state.bioData &&
      !this.state.bioError &&
      (this.state.bioData.country || this.state.bioData.description)
    ) {
      let countryName
      map(this.countryToCode, (i, o) => {
        if(this.state.bioData.country === i) {
          countryName = o
        }  
      })
      bioInfo = (
        <div className={"profile-all" + this.state.dark}>
          <Button
            onClick={this.handleClickEdit}
            type="primary"
            disabled={this.state.error || !this.state.gotData ? true : false}
            className="profile-edit-profile-bio"
          >
            <Icon type="edit" />
            {"Edit Bio"}
          </Button>
          <div className="profile-divider">
            <div className="profile-title">Description</div>
            <div id="profile-description">
              {" "}
              {this.state.bioData.description}
            </div>
          </div>
          <div className="profile-title"> Country</div>
          <div>
            {" "}
            {countryName}

          </div>
        </div>
      );
    } else if (
      this.state.bioError ||
      !(
        (this.state.bioData && this.state.bioData.country) ||
        (this.state.bioData && this.state.bioData.description)
      )
    ) {
      bioInfo = (
        <div className={"profile-all" + this.state.dark}>
          <Button
            onClick={this.handleClickEdit}
            type="primary"
            disabled={this.state.error || !this.state.gotData ? true : false}
            className="profile-edit-profile-bio"
          >
            <Icon type="edit" />
            {"Edit Bio"}
          </Button>
          {this.state.bioError || "No bio availabe for this user."}
       </div>
      );
    } else {
      bioInfo = (
        <div className={"profile-all" + this.state.dark}>
        <Spin
          indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
        />
        </div>
      );
    }

    if(this.gotBookmarks && this.state.bookmarks) {
      if(this.state.bookmarks.length < 1) {
        bookmarkInfo = (
          <div className={"profile-all" + this.state.dark}>
            <div>
              <Button onClick={() => { this.setState({ bookmarkCat: 1 }) }} className={"bookmark-cat-button"+(this.state.bookmarkCat === 1?" active":"")}>Questions</Button>
              <Button onClick={() => { this.setState({ bookmarkCat: 2 }) }} className={"bookmark-cat-button" + (this.state.bookmarkCat === 1 ? "" : " active")}>Articles</Button>
            </div>
            <div style={{textAlign:"center", width:"100%", marginTop:"1em"}}>No Bookmarks</div>
          </div>
        );
      } else {
        let bookmarkCat1 = []
        let bookmarkCat2 = []
        map(this.state.bookmarks, (item, i) =>  {
          if(item.type && item.type === 1) {
            bookmarkCat1.push(
              <div key={i} className="bookmark-title-div">
                <div className="bookmark-delete">
                  <Icon type="close-circle" onClick={() => {this.deleteBookmark(item.b_id)}}/>
                </div>
                <div
                  className="bookmark-title"
                  onClick={() => {
                    this.props.history.push(
                      "/post/" + item.qa_id + "/null"
                    );
                  }}
                  style={{ display: "inline-block" }}
                >
                  {item.title}
                </div>
              </div>
            );
          } else {
            bookmarkCat2.push(
              <div
                key={i}
                className="bookmark-title-div"
              >
                <div className="bookmark-delete"><Icon type="close-circle" onClick={() => { this.deleteBookmark(item.b_id) }}/></div>
                <div className="bookmark-title" style={{display:"inline-block"}} onClick={() => {
                    this.props.history.push(
                      "/article/" + item.qa_id + "/null"
                    );
                  }}>{item.title}
                </div>
              </div>
            );
          }
        })
        if(bookmarkCat1.length < 1) {
          bookmarkCat1 = <div style={{ textAlign: "center", width: "100%", marginTop: "1em" }}>No Bookmarks</div>
        } else {
          bookmarkCat1 = <div>{bookmarkCat1}</div>
        }

        if (bookmarkCat2.length < 1) {
          bookmarkCat2 = <div style={{ textAlign: "center", width: "100%", marginTop: "1em" }}>No Bookmarks</div>
        } else {
          bookmarkCat2 = <div>{bookmarkCat2}</div>
        }

        bookmarkInfo = (
          <div className={"profile-all" + this.state.dark}>
            <div>
              <Button onClick={() => { this.setState({ bookmarkCat: 1 }) }} className={"bookmark-cat-button" + (this.state.bookmarkCat === 1 ? " active" : "")}>Questions</Button>
              <Button onClick={() => { this.setState({ bookmarkCat: 2 }) }} className={"bookmark-cat-button" + (this.state.bookmarkCat === 1 ? "" : " active")}>Articles</Button>
            </div>
            <div>{(this.state.bookmarkCat === 1)?bookmarkCat1:bookmarkCat2}</div>
          </div>
        );
      }
    } else if(this.state.bookmarkError) {
      bookmarkInfo = <Alert message={this.state.bookmarkError}></Alert>
    } else if(!this.state.gotBookmarks && !this.state.bookmarkError) {
      bookmarkInfo = (
        <div style={{ textAlign: "center", width: "100%", marginTop: "1em" }}>
        <Spin
          indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
          /></div>
      );
    }

    if(this.gotFollowingUsers && this.state.followData && !this.state.followDataError) {
      if(this.state.followData.length > 0) {

      followingInfo1 = map(this.state.followData, (item, i) => {
        return (
        <div key={i} className="bookmark-title-div">
        <div style={{display:"inline-block"}}>{item.dp_url?<Avatar size={50} src={item.dp_url}></Avatar>:<Avatar size={50} icon="user"></Avatar>}</div>
          <div onClick={()=>{if(item.u_id){this.props.history.push("/user/"+item.u_id)}}} style={{textTransform:"capitalize", display:"inline-block", marginLeft:"0.5em", cursor:"pointer"}}>{item.fname+" "+(item.lname||"")}</div>
          <div
          onClick={()=> {if(item.u_id){this.unfollowUser(item.u_id)}}}
                            className={
                              "profile-follow-button" + this.state.dark
                            }
                          >
                            Unfollow
                          </div>
        </div>)
      })
    } else {
    
      followingInfo1 = <div style={{textAlign:"center", width:"100%", marginTop:"1em"}}>
      You aren't following anybody</div>
    }
      followingInfo1 = <div>{followingInfo1}</div>
    } else if(this.state.followDataError) {
      followingInfo1 = <div>{this.state.followDataError}</div>;
    }

    if(this.gotFollowingUsers && this.state.followData1 && !this.state.followData1Error) {
      if(this.state.followData1.length > 0) {

      followingInfo2 = map(this.state.followData1, (item, i) => {
        return (
        <div key={i} className="bookmark-title-div">
        <div style={{display:"inline-block"}}>{item.dp_url?<Avatar size={50} src={item.dp_url}></Avatar>:<Avatar size={50} icon="user"></Avatar>}</div>
          <div onClick={()=>{if(item.u_id){this.props.history.push("/user/"+item.u_id)}}} style={{textTransform:"capitalize", display:"inline-block", marginLeft:"0.5em", cursor:"pointer"}}>{item.fname+" "+(item.lname||"")}</div>
        </div>)
      })
    } else {
    
      followingInfo2 = <div style={{textAlign:"center", width:"100%", marginTop:"1em"}}>
      You don't have any followers</div>
    }
      followingInfo2 = <div>{followingInfo2}</div>
    } else if(this.state.followData1Error) {
      followingInfo2 = <div>{this.state.followData1Error}</div>;
    }

    followingInfo = (
      <div className={"profile-all" + this.state.dark}>
        <div>
          <Button onClick={() => { this.setState({ followCat: 1 }) }} className={"bookmark-cat-button" + (this.state.followCat === 1 ? " active" : "")}>Following<span style={{paddingLeft:"1em"}}>{this.state.followData && this.state.followData.length}</span></Button>
          <Button onClick={() => { this.setState({ followCat: 2 }) }} className={"bookmark-cat-button" + (this.state.followCat === 1 ? "" : " active")}>Followers<span style={{paddingLeft:"1em"}}>{this.state.followData1 && this.state.followData1.length}</span></Button>
        </div>
        <div>{(this.state.followCat === 1)?followingInfo1:followingInfo2}</div>
      </div>
    );

    proInfo = (
      <div className={"profile-all" + this.state.dark}>
        <div>
          placeholder
        </div>
      </div>
        )

    switch (this.state.selector) {
      case 3: {
        content = userInfo;
        userActive = "profile-selector-button-active" + this.state.dark
        bioActive = "profile-selector-button"+this.state.dark
        bookmarkActive = "profile-selector-button" + this.state.dark
        followingActive = "profile-selector-button"+this.state.dark
        break;
      }
      case 1: {
        content = bioInfo;
        bioActive = "profile-selector-button-active" + this.state.dark
        userActive = "profile-selector-button" + this.state.dark
        bookmarkActive = "profile-selector-button" + this.state.dark
        followingActive = "profile-selector-button"+this.state.dark
        break;
      }
      case 2: {
        content = bookmarkInfo;
        bioActive = "profile-selector-button" + this.state.dark
        userActive = "profile-selector-button" + this.state.dark
        bookmarkActive = "profile-selector-button-active" + this.state.dark
        followingActive = "profile-selector-button"+this.state.dark
        break;
      }
      // case 4: {
      //   content = proInfo;
      //   userActive = "profile-selector-button" + this.state.dark
      //   bioActive = "profile-selector-button" + this.state.dark
      //   bookmarkActive = "profile-selector-button" + this.state.dark
      //   followingActive = "profile-selector-button"+this.state.dark
      //   break;
      // }
      case 5: {
        content = followingInfo;
        userActive = "profile-selector-button" + this.state.dark
        bioActive = "profile-selector-button" + this.state.dark
        bookmarkActive = "profile-selector-button" + this.state.dark
        followingActive = "profile-selector-button-active"+this.state.dark
        break;
      }
      default: {
        content = userInfo;
        userActive = "profile-selector-button-active"+this.state.dark;
        bioActive = "profile-selector-button" + this.state.dark
        bookmarkActive = "profile-selector-button" + this.state.dark
        break;
      }
    }
    const uploadButton = (
      <div>
        <Icon type={this.state.imageLoading ? "loading" : "plus"} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    return (
      <div className={"user-profile-main-div"+this.state.dark}>
        <div className={"profile-first-col"+this.state.dark}>
          <Button
            id="profile-profile-button"
            disabled={this.state.error ? true : false}
            onClick={this.handleBioButtonClick}
            className={bioActive}
          >
            Bio
          </Button>
          <Button
            id="profile-account-button"
            disabled={this.state.error ? true : false}
            onClick={this.handleProfileButtonClick}
            className={userActive}
          >
            Account Info
          </Button>
          <Button
            id="profile-bookmarks-button"
            disabled={this.state.error ? true : false}
            onClick={this.handleBookmarkButtonClick}
            className={bookmarkActive}
          >
            Bookmarks
          </Button>
          <Button
            id="profile-account-button"
            disabled={this.state.error ? true : false}
            onClick={this.handleFollowingButtonClick}
            className={followingActive}
          >
            Follow
          </Button>
          <Button
          id="profile-public-button"
            disabled={
              this.state.error || !this.state.gotData ? true : false
            }
            onClick={() => {
              this.props.history.push("/user/" + this.state.uId);
            }}
            className={"profile-selector-button"+this.state.dark}
          >
            <Icon type="eye" />
            Public Profile
          </Button>
          {/* <Button
            id="profile-pro-button"
            disabled={this.state.error ? true : false}
            onClick={this.handleProButtonClick}
            className={userActive}
          >
            PRO
          </Button> */}
        </div>

        <Modal
          className={"profile-profile-edit-modal"+this.state.dark}
          title="Edit profile"
          confirmLoading={this.state.disabled}
          visible={this.state.profileEditModalVisible}
          onOk={this.profileEditOk}
          onCancel={() => {
            this.setState({
              profileEditModalVisible: false,
              editableImageData: null,
              imageLoading: false
            });
          }}
        >
          <div className="profile-profile-edit-modal-div">
            <div>
              <Upload
                openFileDialogOnClick={!this.state.imageLoading}
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                // action={this.uploadImage.bind(this)}
                beforeUpload={this.beforeImageUpload.bind(this)}
                onChange={this.handleImageChange}
              >
                {(this.state.editableDpUrl ||
                  this.state.editableImageData) &&
                !this.state.imageLoading ? (
                  <div>
                    <img
                      className="profile-image-upload-image"
                      src={
                        this.state.editableImageData ||
                        this.state.editableDpUrl
                      }
                      alt="avatar"
                    />
                  </div>
                ) : (
                  uploadButton
                )}
              </Upload>
              <Button
                onClick={() => {
                  this.setState({
                    editableDpUrl: null,
                    editableImageData: null,
                    imageToUpload: null
                  });
                }}
              >
                Remove picture
              </Button>
            </div>
            <p />
            First Name
            <Input
              onPressEnter={this.profileEditOk}
              disabled={this.state.disabled}
              onChange={this.handleFnameChange}
              value={
                this.state.editableFname ? this.state.editableFname : null
              }
            />
            <p />
            Last Name
            <Input
              onPressEnter={this.profileEditOk}
              disabled={this.state.disabled}
              onChange={this.handleLnameChange}
              value={
                this.state.editableLname ? this.state.editableLname : null
              }
            />
            <p />
            Gender
            <br />
            <Select
              defaultValue={
                this.state.editableGender
                  ? this.state.editableGender.charAt(0).toUpperCase() +
                    this.state.editableGender.slice(1)
                  : null
              }
              disabled={this.state.disabled}
              className="profile-gender-select"
              placeholder="Gender"
              onChange={this.handleGenderChange}
            >
              <Select.Option value="Male">Male</Select.Option>
              <Select.Option value="Female">Female</Select.Option>
              <Select.Option value="Other">Other</Select.Option>
              <Select.Option value="">Prefer not to say</Select.Option>
            </Select>
          </div>
          {this.state.infoEditError ? (
            <Alert message={this.state.infoEditError} type="error" />
          ) : null}
        </Modal>
        <Modal
        wrapClassName={"profile-edit-bio-modal"+this.state.dark}
          confirmLoading={this.state.disabled}
          title="Edit bio"
          onOk={this.bioEditOk}
          visible={this.state.bioEditModalVisible}
          onCancel={() => {
            this.setState({
              bioEditModalVisible: false
            });
          }}
        >
          <div>Country</div>
          <Select
            disabled={this.state.disabled}
            showSearch
            style={{ width: "15em" }}
            onChange={this.handleCountryChange}
            value={
              this.state.editableCountry ? this.state.editableCountry : null
            }
          >
            {this.countrySelectVals}
          </Select>
          <p />
          <div>Description</div>
          <Input.TextArea
            disabled={this.state.disabled}
            autosize={{ maxRows: "10" }}
            onChange={this.handleDescriptionChange}
            value={
              this.state.editableDescription
                ? this.state.editableDescription
                : null
            }
          />
          {this.state.bioEditError ? (
            <Alert message={this.state.bioEditError} type="error" />
          ) : null}
        </Modal>
        <Modal
        className={"password-change-modal"+this.state.dark}
          visible={this.state.passwordModalVisible}
          onCancel={() => {
            this.setState({ passwordModalVisible: false });
          }}
          onOk={this.handlePasswordChange}
          title="Change Password"
          confirmLoading={this.state.passwordChangeFieldsDisabled}
        >
          <div className="profile-password-field">
            <Input.Password
              disabled={this.state.passwordChangeFieldsDisabled}
              value={this.state.oldPassword}
              onChange={this.handleOldPasswordChange}
              placeholder="Old password"
            />
          </div>
          <div className="profile-password-field">
            <Input.Password
              disabled={this.state.passwordChangeFieldsDisabled}
              value={this.state.newPassword}
              onChange={this.handleNewPasswordChange}
              placeholder="New password"
            />
          </div>
          <div className="profile-password-field">
            <Input.Password
              disabled={this.state.passwordChangeFieldsDisabled}
              value={this.state.newConfirmPassword}
              onChange={this.handleNewConfirmPasswordChange}
              placeholder="Confirm new password"
            />
          </div>
          <div>
            {this.state.changePasswordError ? (
              <Alert
                type="error"
                message={this.state.changePasswordError}
              />
            ) : null}
          </div>
        </Modal>
        <Header />
        <Row className={"profile-row"+this.state.dark}>
          <div className="profile-content-div">{content}</div>
        </Row>
      </div>
    );
  }
}

export default withRouter(withCookies(Profile));
