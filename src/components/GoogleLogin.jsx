import React, { Component } from "react";
import GoogleLogin1 from "react-google-login";
import "./GoogleLogin.css";
import axios from "axios"
import { withCookies } from "react-cookie";
import {message} from "antd"
import moment from "moment"

class GoogleLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
 
  componentDidMount() {
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
  }

  onFailure = val => {
    message.error("Something went wrong. Please try again later");
  };

  onSuccess = googleUser => {
    message.loading("Please wait", 0)
    this.login(googleUser.tokenId);
  };

  signOut = () => {
    var auth2 = window.gapi.auth2.getAuthInstance();
    auth2.signOut().then(function() {
    });
  };

  login = token => {
    axios({
      url: this.backUrl + "continueWithGoogle",
      method: "POST",
      headers: {
        "Content-type": "text/plain"
      },
      data: {
        "token":token
      }
    })
      .then(res => {
        message.destroy()
        if (res.data) {
          if (res.data.success) {
            if(res.data.data) {
              this.props.cookies.set("session_id", res.data.data.session_id, {
                path: "/"
              });
              this.props.cookies.set("name", res.data.data.name, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
              this.props.cookies.set("pro", res.data.data.pro, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
              this.props.cookies.set("u_id", res.data.data.UId, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
              this.props.cookies.set("role", res.data.data.Role, { path: "/", expires: moment().add(30, 'days').toDate(), secure: true });
              window.location.reload()
            } else {
              message.info(res.data.msg)
            }
          } else {
            if (res.data.msg) {
              message.error(
                res.data.msg
              );
            } else {
              message.error(
                "Something went wrong. Please try again later"
              );
            }
          }
        } else {
          message.error(
            res.data.msg || "Something went wrong. Please try again later"
          );
        }
      })
      .catch(e => {
        message.destroy()
        message.error("Something went wrong. Please try again later");
      });
  };

  render() {
    return (
      <div className="google-button">
        <GoogleLogin1
          clientId={process.env.REACT_APP_API_GOOGLE_CLIENT_ID}
          // render={renderProps => (
          //     <div onClick={renderProps.onClick}>
          //     <img style={{width:"100%"}} src={GoogleNormal}/>
          //   </div>
          // )}
          buttonText="Continue with Google"
          onSuccess={this.onSuccess}
          onFailure={this.onFailure}
          cookiePolicy={"single_host_origin"}
        />
      </div>
    );
  }
}

export default withCookies(GoogleLogin);
