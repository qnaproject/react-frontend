import React, { Component } from "react";
import ReactQuill, { Quill } from "react-quill";

import "react-quill/dist/quill.snow.css";
import "./Editor.css";

import { Button, Checkbox, Alert, Input, message, Modal } from "antd";
import imageUpload from "quill-plugin-image-upload";
import { withCookies } from "react-cookie";
import axios from "axios";
import sanitizeHtml from "sanitize-html";
import { withRouter } from "react-router-dom";
import Resizer from "react-image-file-resizer";
import PopupLogin from "./PopupLogin"
import "./SyntaxHighlightOverride.css"
import { Prompt } from 'react-router'

let globalSessionId;

Quill.register("modules/imageUpload", imageUpload);
class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = { text: "" };
    this.handleChange = this.handleChange.bind(this);
    this.backUrl = ""
    this.trackIds = []
  }

  modules = {
    syntax: true,
    toolbar: [
      ["bold", "italic", "underline", "strike", "code-block"],
      [{ list: "ordered" }, { list: "bullet" }],
      ["link", "image"],
      ["clean"]
    ],
    imageUpload: {
      upload: file => {
        var that = this
        return new Promise((resolve, reject) => {
          const isJPG = file.type === "image/jpeg";
          if (!isJPG) {
            message.error("You can only upload JPG images");
            reject();
            return
          }
          const isLt2M = file.size / 1024 / 1024 < 10;
          if (!isLt2M) {
            message.error("Image must smaller than 10MB");
            reject();
            return
          }

          Resizer.imageFileResizer(
            file,
            500,
            500,
            "JPEG",
            100,
            0,
            uri => {
              var reader = new FileReader();
              reader.onload = function (e) {
                var img = uri.match(/,(.*)$/)[1];
                axios({
                  url: that.backUrl + "uploadImage",
                  method: "POST",
                  headers: {
                    "Content-type": "multipart/form-data",
                    session_id: globalSessionId
                  },
                  data: img
                }).then(res => {
                  if (res && res.data && res.data.success) {
                    if(res.data.data.track_id) {
                      that.trackIds.push(res.data.data.track_id)
                    }
                    resolve(res.data.data.url);
                  } else {
                    reject();
                  }
                });
              };
              reader.readAsDataURL(file);
              return false;
            },
            "base64"
          );
        });
      }
    }
  };

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
  }

  componentDidUpdate() {
    if(this.state.text && this.state.text.length > 0) {
      window.onbeforeunload = function() {
        return "";
      }
    }
  }

  componentWillUnmount() {
    window.onbeforeunload = null
  }

  handleChange(value) {
    this.setState({text: value, error: null })
  }


  setEditor = () => {
    var sessionId = this.props.cookies.get("session_id");
    if (!sessionId) {
      this.setState({
        loginModalVisible: true
      })
      this.props.blur(true)
      return;
    }
    globalSessionId = sessionId;
    this.setState({
      displayEditor: true
    });
  };

  handleQualificationChange = e => {
    this.setState({
      ...this.state,
      qualificationText: e.target.value,
      error: null
    });
  };

  postAnswer = () => {
   if (!this.state.text) {
      this.setState({
        error: "You cannot post an empty answer."
      });
      return;
    } else {
      var sessionId = this.props.cookies.get("session_id");
      if (!sessionId) {
        message.warn("You need to login to continue.")
        return;
      } else {
        this.setState({
          error: null,
          loading: true
        });
        this.done = true
        axios({
          url: this.backUrl + "addAnswer",
          method: "POST",
          headers: {
            "Content-type": "text/plain",
            session_id: sessionId
          },
          data: {
            q_id: this.props.qId,
            qualification: this.state.qualificationText
              ? this.state.qualificationText
              : null,
              track_ids: this.trackIds,
            answer: sanitizeHtml(this.state.text, {
              allowedTags: [
                "p",
                "strong",
                "em",
                "s",
                "blockquote",
                "ol",
                "ul",
                "li",
                "a",
                "img",
                "pre",
                "span",
                "u"
              ],
              allowedAttributes: {
                a: ["href", "rel", "target"],
                img: ["src", "rel"],
                pre: ["class", "spellcheck"],
                span: ["class"]
              }
            })
          }
        })
          .then(res => {
            if (res && res.data && res.data.success) {
              this.done = true
              this.setState({
                displayEditor: false,
                text: null,
                qualificationText: null,
                boxChecked: false,
                loading: false
              });
              this.props.updatePostAndGotoNewPost();
              return;
            } else {
              this.done = false
              if (res && res.data && res.data.msg) {
                if (res.data.msg === "You need to login to continue.") {
                  let cookiesObj = this.props.cookies.getAll();
                  for (var key in cookiesObj) {
                    this.props.cookies.remove(key, { path: "/" });
                  }
                }
                this.setState({
                  error: res.data.msg,
                  loading: false
                });
              } else {
                this.setState({
                  error:
                    "Something went wrong while posting this answer. Please try again later.",
                  loading: false
                });
              }
            }
          })
          .catch(e => {
            this.done = false
            this.setState({
              error:
                "Something went wrong while posting this answer. Please try again later.",
              loading: false
            });
          });
      }
    }
  };

  render() {
    var error;
    if (this.state.error) {
      error = (
        <Alert
          type="error"
          message={this.state.error}
          style={{ marginTop: "1em" }}
        >
          {this.state.error}
        </Alert>
      );
    }
    var content;
    if (this.state.displayEditor) {
      content = (
        <div className={"editor-div"+this.state.dark}>
          <ReactQuill
            value={this.state.text}
            modules={this.modules}
            onChange={this.handleChange}
            placeholder="Type your answer here."
          />
          <div className="qualification">
            <span style={{ marginLeft: "1em", color:this.state.dark?"white":"black" }}>
              What makes you qualified to answer this question? (optional)
            </span>
            <Input
              className="qualification-input"
              onChange={this.handleQualificationChange}
              value={this.state.qualificationText}
              placeholder="e.g. I have a degree in XYZ ,  I have worked in this field for X years."
            />
          </div>
          <div className="editor-post-confirm-div">
            <div style={{ marginTop: "1em" }}>
              <Button
                type="primary"
                onClick={this.postAnswer}
                loading={this.state.loading}
              >
                Post answer
              </Button>
            </div>
            <br />
            {error}
          </div>
        </div>
      );
    } else {
      content = (
        <Button
        disabled={(this.props.uId+"" === this.props.cookies.get("u_id")) || this.props.closed}
          style={{ backgroundColor: this.props.colorScheme }}
          className={(this.state.dark===" dark")?"answer-button-dark":"answer-button"}
          value={this.props.closed?"Closed for answers":"Answer this question"}
          onClick={this.setEditor}
        >
          {this.props.closed ? "Closed for answers" : "Answer this question"}
        </Button>
      );
    }
    return <React.Fragment>
    {content} 
    <Modal
      visible={this.state.loginModalVisible}
      footer={null}
      onCancel={() => {this.setState({ loginModalVisible: false }); this.props.blur(false)}}
    >
      <PopupLogin login />
    </Modal>
    <Prompt
      when={(this.state.text && this.state.text.length > 0 && !this.done)}
      message="Navigate away? Changes you made may not be saved."
    />
    </React.Fragment>;
  }
}

export default withRouter(withCookies(Editor));
