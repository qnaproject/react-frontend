import React, { Component } from "react";
import ReactQuill, { Quill } from "react-quill";

import "react-quill/dist/quill.snow.css";
import "./ArticleEditor.css";

import { Button, Checkbox, Alert, Input, message, InputNumber, Select, Modal } from "antd";
import imageUpload from "quill-plugin-image-upload";
import { withCookies } from "react-cookie";
import axios from "axios";
import sanitizeHtml from "sanitize-html";
import { withRouter } from "react-router-dom";
import Resizer from "react-image-file-resizer";
import { Prompt } from 'react-router'
import { find, map } from "lodash"

class ArticleEditor extends Component {
  constructor(props) {
    super(props);
    this.state = { text: "" };
    this.handleChange = this.handleChange.bind(this);
    this.trackIds = []
    this.prevUrls = []
  }
  modules = {
    syntax: true,
    toolbar: [
      ["bold", "italic", "underline", "strike", "code-block"],
      [{ list: "ordered" }, { list: "bullet" }],
      ["link", "image"],
      ["clean"]
    ],
    imageUpload: {
      upload: file => {
        var that = this;
        return new Promise((resolve, reject) => {
          const isJPG = file.type === "image/jpeg";
          if (!isJPG) {
            message.error("You can only upload JPG images");
            reject();
            return;
          }
          const isLt2M = file.size / 1024 / 1024 < 10;
          if (!isLt2M) {
            message.error("Image must smaller than 10MB");
            reject();
            return;
          }

          Resizer.imageFileResizer(
            file,
            500,
            500,
            "JPEG",
            100,
            0,
            uri => {
              var reader = new FileReader();
              reader.onload = function(e) {
                var img = uri.match(/,(.*)$/)[1];
                axios({
                  url: that.backUrl + "uploadArticleImage",
                  method: "POST",
                  headers: {
                    "Content-type": "multipart/form-data",
                    session_id: that.props.cookies.get("session_id")
                  },
                  data: img
                }).then(res => {
                  if (res && res.data && res.data.success) {
                    if (res.data.data.track_id) {
                      that.trackIds.push(res.data.data.track_id)
                      that.prevUrls.push({id:res.data.data.track_id, url:res.data.data.url})
                      if(that.trackIds && that.trackIds.length === 1) {
                        that.setState({prevUrl:res.data.data.url, prevId: res.data.data.track_id})
                      }
                    }
                    resolve(res.data.data.url);
                  } else {
                    reject();
                  }
                });
              };
              reader.readAsDataURL(file);
              return false;
            },
            "base64"
          );
        });
      }
    }
  };

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
  }

  componentDidUpdate() {
    if(this.state.text && this.state.text.length > 0) {
      window.onbeforeunload = function() {
        return "";
      }
    }
  }

  componentWillUnmount() {
    window.onbeforeunload = null
  }

  handleChange(value) {
    this.setState({
      text: value,
      error: null
    });
  }

  handleTitleChange = value => {
    this.setState({
      title: value.target.value,
      error: null
    });
  };

  handleCategorySelectChange = (value) => {
    this.setState({
      category: value,
      error: null
    })
  }

  handlePostArticle = () => {
    if (!this.state.text) {
      this.setState({
        error: "You cannot post an empty article."
      });
      return;
    }

    if (!this.state.title) {
      this.setState({
        error: "Please provide a title."
      });
      return;
    }

    if (this.state.text.length < 20) {
      this.setState({
        error: "Your article is too short."
      });
      return;
    }

    if (this.state.text.length > 5000) {
      this.setState({
        error: "Your article is too long."
      });
      return;
    }

    if (this.state.title.length < 10) {
      this.setState({
        error: "Your article title is too short."
      });
      return;
    }

    if (this.state.title.length > 100) {
      this.setState({
        error: "Your article title is too long."
      });
      return;
    }

    if (!this.state.category) {
      this.setState({
        error: "Please choose a category."
      });
      return;
    }
    this.setState({
      loading: true,
      error: null
    })
    this.done = true
    axios({
        url: this.backUrl+"addArticle",
        method: "POST",
        headers: {
            "Content-type":"text/plain",
            "session_id":this.props.cookies.get("session_id")
        },
        data: {
            title: this.state.title,
            track_ids: this.trackIds,
            preview_id: this.state.prevId,
          content: sanitizeHtml(this.state.text, {
            allowedTags: [
              "p",
              "strong",
              "em",
              "s",
              "blockquote",
              "ol",
              "ul",
              "li",
              "a",
              "img",
              "pre",
              "span",
              "u"
            ],
            allowedAttributes: {
              a: ["href", "rel", "target"],
              img: ["src", "rel"],
              pre: ["class", "spellcheck"],
              span: ["class"]
            }
          }),
            category: this.state.category
        }
    })
    .then(res => {
      if(res && res.data && res.data.success) {
        message.success("Successfully posted article.")
        this.setState({text:null})
        setTimeout(() => {
          if(res.data.data && res.data.data > 0) {
            this.props.history.push("/article/"+res.data.data+"/null")
          }
        }, 500)
      } else {
        this.done = false
        if(res && res.data && res.data.msg) {
          this.setState({
            error: res.data.msg,
            loading: false
          })
        } else {
          this.setState({
            error: "Something went wrong while posting this article. Please try again later.",
            loading: false
          })
        }
      }
    })
    .catch(e => {
      this.done = false
      this.setState({
        error: "Something went wrong while posting this article. Please try again later.",
        loading: false
      })
    })
  };

  render() {
    return (
      <div className="editor-div profile">
        <ReactQuill
          value={this.state.text}
          modules={this.modules}
          onChange={this.handleChange}
          placeholder="Type your article here."
        />
        {this.state.text && this.state.text.length > 1 ? (
          <div>
            <Input
              onChange={this.handleTitleChange}
              style={{ borderRadius: "0", marginTop: "1em" }}
              placeholder="Title for the article"
            />
            <div className="article-editor-preview-image-div">
              <div style={{marginBottom:"0.5em"}}><span style={{color:(this.state.dark === " dark")?"white":"#000000a6"}}>Preview Image</span><Button onClick={() => {this.setState({previewChooserModalVisible: true})}} style={{marginLeft:"0.5em"}}>
                Change</Button>
              </div>
              <div className="article-editor-preview-image-inner-div" style={{backgroundColor:this.state.prevUrl?"initial":"white"}}>
                <div className="article-editor-preview-image-img-div">
                  {this.state.prevUrl?<img className="article-editor-preview-image-img" src={this.state.prevUrl}></img>:"No image selected"}
                </div>
              </div>
            </div>
            <div style={{textAlign:"center", marginTop:"1em"}}>
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Category"
                optionFilterProp="children"
                onChange={this.handleCategorySelectChange}
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                <Select.Option value="Art">Art</Select.Option>
                <Select.Option value="Dance">Dance</Select.Option>
                <Select.Option value="Economy">Economy</Select.Option>
                <Select.Option value="Electronics">
                  Electronics
                </Select.Option>
                <Select.Option value="Fiction">Fiction</Select.Option>
                <Select.Option value="Filmfare">Filmfare</Select.Option>
                <Select.Option value="Food">Food</Select.Option>
                <Select.Option value="Gadgets">Gadgets</Select.Option>
                <Select.Option value="Health">Health</Select.Option>
                <Select.Option value="Humanity">Humanity</Select.Option>
                <Select.Option value="Love">Love</Select.Option>
                <Select.Option value="Music">Music</Select.Option>
                <Select.Option value="Nature">Nature</Select.Option>
                <Select.Option value="Software">Software</Select.Option>
                <Select.Option value="Sports">Sports</Select.Option>
                <Select.Option value="Students">Students</Select.Option>
                <Select.Option value="Travel">Travel</Select.Option>
                {/* <Select.Option value="Anon">Anon</Select.Option> */}
              </Select>
            </div>
            {this.state.error ? (
              <Alert
                style={{ marginTop: "1em", marginBottom:"1em" }}
                message={this.state.error}
                type="error"
              />
            ) : null}
            <div className="post-article-button-div">
              <Button
                onClick={() => {
                  this.handlePostArticle();
                }}
                className="post-article-button"
                type="primary"
              >
                Post Article
              </Button>
            </div>
          </div>
        ) : null}
        <Modal
        title="Preview image"
          visible={this.state.previewChooserModalVisible}
         footer={null}
          onCancel={()=>{this.setState({previewChooserModalVisible:false})}}
        >
        {(this.prevUrls && this.prevUrls.length > 0)?<div onClick={() => {this.setState({prevUrl: null, prevId: null, previewChooserModalVisible: false})}} className="article-editor-preview-image-list-noimg">No preview image</div>:null}
        {(this.prevUrls && this.prevUrls.length > 0)? 
        map(this.prevUrls,(val, key) => {
         return <img key={key} onClick={() => {this.setState({prevUrl: val.url, prevId: val.id, previewChooserModalVisible: false})}} className="article-editor-preview-image-list-img" src={val.url}></img>
          }
        )
        :
        "No image added to article."}
        </Modal>
        <Prompt
      when={(this.state.text && (this.state.text.length > 0) && !this.done)}
      message="Navigate away? Changes you made may not be saved."
    />
      </div>
    );
  }
}

export default withRouter(withCookies(ArticleEditor));
