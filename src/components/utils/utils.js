export var categoryColorMap = new Map([
    ["Art", "#673AB7"],
    ["Economy", "#8BC34A"],
    ["Education", "#CDDC39"],
    ["Entertainment", "#9C27B0"],
    ["Fiction", "#009688"],
    ["Food", "#FF9800"],
    ["Health", "#F44336"],
    ["Humanity", "#607D8B"],
    ["Nature", "#4CAF50"],
    ["News", "#00BCD4"],
    ["Pets", "#FFEB3B"],
    ["Relationships", "#E91E63"],
    ["Science", "#FF5722"],
    ["Software", "#03A9F4"],
    ["Sports", "#FFC107"],
    ["Technology", "#3F51B5"],
    ["Travel", "#2196F3"]
  ]);

  export var categoryList = {
    1: "Art",
    2: "Economy",
    3: "Education",
    4: "Entertainment",
    5: "Fiction",
    6: "Food",
    7: "Health",
    8: "Humanity",
    9: "Nature",
    10: "News",
    11: "Pets",
    12: "Relationships",
    13: "Science",
    14: "Software",
    15: "Sports",
    16: "Technology",
    17: "Travel"
  };