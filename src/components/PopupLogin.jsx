import React, { Component } from "react";
import "./PopupLogin.css";
import { Modal, Button } from "antd";
import GoogleLogin from "./GoogleLogin";
import FacebookLogin from "./FacebookLogin";
import {withRouter} from "react-router-dom"

class PopupLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        <div style={{ marginTop: "2em" }}>
          <div className="popup-login-button">
            <GoogleLogin />
          </div>
          <div className="popup-login-button">
            <FacebookLogin />
          </div>
          <div
            style={{
              width: "100%",
              textAlign: "center",
              fontSize: "1.3em",
              margin: "0.5em"
            }}
          >
            Or
          </div>
          <div style={{}} className="popup-login-button">
            <Button
              onClick={() => {
                this.props.login
                  ? this.props.history.push("/login")
                  : this.props.history.push("/signup");
              }}
              style={{
                width: "100%",
                height: "46px",
                fontSize: "1.2em"
              }}
            >
              {this.props.login ? "Login with email" : "Sign Up with email"}
            </Button>
          </div>
        </div>
        <div style={{marginTop:"2em", textAlign:"center"}}>
        By continuing with any of the above options, you agree to the{" "}<a href="/terms">Terms and Conditions</a>{" and "} 
            <a href="/privacy">Privacy Policy</a>
            </div>
      </div>
    );
  }
}

export default withRouter(PopupLogin);
