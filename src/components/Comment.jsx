import React, { Component } from "react";
import { map, cloneDeep, reject, findIndex } from "lodash";
import "./Comment.css";
import { withRouter } from "react-router-dom";
import tinycolor from "tinycolor2";
import ReactDOM from "react-dom";
import {
  Input,
  Alert,
  message,
  Popover,
  Button,
  Row,
  Col,
  Spin,
  Icon,
  Avatar,
  Rate,
  Modal,
  Select
} from "antd";
import axios from "axios";
import { withCookies } from "react-cookie";
import moment from "moment";
import PopupLogin from "./PopupLogin";

class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = { comments: [], commentLimit: 5 };
    this.commentRefs = [];
    this.backUrl = "";
    this.userInfo = {};
    this.commentUidRefs = []
  }

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    this.setState({
      comments: this.props.val
    });
  }

  componentDidUpdate() {
    if (this.state.newCommentAdded) {
      this.gotDps = false
      if (
        this.commentRefs &&
        this.commentRefs.length > 0 &&
        this.commentRefs[this.commentRefs.length - 1]
      ) {
        this.commentRefs[this.commentRefs.length - 1].scrollIntoView({
          block: "end",
          behavior: "smooth"
        });
        if (this.commentRefs[this.commentRefs.length - 1].style) {
          this.commentRefs[this.commentRefs.length - 1].style.backgroundColor =
            this.state.dark === " dark" ? "#1890FF" : "#1890FF";
        }
        setTimeout(() => {
          if (
            this.commentRefs &&
            this.commentRefs.length > 0 &&
            this.commentRefs[this.commentRefs.length - 1]
          ) {
            if (this.commentRefs[this.commentRefs.length - 1].style) {
              this.commentRefs[
                this.commentRefs.length - 1
              ].style.backgroundColor = "";
            }
          }
        }, 1500);
      }
      this.setState({newCommentAdded: false });
      this.getAndSetDpUrlsAndPoints(parseInt(this.props.cookies.get("u_id")))
    }
    if(this.state.viewComments && !this.gotDps) {
      this.gotDps = true
      var userDpData = this.props.getCommentUserDp()
      if (userDpData && userDpData.length > 0) {
        map(this.commentUidRefs, (item, i) => {
          let resI = findIndex(userDpData, function (o) {
            return o.u_id === item[0];
          });
          if (resI > -1) {
            if (
              this.commentUidRefs[i][1] &&
              userDpData[resI].dp_url
            ) {
              ReactDOM.findDOMNode(
                this.commentUidRefs[i][1]
              ).innerHTML =
                "<img src=" +
                userDpData[resI].dp_url +
                " class='mark'>";
            }
          }
        });
      }
    }
  }

  getAndSetDpUrlsAndPoints = (uid) => {
    axios({
      url: this.backUrl + "getUserDpUrlAndPoints",
      headers: {
        "Content-type": "text/plain"
      },
      method: "POST",
      data: {
        u_ids: [uid]
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          var that = this;
          if (res.data.data.length > 0) {
            var userDpData =  res.data.data
             if (userDpData && userDpData.length > 0) {
              map(this.commentUidRefs, (item, i) => {
                let resI = findIndex(userDpData, function (o) {
                  return o.u_id === item[0];
                });
                if (resI > -1) {
                  if (
                    this.commentUidRefs[i][1] &&
                    userDpData[resI].dp_url
                  ) {
                    ReactDOM.findDOMNode(
                      this.commentUidRefs[i][1]
                    ).innerHTML =
                      "<img src=" +
                      userDpData[resI].dp_url +
                      " class='mark'>";
                  }
                }
              });
            }
          }
        } else {
          if (res && res.data && res.data.msg) {
          } else {
          }
        }
      })
      .catch(e => {});
  };

  viewComments = () => {
    if (!this.state.viewComments) {
      this.setState({
        viewComments: true
      });
    } else {
      this.setState({
        viewComments: false,
        commentLimit: 5
      });
    }
  };

  handleCommentChange = e => {
    if (!this.state.commentText) {
      if (!this.props.cookies.get("session_id")) {
        this.setState({
          loginModalVisible: true
        });
        this.props.blur && this.props.blur(true)
        return;
      }
    }
    this.setState({
      commentText: e.target.value,
      commentError: null
    });
  };

  postComment = () => {
    let sessionId = this.props.cookies.get("session_id");
    if (!sessionId) {
      this.setState({
        loginModalVisible: true
      });
      return;
    }
    if (!this.state.commentText) {
      this.setState({
        commentError: "You cannot post an empty comment."
      });
      return;
    }
    this.setState({
      commentDisabled: true
    });
    axios({
      url:
        this.backUrl +
        (this.props.article ? "addArticleComment" : "addComment"),
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: sessionId
      },
      data: {
        a_id: this.props.aId,
        comment: this.state.commentText
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          var cookieKey = window.location.pathname.split("/")[1]+":"+window.location.pathname.split("/")[2];
          this.props.cookies.set(cookieKey, moment.now().toString(), { path:"/", expires: moment().add(610, 'seconds').toDate()})
          let newComment = {};
          newComment.a_id = this.props.aId;
          newComment.comment = this.state.commentText;
          newComment.name = "You";
          newComment.created = moment();
          newComment.u_id = parseInt(this.props.cookies.get("u_id"));
          newComment.c_id = res.data.data;
          let comments = cloneDeep(this.state.comments);
          if (!comments) {
            comments = [];
          }
          comments.push(newComment);
          this.setState({
            viewComments: true,
            commentLimit: (this.state.comments && this.state.comments.length+1) || 1,
            commentDisabled: false,
            comments: comments,
            commentText: null,
            newCommentAdded: true
          });
          // message.success("Comment posted successfully.");
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              commentError: res.data.msg,
              commentDisabled: false
            });
          } else {
            this.setState({
              commentError:
                "Something went wrong while posting your comment. Please try again later.",
              commentDisabled: false
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          commentDisabled: false,
          commentError:
            "Something went wrong while posting your comment. Please try again later."
        });
      });
  };

  reportComment(c_id) {
    if (this.props.cookies.get("session_id")) {
      this.setState({
        viewReportModal: true,
        cId: c_id
      });
    } else {
      message.warn("You need to login to continue");
    }
  }

  handleReportCancel() {
    this.setState({
      viewReportModal: false,
      reportCategory: null,
      reportLoading: false
    });
  }

  handleReportCategoryChange(val) {
    this.setState({
      reportCategory: val
    });
  }

  handleReportOk() {
    this.setState({
      reportLoading: true
    });
    axios({
      url: this.backUrl + "reportPost",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id")
      },
      data: {
        qac_id: this.state.cId,
        r_type: this.props.article ? 5 : 3,
        report_category: this.state.reportCategory
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            viewReportModal: false,
            reportCategory: null,
            reportLoading: false
          });
          message.success(res.data.msg);
        } else {
          if (res && res.data && res.data.msg) {
            this.setState({
              reportLoading: false,
              reportError: res.data.msg
            });
          } else {
            this.setState({
              reportLoading: false,
              reportError:
                "Something went wrong while reporting this question. Please try again later."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          reportLoading: false,
          reportError:
            "Something went wrong while reporting this question. Please try again later."
        });
      });
  }

  deleteComment = cId => {
    var that = this;
    Modal.confirm({
      title:
        "Are you sure you want to delete this comment? This action cannot be undone.",
      onOk() {
        axios({
          url:
            that.backUrl +
            (that.props.article ? "deleteArticleComment" : "deleteComment"),
          method: "POST",
          headers: {
            "Content-type": "text/plain",
            session_id: that.props.cookies.get("session_id")
          },
          data: {
            c_id: cId
          }
        })
          .then(res => {
            if (res && res.data && res.data.success) {
              message.success(res.data.msg);
              let comments = cloneDeep(that.state.comments);
              comments = reject(comments, function(item) {
                return item.c_id === cId;
              });
              that.commentUidRefs= []
              that.setState({
                comments: comments
              });
            } else {
              if (res && res.data && res.data.msg) {
                message.error(res.data.msg);
              } else {
                message.error(
                  "Something went wrong while deleting this comment. Please try again later."
                );
              }
            }
          })
          .catch(e => {
            message.error(
              "Something went wrong while deleting this comment. Please try again later."
            );
          });
      }
    });
  };

  handleDestoyGlobalPopoverVisibility = v => {
    if (v) {
      this.setState({
        destroyPopover: false
      });
    }
  };

  getUserDetail = uid => {
    this.setState({
      userDetail: (
        <center>
          <Spin
            indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
          />
        </center>
      )
    });
    if (uid && uid > 0) {
      axios({
        url: this.backUrl + "getPublicUserDetails",
        method: "POST",
        headers: {
          "Content-type": "text/plain"
        },
        data: {
          u_id: uid
        }
      })
        .then(res => {
          if (res && res.data && res.data.success) {
            let t_points = res.data.data.points,
              points;
            if (t_points > 999999) {
              let point_string = ((1.0 * t_points) / 1000000.0).toFixed(3);
              let seperateVals = point_string.split(".");
              if (seperateVals[1]) {
                points =
                  seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "M";
              } else {
                points = seperateVals[0] + "M";
              }
            } else if (t_points > 999) {
              let point_string = ((1.0 * t_points) / 1000.0).toFixed(3);
              let seperateVals = point_string.split(".");
              if (seperateVals[1]) {
                points =
                  seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "K";
              } else {
                points = seperateVals[0] + "K";
              }
            } else {
              points = t_points;
            }
            let name = res.data.data.fname;
            if (res.data.data.lname) {
              name += " " + res.data.data.lname;
            }
            let tier;
            if (t_points > 999999) {
              tier = 5;
            } else if (t_points > 99999) {
              tier = 4;
            } else if (t_points > 9999) {
              tier = 3;
            } else if (t_points > 999) {
              tier = 2;
            } else if (t_points > 99) {
              tier = 1;
            } else {
              tier = 0;
            }

            this.setState({
              userDetail: (
                <div>
                  <h4 className="other-question-heading">
                    <span className="article-user-name">{name}</span>
                  </h4>
                  <div className="article-other-user-data-div">
                    <div className="article-dp">
                      {res.data.data.dp_url ? (
                        <Avatar size={64} src={res.data.data.dp_url} />
                      ) : (
                        <Avatar size={64} icon="user" />
                      )}
                    </div>
                    <div className="article-user-points">{points}</div>
                    <Rate count={tier} disabled defaultValue={tier} />
                  </div>
                </div>
              )
            });
          } else {
            if (res && res.data && res.data.msg) {
              this.setState({
                userDetail: <Alert type="error" message={res.data.msg} />
              });
            } else {
              this.setState({
                userDetail: (
                  <Alert
                    type="error"
                    message="Something went wrong.Please try again later."
                  />
                )
              });
            }
          }
        })
        .catch(e => {
          this.setState({
            userDetail: (
              <Alert
                type="error"
                message="Something went wrong.Please try again later."
              />
            )
          });
        });
    }
  };

  render() {
    var that = this;
    var com;
    var commentError;
    if (this.state.commentError) {
      commentError = (
        <div style={{ marginTop: "1em" }}>
          <Alert type="error" message={this.state.commentError} />
        </div>
      );
    }
    if (
      this.state.viewComments &&
      this.state.comments &&
      this.state.comments.length > 0
    ) {
      com = map(this.state.comments, function(item, i) {
        if(i === that.state.commentLimit){
          return <div key={i} className={"view-more-div" + that.state.dark}><div className="view-10-more" onClick={() => { that.setState({ commentLimit: that.state.commentLimit + 10 }) }}>View 10 more</div><div onClick={() => { that.setState({ commentLimit: that.state.comments.length }) }} className="view-all-comments">View All</div></div>
        }
        if (i >= that.state.commentLimit+1) {
          return false
        }

        var createdUTC = moment(item.created).utc();
        var nowUtc = moment(moment()).utc();
        var duration = moment.duration(-nowUtc.diff(createdUTC));
        return (
          <div className="comment-outer" key={i}>
            <div
              className={"individual-comment" + that.state.dark}
              ref={ref => (that.commentRefs[i] = ref)}
            >
              <div
                className={
                  that.props.uId && that.props.uId === item.u_id
                    ? "comment-op" + that.state.dark
                    : null
                }
              >
                <Avatar
                  ref={ref => {
                    that.commentUidRefs.push([item.u_id, ref]);
                  }}
                  onClick={() => {
                    if (item.u_id) {
                      that.props.history.push("/user/" + item.u_id);
                    }
                  }}
                  className="answer-avatar"
                  style={{
                    cursor: "pointer",
                    marginRight: "0.3em",
                    marginTop:"0.2em"
                  }}
                  icon="user"
                />
                <div className={"comment-created" + that.state.dark}>
                  {duration.humanize(true)}
                </div>
                <div className="comment-inner-content">
                  <div
                    onClick={() => {
                      if (item.u_id) {
                        that.props.history.push("/user/" + item.u_id);
                      }
                    }}
                    className={"comment-author" + that.state.dark}
                  >
                    {item.name ? (
                      <Popover
                        overlayClassName={
                          "popover-user-data-modal" + that.state.dark
                        }
                        onVisibleChange={e => {
                          if (e) {
                            that.getUserDetail(item.u_id);
                          }
                        }}
                        content={that.state.userDetail}
                      >
                        {" "}
                        <div>{item.name}</div>{" "}
                      </Popover>
                    ) : (
                      "Deleted User"
                    )}
                  </div>
                  <div className={"comment-comment" + that.state.dark}>
                    {item.comment}
                  </div>
                </div>
              </div>
              <div
                className="comments-options-div"
                style={{ display: "none !important" }}
              >
                <Popover
                  // mouseLeaveDelay={1}
                  placement="bottom"
                  onVisibleChange={
                    that.handleDestoyGlobalPopoverVisibility
                  }
                  trigger="click"
                  content={
                    <div>
                      {item.u_id + "" ===
                        that.props.cookies.get("u_id") ? null : (
                          <Button
                            style={{ border: "none", width: "7em" }}
                            onClick={() => {
                              that.reportComment(item.c_id);
                            }}
                          >
                            Report
                            </Button>
                        )}
                      {item.u_id + "" ===
                        that.props.cookies.get("u_id") ||
                        that.props.cookies.get("role") > 0 ? (
                          <div>
                            <Button
                              style={{ border: "none", width: "7em" }}
                              type="danger"
                              onClick={() => {
                                that.deleteComment(item.c_id);
                              }}
                            >
                              Delete
                              </Button>
                          </div>
                        ) : null}
                    </div>
                  }
                >
                  <div
                    className={
                      "comments-options-trigger"+(that.props.article?" article":"") + that.state.dark
                    }
                  >
                    ...
                      </div>
                </Popover>
              </div>
            </div>
          </div>
        );
      });
    }
    return (
      <Row type="flex">
        <Modal //report modal
        wrapClassName={"report-comment-modal"+this.state.dark}
          style={{ zIndex: "9999" }}
          title="Report Comment"
          onOk={this.handleReportOk.bind(this)}
          visible={this.state.viewReportModal}
          onCancel={this.handleReportCancel.bind(this)}
          okButtonProps={{
            disabled: this.state.reportCategory ? false : true,
            loading: this.state.reportLoading
            // onClick: () => {
            //   this.handleReportOk();
            // }
          }}
        >
          <div className="report-div">
            <div>What are you reporting?</div>
            <Select
              defaultValue={this.state.reportCategory}
              value={this.state.reportCategory}
              className="report-selector"
              onChange={this.handleReportCategoryChange.bind(this)}
            >
              <Select.Option value="Hate Speech">Hate Speech</Select.Option>
              <Select.Option value="Political Propaganda">
                Political Propaganda
              </Select.Option>
              <Select.Option value="Harmful Intent">
                Harmful Intent
              </Select.Option>
              <Select.Option value="Sexually Explicit Content">
                Sexually Explicit Content
              </Select.Option>
              <Select.Option value="Advertising/Spam">
                Advertising/Spam
              </Select.Option>
              <Select.Option value="Misleading">Misleading</Select.Option>
              <Select.Option value="Content unrelated to question">
                Content unrelated to question
              </Select.Option>
              <Select.Option value="Wrong Category">
                Wrong Category
              </Select.Option>
            </Select>
          </div>
          {this.state.reportError ? (
            <Alert
              style={{ marginTop: "1em" }}
              type="error"
              message={this.state.reportError}
            />
          ) : null}
        </Modal>
        <Col span={24}>
          <div className={"comment-div" + this.state.dark}>
            <div className={"add-comment-div" + this.state.dark + ((this.state.commentText && this.state.commentText.length > 0)?" active":"")}>
              <Input
                disabled={this.state.commentDisabled}
                placeholder="Add a comment"
                onChange={this.handleCommentChange}
                onPressEnter={this.postComment}
                value={this.state.commentText}
              />
              {commentError}
            </div>
            <div style={{
                backgroundColor:
                   tinycolor(this.props.colorScheme)
                        .darken(0)
                        .toHexString()
                    
              }} 
              className={"comment-post-button"+ ((this.state.commentText && this.state.commentText.length > 0)?" active":"")}
              onClick={this.postComment}
              >           
              Post
              </div>
            <div
              className="view-comments-div"
              style={{
                backgroundColor:
                  this.state.dark === " dark"
                    ? tinycolor(this.props.colorScheme)
                        .darken(0)
                        .toHexString()
                    : this.props.colorScheme
              }}
              onClick={this.viewComments}
            >
              {this.state.comments ? this.state.comments.length + " " : "0 "}
              {this.state.comments && this.state.comments.length === 1
                ? "comment"
                : "comments"}
            </div>
            <div className="comment-inner-div">{com}</div>
          </div>
        </Col>
        <Modal
          visible={this.state.loginModalVisible}
          footer={null}
          onCancel={() => {this.setState({ loginModalVisible: false })
          this.props.blur && this.props.blur(false)}}
        >
          <PopupLogin login />
        </Modal>
      </Row>
    );
  }
}

export default withRouter(withCookies(Comment));
