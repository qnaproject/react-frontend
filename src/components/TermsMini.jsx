import React, { Component } from "react";
import "./TermsMini.css";

class TermsMini extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <div className="terms-mini-main-div">
        <div className="terms-mini-inner-div">
            <a href="/terms">Terms and Conditions</a> <span className="terms-mini-divider-span">|</span>
            <a href="/privacy">Privacy Policy</a> <span className="terms-mini-divider-span">|</span>
            <a href="/contact">Contact</a><br/>
            <p>site design / logo &copy; 2019 clevereply.com</p>
        </div>
    </div>;
  }
}

export default (TermsMini)