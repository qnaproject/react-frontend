import React, { Component } from "react";
import { Modal, Input, Select, Alert } from "antd";
import { withCookies } from "react-cookie";
import axios from "axios";
import { withRouter } from "react-router-dom";

class AskQuestion extends Component {
  state = {};

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
  }

  handleCategorySelectChange = val => {
    this.setState({
      askQuestionCategory: val
    });
  };

  handleAskQuestionTitle = e => {
    this.setState({ askQuestionTitle: e.target.value });
  };

  handleAskQuestionDesc = e => {
    this.setState({ askQuestionDesc: e.target.value });
  };

  handleOk = () => {
    if (!this.state.askQuestionTitle) {
      this.setState({
        askError: "Please provide a question title."
      });
      return;
    }
    if (this.state.askQuestionDesc && this.state.askQuestionDesc.length < 20) {
      this.setState({
        askError: "Please provide a longer description."
      });
      return;
    }
    if (
      this.state.askQuestionDesc &&
      this.state.askQuestionDesc.length > 1500
    ) {
      this.setState({
        askError: "The description is too long."
      });
      return;
    }
    if (!this.state.askQuestionCategory) {
      this.setState({
        askError: "Please provide a question category."
      });
      return;
    }
    if (!this.props.cookies.get("session_id")) {
      this.setState({
        askError: "You need to login to ask a question."
      });
      return;
    }
    this.setState({ confirmLoading: true, askError: null });
    axios({
      url: this.backUrl + "addQuestion",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id")
      },
      data: {
        title: this.state.askQuestionTitle,
        description: this.state.askQuestionDesc,
        category: this.state.askQuestionCategory
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          if (res.data.data && res.data.data) {
            let safeTitle = this.state.askQuestionTitle
              .split(" ")
              .join("-")
              .toLowerCase()
              .split("?")
              .join("-")
              .split("'")
              .join("");
            if (safeTitle[safeTitle.length - 1] === "-") {
              safeTitle = safeTitle.slice(0, safeTitle.length - 1);
            }
            this.props.history.push(
              encodeURI("/post/" + res.data.data + "/" + safeTitle)
            );
          }
        } else {
          if (res && res.data && res.data.msg) {
            if (res.data.msg === "You need to login to continue.") {
              let cookiesObj = this.props.cookies.getAll();
              for (var key in cookiesObj) {
                this.props.cookies.remove(key, { path: "/" });
              }
            }
            this.setState({
              askError: res.data.msg,
              confirmLoading: false
            });
          } else {
            this.setState({
              askError:
                "Something went wrong while posting this question. Please try again later.",
              confirmLoading: false
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          askError:
            "Something went wrong while posting this question. Please try again later.",
          confirmLoading: false
        });
      });
  };


  render() {
    return (
      <React.Fragment>
        <Modal
          wrapClassName={"ask-a-question-modal" + this.state.dark}
          title="Ask a question"
          visible={this.props.askVisible}
          okText="Post"
          onOk={this.handleOk}
          confirmLoading={this.state.confirmLoading}
          onCancel={()=>{
            this.props.handleCancel()
            this.setState({confirmLoading: false, askError: null})
          }}
          maskClosable={false}
        >
          <div className="ask-div">
            <div className="ask-title-div">
              <Input.TextArea
                autoFocus
                autosize
                size="large"
                placeholder="Question"
                onChange={this.handleAskQuestionTitle}
              />
              <p>Start your question with 'what', 'why', 'where' etc.</p>
            </div>
            <div className="ask-desc-div">
              <Input.TextArea
                placeholder="Description"
                onChange={this.handleAskQuestionDesc}
              />
              <p>
                Provide a description to further elaborate on your question or
                to provide additional details.
              </p>
            </div>
            <div className="category-selector-div">
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Category"
                optionFilterProp="children"
                onChange={this.handleCategorySelectChange}
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                <Select.Option value="Art">Art</Select.Option>
                <Select.Option value="Economy">Economy</Select.Option>
                <Select.Option value="Education">Education</Select.Option>
                <Select.Option value="Entertainment">Entertainment</Select.Option>
                <Select.Option value="Fiction">Fiction</Select.Option>
                <Select.Option value="Food">Food</Select.Option>
                <Select.Option value="Health">Health</Select.Option>
                <Select.Option value="Humanity">Humanity</Select.Option>
                <Select.Option value="Nature">Nature</Select.Option>
                <Select.Option value="News">News</Select.Option>
                <Select.Option value="Pets">Pets</Select.Option>
                <Select.Option value="Relationships">Relationships</Select.Option>
                <Select.Option value="Science">Science</Select.Option>
                <Select.Option value="Software">Software</Select.Option>
                <Select.Option value="Sports">Sports</Select.Option>
                <Select.Option value="Technology">Technology</Select.Option>
                <Select.Option value="Travel">Travel</Select.Option>
                {/* <Select.Option value="Anon">Anon</Select.Option> */}
              </Select>
            </div>
            <div style={{ marginTop: "2em" }}>
              {(this.props.askError || this.state.askError) ? (
                <Alert type="error" message={(this.props.askError || this.state.askError)} />
              ) : null}
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default withRouter(withCookies(AskQuestion));
