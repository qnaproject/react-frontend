import React, { Component } from "react";
import "react-quill/dist/quill.snow.css";
import {
  Row,
  Col,
  Avatar,
  Alert,
  Spin,
  Icon,
  Popover,
  Modal,
  Select,
  message,
  Button,
  Rate,
  Tooltip
} from "antd";
import { withCookies } from "react-cookie";
import sanitizeHtml from "sanitize-html";
import Header from "./Header";
import Comment from "./Comment";
import { withRouter, Link } from "react-router-dom";
import "./Post.css";
import axios from "axios";
import moment from "moment";
import { map, sortBy, uniq, findIndex } from "lodash";
import Vote from "./Vote.jsx";
import Editor from "./Editor";
import tinycolor from "tinycolor2";
import ReactDOM from "react-dom";
import bookmark from "./res/bookmark.png";
import PopupLogin from "./PopupLogin";
import * as utils from "./utils/utils.js";
import TermsMini from "./TermsMini";
import { Helmet } from "react-helmet";

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      votes: {}
    };
    this.gotDps = false;
    this.myRef = React.createRef();
    this.answerIdRefs = [];
    this.answerUids = [];
    this.commentUids = [];
    this.goToLatestRef = null;
    this.doNotUpdate = false;
    this.answerUidRefs = [];
    this.rateRef = [];
    this.backUrl = "";
    window.addEventListener("scroll", this.scrollHandler);
  }

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    this.backUrl =
      process.env.NODE_ENV === "production"
        ? process.env.REACT_APP_API_URL
        : process.env.REACT_APP_API_URL_DEV;
    var findLatest = false;
    this.getPost(findLatest);
  }

  componentDidUpdate() {
    if (
      this.answerUidRefs.length > 0 &&
      this.answerUids.length > 0 &&
      !this.gotDps
    ) {
      this.gotDps = true;
      this.getAndSetDpUrlsAndPoints();
    }
    if (this.state.pathHasChanged) {
      this.setState({
        pathHasChanged: false
      });
      this.gotDps = false;
      this.answerUidRefs = [];
      this.answerUids = [];
      this.rateRef = [];
      this.getPost(false);
      window && window.scroll(0, 0);
      return;
    }
    if (
      this.state.qn &&
      this.state.ans &&
      this.state.qId &&
      this.state.categoryId &&
      !this.gotOtherQuestions
    ) {
      this.gotOtherQuestions = true;
      axios({
        url: this.backUrl + "getOtherQuestions",
        method: "POST",
        headers: {
          "Content-type": "text/plain"
        },
        data: {
          q_id: this.state.qId,
          category: this.getCategory(this.state.categoryId)
        }
      })
        .then(res => {
          if (res && res.data && res.data.success && res.data.data) {
            this.setState({
              otherQuestions: res.data.data
            });
          } else {
            this.setState({
              ...this.state
            });
          }
        })
        .catch(e => {
          this.setState({
            ...this.state
          });
        });
      axios({
        url: this.backUrl + "getOtherArticlesForQuestion",
        method: "POST",
        headers: {
          "Content-type": "text/plain"
        },
        data: {
          category: this.getCategory(this.state.categoryId),
          q_id: this.state.qId
        }
      })
        .then(res => {
          if (res && res.data && res.data.success && res.data.data) {
            this.setState({
              ...this.state,
              otherArticles: res.data.data
            });
          } else {
            this.setState({
              ...this.state
            });
          }
        })
        .catch(e => {
          this.setState({
            ...this.state
          });
        });
      if (this.state.qAuthor && this.state.qAuthor > 0) {
        axios({
          url: this.backUrl + "getPublicUserDetails",
          method: "POST",
          headers: {
            "Content-type": "text/plain",
            session_id: this.props.cookies.get("session_id")
          },
          data: {
            u_id: this.state.qAuthor
          }
        })
          .then(res => {
            if (res && res.data && res.data.success) {
              this.setState({
                otherPublicUserData: res.data.data
              });
            } else {
              if (res && res.data && res.data.msg) {
                this.setState({
                  otherPublicUserError: res.data.msg
                });
              } else {
                this.setState({
                  otherPublicUserError:
                    "Something went wrong while fetching other user details."
                });
              }
            }
          })
          .catch(e => {
            this.setState({
              otherPublicUserError:
                "Something went wrong while fetching other user details."
            });
          });
      }
    }
    var path = window.location.pathname;
    var answerIdToJumpTo = path.split("/")[4];
    if (
      this.props.jumpToAnswer &&
      this.state.postLoaded &&
      !this.state.jumpedToAnswer &&
      this.answerIdRefs &&
      this.answerIdRefs[answerIdToJumpTo]
    ) {
      this.setState({
        ...this.state,
        jumpedToAnswer: true
      });
      if (answerIdToJumpTo) {
        if (this.answerIdRefs[answerIdToJumpTo]) {
          var that = this;
          this.answerIdRefs[answerIdToJumpTo].scrollIntoView({
            block: "start",
            behavior: "smooth"
          });
          // setTimeout(() => {
          //   window.scrollBy(0, -35)
          // }, 0);
          if (this.answerIdRefs[answerIdToJumpTo].style) {
            this.answerIdRefs[answerIdToJumpTo].style.backgroundColor =
              this.state.dark === " dark" ? "#1890FF" : "#1890FF";
          }
          setTimeout(function() {
            if (
              answerIdToJumpTo &&
              that &&
              that.answerIdRefs &&
              that.answerIdRefs[answerIdToJumpTo] &&
              that.answerIdRefs[answerIdToJumpTo].style
            ) {
              that.answerIdRefs[answerIdToJumpTo].style.backgroundColor = null;
            }
          }, 2000);
        }
      }
    }
    if (this.goToLatestRef) {
      this.gotDps = false;
      var tempRef = this.goToLatestRef;
      this.getAndSetDpUrlsAndPoints();
      this.goToLatestRef.scrollIntoView({
        block: "start",
        behavior: "smooth"
      });
      this.goToLatestRef.style.backgroundColor =
        this.state.dark === " dark" ? "#1890FF" : "#1890FF";
      setTimeout(function() {
        if (tempRef && tempRef.style) {
          tempRef.style.backgroundColor = null;
          tempRef = null;
        }
      }, 2000);
      this.goToLatestRef = null;
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.location.pathname !== nextProps.location.pathname &&
      !this.doNotUpdate
    ) {
      this.commentUids = [];
      this.answerUids = [];
      this.answerIdRefs = [];
      this.answerUidRefs = [];
      this.gotOtherQuestions = false;
      this.setState({
        pathHasChanged: true,
        jumpedToAnswer: false,
        qn: null,
        ans: null,
        qTitle: null,
        qDescription: null,
        aId: null,
        qId: null,
        otherPublicUserData: null,
        error: null
      });
    }
    if (this.doNotUpdate) {
      this.doNotUpdate = false;
      this.gotOtherQuestions = false;
    }
  }

  getCommentUserDp = () => {
    return this.state.userDpDataForComments;
  };

  fallbackCopyFunction = text => {
    var node = document.createElement("textarea");
    var selection = document.getSelection();

    node.textContent = text;
    document.body.appendChild(node);

    selection.removeAllRanges();
    node.select();
    document.execCommand("copy");

    selection.removeAllRanges();
    document.body.removeChild(node);
  };

  getRateHtml = num => {
    let start = `<ul
      class="ant-rate ant-rate-disabled post-answer-user-rate"
      tabindex="-1"
      role="radiogroup"
    >`;
    let middleContent = `<li class="ant-rate-star ant-rate-star-full">
      <div
        role="radio"
        aria-checked="true"
        aria-posinset="1"
        aria-setsize="3"
        tabindex="0"
      >
        <div class="ant-rate-star-first">
          <i class="anticon anticon-star">
            <svg
              viewBox="64 64 896 896"
              class=""
              data-icon="star"
              width="1em"
              height="1em"
              fill="currentColor"
              aria-hidden="true"
            >
              <path d="M908.1 353.1l-253.9-36.9L540.7 86.1c-3.1-6.3-8.2-11.4-14.5-14.5-15.8-7.8-35-1.3-42.9 14.5L369.8 316.2l-253.9 36.9c-7 1-13.4 4.3-18.3 9.3a32.05 32.05 0 0 0 .6 45.3l183.7 179.1-43.4 252.9a31.95 31.95 0 0 0 46.4 33.7L512 754l227.1 119.4c6.2 3.3 13.4 4.4 20.3 3.2 17.4-3 29.1-19.5 26.1-36.9l-43.4-252.9 183.7-179.1c5-4.9 8.3-11.3 9.3-18.3 2.7-17.5-9.5-33.7-27-36.3z" />
            </svg>
          </i>
        </div>
        <div class="ant-rate-star-second">
          <i class="anticon anticon-star">
            <svg
              viewBox="64 64 896 896"
              class=""
              data-icon="star"
              width="1em"
              height="1em"
              fill="currentColor"
              aria-hidden="true"
            >
              <path d="M908.1 353.1l-253.9-36.9L540.7 86.1c-3.1-6.3-8.2-11.4-14.5-14.5-15.8-7.8-35-1.3-42.9 14.5L369.8 316.2l-253.9 36.9c-7 1-13.4 4.3-18.3 9.3a32.05 32.05 0 0 0 .6 45.3l183.7 179.1-43.4 252.9a31.95 31.95 0 0 0 46.4 33.7L512 754l227.1 119.4c6.2 3.3 13.4 4.4 20.3 3.2 17.4-3 29.1-19.5 26.1-36.9l-43.4-252.9 183.7-179.1c5-4.9 8.3-11.3 9.3-18.3 2.7-17.5-9.5-33.7-27-36.3z" />
            </svg>
          </i>
        </div>
      </div>
    </li>`;

    let middle = "";
    for (let i = 0; i < num; i++) {
      middle = middle + middleContent;
    }

    let last = `<ul
      class="ant-rate ant-rate-disabled post-answer-user-rate"
      tabindex="-1"
      role="radiogroup"
    ></ul>`;
    return start + middle + last;
  };

  getAndSetDpUrlsAndPoints = () => {
    axios({
      url: this.backUrl + "getUserDpUrlAndPoints",
      headers: {
        "Content-type": "text/plain"
      },
      method: "POST",
      data: {
        u_ids: uniq([...this.answerUids, ...this.commentUids])
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          var that = this;
          if (res.data.data.length > 0) {
            that.setState({
              userDpDataForComments: res.data.data
            });
            map(this.answerUidRefs, function(item, i) {
              let resI = findIndex(res.data.data, function(o) {
                return o.u_id === item[0];
              });
              if (resI > -1) {
                if (that.answerUidRefs[i][1] && res.data.data[resI].dp_url) {
                  ReactDOM.findDOMNode(that.answerUidRefs[i][1]).innerHTML =
                    "<img src=" + res.data.data[resI].dp_url + " class='mark'>";
                }
              }
            });
            map(this.rateRef, function(item, i) {
              let resI = findIndex(res.data.data, function(o) {
                return o.u_id === item[0];
              });
              if (resI > -1) {
                if (that.rateRef[i][1] && res.data.data[resI].points) {
                  ReactDOM.findDOMNode(
                    that.rateRef[i][1]
                  ).innerHTML = that.getRateHtml(
                    that.getNumStars(res.data.data[resI].points)
                  );
                }
              }
            });
          }
        } else {
          if (res && res.data && res.data.msg) {
          } else {
          }
        }
      })
      .catch(e => {});
  };

  deleteQuestion = qId => {
    var that = this;
    Modal.confirm({
      title:
        "Are you sure you want to delete this question? This action cannot be undone.",
      onOk() {
        axios({
          url: that.backUrl + "deleteQuestion",
          method: "POST",
          headers: {
            "Content-type": "text/plain",
            session_id: that.props.cookies.get("session_id")
          },
          data: {
            q_id: qId
          }
        })
          .then(res => {
            if (res && res.data && res.data.success) {
              message.success(res.data.msg);
              setTimeout(function() {
                that.props.history.push("/");
              }, 1500);
            } else {
              if (res && res.data && res.data.msg) {
                if (res.data.msg === "You need to login to continue.") {
                  message.warn(res.data.msg);
                  let cookiesObj = this.props.cookies.getAll();
                  for (var key in cookiesObj) {
                    this.props.cookies.remove(key, { path: "/" });
                  }
                } else {
                  message.error(res.data.msg);
                }
              } else {
                message.success(
                  "Something went wrong while deleting this question. Please try again later."
                );
              }
            }
          })
          .catch(e => {
            message.success(
              "Something went wrong while deleting this question. Please try again later."
            );
          });
      }
    });
  };

  deleteAnswer = aId => {
    var that = this;
    Modal.confirm({
      title:
        "Are you sure you want to delete this answer? This action cannot be undone.",
      onOk() {
        axios({
          url: that.backUrl + "deleteAnswer",
          method: "POST",
          headers: {
            "Content-type": "text/plain",
            session_id: that.props.cookies.get("session_id")
          },
          data: {
            a_id: aId
          }
        })
          .then(res => {
            if (res && res.data && res.data.success) {
              var cookieKey =
                window.location.pathname.split("/")[1] +
                ":" +
                window.location.pathname.split("/")[2];
              that.props.cookies.set(cookieKey, moment.now().toString(), {
                path: "/",
                expires: moment()
                  .add(610, "seconds")
                  .toDate()
              });
              message.success(res.data.msg);
              that.getPost(false);
            } else {
              if (res && res.data && res.data.msg) {
                if (res.data.msg === "You need to login to continue.") {
                  message.warn(res.data.msg);
                  let cookiesObj = that.props.cookies.getAll();
                  for (var key in cookiesObj) {
                    that.props.cookies.remove(key, { path: "/" });
                  }
                } else {
                  message.error(res.data.msg);
                }
              } else {
                message.error(
                  "Something went wrong while deleting this answer. Please try again later."
                );
              }
            }
          })
          .catch(e => {
            message.error(
              "Something went wrong while deleting this answer. Please try again later."
            );
          });
      }
    });
  };

  getPost(findLatest) {
    var data1;
    var qid = window.location.pathname.split("/")[2];
    let cookieKey =
      window.location.pathname.split("/")[1] +
      ":" +
      window.location.pathname.split("/")[2];
    let cacheOverrideNounce = this.props.cookies.get(cookieKey);
    let headers = {
      "Content-type": "text/plain"
    };
    if (cacheOverrideNounce && cacheOverrideNounce.length > 0) {
      headers["cache-override-nounce"] = cacheOverrideNounce;
    }
    if (qid) {
      var data = JSON.stringify({
        q_id: parseInt(qid)
      });
      axios({
        url: this.backUrl + "getPost",
        method: "POST",
        headers: headers,
        data: data
      })
        .then(res => {
          if (res && res.data && res.data.success && res.data.data) {
            if (
              window &&
              window.location &&
              window.location.pathname &&
              window.location.pathname.split("/")[3] === "null"
            ) {
              let safeTitle = res.data.data.question.title
                .split(" ")
                .join("-")
                .toLowerCase()
                .split("?")
                .join("-")
                .split("'")
                .join("");
              if (safeTitle[safeTitle.length - 1] === "-") {
                safeTitle = safeTitle.slice(0, safeTitle.length - 1);
              }
              this.doNotUpdate = true;
              this.props.history.replace(
                encodeURI(
                  "/post/" +
                    res.data.data.question.q_id +
                    "/" +
                    safeTitle +
                    "/" +
                    window.location.pathname.split("/")[4]
                )
              );
            }
            data1 = res.data.data;
            if (data1) {
              var question = data1.question;
              var answers = data1.answers;
              var createdUTC = moment(question.created).utc();
              var localTime = moment(createdUTC)
                .local()
                .format("MMMM Do YYYY, h:mm:ss a");
              var qn = (
                <Row>
                  <Col xs={22} sm={22} md={22} lg={22} xl={22} xxl={22}>
                    <div className="post-question-div">
                      <h2 className={"q-post-title" + this.state.dark}>
                        {question.title}
                      </h2>
                      <p className={"q-description" + this.state.dark}>
                        {question.description}
                      </p>
                      <div
                        onClick={() => {
                          this.props.history.push(
                            "/" + this.getCategory(question.category)
                          );
                        }}
                        className={"q-info-post" + this.state.dark}
                        style={{
                          cursor: "pointer"
                        }}
                      >
                        <span
                          style={{
                            backgroundColor: tinycolor(
                              this.categoryColorMap.get(
                                this.getCategory(question.category)
                              )
                            )
                              .darken(0)
                              .toHexString()
                          }}
                          className="cat-span"
                        />
                        {this.getCategory(question.category)}
                      </div>
                      <div className="vl" />
                      <div className={"q-info-created post" + this.state.dark}>
                        {localTime.toString()}
                      </div>
                      <div className="vl" />
                      <div
                        className={"author" + this.state.dark}
                        onClick={() => {
                          if (question.u_id) {
                            this.props.history.push("/user/" + question.u_id);
                          }
                        }}
                      >
                        {question.name || "Deleted User"}
                      </div>
                    </div>
                  </Col>
                  <Col
                    xs={2}
                    sm={2}
                    md={2}
                    lg={2}
                    xl={2}
                    xxl={2}
                    className="question-options-col"
                  >
                    <div className="question-options-div">
                      <Popover
                        // mouseLeaveDelay={1}
                        className="question-options-div-inner"
                        placement="bottom"
                        trigger="click"
                        content={
                          <div>
                            {question.u_id + "" ===
                            this.props.cookies.get("u_id") ? null : (
                              <div>
                                <Button
                                  style={{
                                    border: "none",
                                    width: "7em"
                                  }}
                                  onClick={() => {
                                    this.reportQuestion(question.q_id);
                                  }}
                                >
                                  Report
                                </Button>
                              </div>
                            )}
                            {question.u_id + "" ===
                              this.props.cookies.get("u_id") ||
                            this.props.cookies.get("role") > 0 ? (
                              <div>
                                <Button
                                  style={{
                                    border: "none",
                                    width: "7em"
                                  }}
                                  type="danger"
                                  onClick={() => {
                                    this.deleteQuestion(question.q_id);
                                  }}
                                >
                                  Delete
                                </Button>
                              </div>
                            ) : null}
                          </div>
                        }
                      >
                        <div
                          className={
                            "question-options-div-trigger" + this.state.dark
                          }
                        >
                          ...
                        </div>
                      </Popover>
                    </div>
                  </Col>
                </Row>
              );
              var ans;
              if (!answers || answers.length === 0) {
                ans = (
                  <div className="post-answer-div">
                    <div className={"q-not-answered" + this.state.dark}>
                      {"This question hasn't been answered yet."}
                    </div>
                  </div>
                );
              } else {
                var temp = {};
                var sortedAnswers = sortBy(answers, function(item) {
                  return item.down_votes - item.up_votes;
                });
                var that = this;
                var setLatestRef = false;
                that.commentUids = [];
                ans = map(sortedAnswers, function(answer, i) {
                  if (answer.comments && answer.comments.length > 0) {
                    map(answer.comments, c => {
                      c.u_id && that.commentUids.push(c.u_id);
                    });
                  }
                  that.answerUids.push(answer.u_id);
                  setLatestRef = false;
                  var createdUTC = moment(answer.created).utc();
                  var localTime = moment(createdUTC)
                    .local()
                    .format("MMMM Do YYYY, h:mm:ss a");
                  if (findLatest) {
                    if (answer.u_id + "" === that.props.cookies.get("u_id")) {
                      let nowUtc = moment(moment()).utc();
                      var duration = moment.duration(nowUtc.diff(createdUTC));
                      var secondsFromPost = duration.asSeconds();
                      if (secondsFromPost < 20) {
                        setLatestRef = true;
                      }
                    }
                  }

                  temp[i] = answer.up_votes - answer.down_votes;
                  return (
                    <Row
                      key={i}
                      className="post-answer-div-row"
                      style={{
                        borderColor: that.state.dark
                          ? tinycolor("#1890ff")
                              .lighten(0)
                              .toHexString()
                          : tinycolor("#1890ff")
                              .lighten(20)
                              .toHexString()
                      }}
                    >
                      <Col
                        xs={22}
                        sm={22}
                        md={22}
                        lg={22}
                        xl={22}
                        xxl={22}
                        className="post-answer-col"
                      >
                        <div
                          className="post-answer-div"
                          ref={
                            setLatestRef
                              ? ref => {
                                  that.goToLatestRef = ref;
                                  that.answerIdRefs[answer.a_id] = ref;
                                }
                              : ref => {
                                  that.answerIdRefs[answer.a_id] = ref;
                                }
                          }
                        >
                          <Avatar
                            ref={ref => {
                              that.answerUidRefs.push([answer.u_id, ref]);
                            }}
                            onClick={() => {
                              answer.u_id
                                ? that.props.history.push(
                                    "/user/" + answer.u_id
                                  )
                                : (() => {})();
                            }}
                            className="answer-avatar"
                            style={{
                              cursor: "pointer"
                            }}
                            icon="user"
                          />
                          <div
                            onClick={() => {
                              if (answer.u_id) {
                                that.props.history.push("/user/" + answer.u_id);
                              }
                            }}
                            ref={ref => {
                              that.answerIdRefs[answer.a_id] = ref;
                            }}
                            className={"answer-name" + that.state.dark}
                            style={{
                              display: "inline-block"
                            }}
                          >
                            {answer.name || "Deleted User"}
                          </div>
                          <div className="post-answer-user-rate-div">
                            <Rate
                              ref={ref => {
                                that.rateRef.push([answer.u_id, ref]);
                              }}
                              className="post-answer-user-rate"
                              count={0}
                              disabled
                              defaultValue={0}
                            />
                          </div>
                          <div className={"answer-created" + that.state.dark}>
                            {localTime.toString()}
                          </div>

                          {answer.qualification ? (
                            <div
                              style={{
                                borderColor: tinycolor("#1890ff")
                                  .lighten(20)
                                  .toHexString()
                              }}
                              className={
                                "answer-qualifcation" + that.state.dark
                              }
                            >
                              {answer.qualification}
                            </div>
                          ) : null}

                          <div
                            className={"answer-answer" + that.state.dark}
                            dangerouslySetInnerHTML={{
                              __html: sanitizeHtml(answer.answer, {
                                allowedTags: [
                                  "p",
                                  "strong",
                                  "em",
                                  "s",
                                  "blockquote",
                                  "ol",
                                  "ul",
                                  "li",
                                  "a",
                                  "img",
                                  "pre",
                                  "span",
                                  "u"
                                ],
                                allowedAttributes: {
                                  a: ["href", "rel", "target"],
                                  img: ["src", "rel"],
                                  pre: ["class", "spellcheck"],
                                  span: ["class"]
                                }
                              })
                            }}
                          />
                          <Comment
                            blur={that.enableMainBlur}
                            colorScheme={tinycolor("#1890ff")
                              .lighten(30)
                              .toHexString()}
                            aId={answer.a_id}
                            val={answer.comments}
                            uId={answer.u_id}
                            getCommentUserDp={that.getCommentUserDp}
                          />
                        </div>
                        <div className="answer-padding" />
                        <div className="" />
                      </Col>
                      <Col
                        xs={2}
                        sm={2}
                        md={2}
                        lg={2}
                        xl={2}
                        xxl={2}
                        className="vote-col"
                      >
                        <div className={"answer-options-div"}>
                          <Popover
                            // mouseLeaveDelay={21}
                            trigger="click"
                            placement="bottom"
                            content={
                              <div>
                                {answer.u_id + "" ===
                                that.props.cookies.get("u_id") ? null : (
                                  <div>
                                    <Button
                                      style={{
                                        border: "none",
                                        width: "7em"
                                      }}
                                      onClick={() => {
                                        that.reportAnswer(answer.a_id);
                                      }}
                                    >
                                      Report
                                    </Button>
                                  </div>
                                )}
                                <div>
                                  <Button
                                    style={{
                                      border: "none",
                                      width: "7em"
                                    }}
                                    onClick={() => {
                                      let fUrl =
                                        process.env.NODE_ENV === "production"
                                          ? process.env
                                              .REACT_APP_API_FRONTEND_URL
                                          : process.env
                                              .REACT_APP_API_FRONTEND_URL_DEV;
                                      let url = (
                                        fUrl +
                                        "post/" +
                                        answer.q_id +
                                        "/" +
                                        question.title
                                          .split(" ")
                                          .join("-")
                                          .toLowerCase()
                                      )
                                        .split("?")
                                        .join("-")
                                        .split("'")
                                        .join("");
                                      if (url[url.length - 1] === "-") {
                                        url = url.slice(0, url.length - 1);
                                      }
                                      if (navigator.clipboard) {
                                        navigator.clipboard
                                          .writeText(
                                            encodeURI(url + "/" + answer.a_id)
                                          )
                                          .then(() => {})
                                          .catch(e => {
                                            that.fallbackCopyFunction(
                                              encodeURI(url + "/" + answer.a_id)
                                            );
                                          });
                                      } else {
                                        that.fallbackCopyFunction(
                                          encodeURI(url + "/" + answer.a_id)
                                        );
                                      }
                                      message.success(
                                        "Link copied to clipboard."
                                      );
                                    }}
                                  >
                                    Copy Link
                                  </Button>
                                </div>
                                {answer.u_id + "" ===
                                  that.props.cookies.get("u_id") ||
                                that.props.cookies.get("role") > 0 ? (
                                  <div>
                                    <Button
                                      style={{
                                        border: "none",
                                        width: "7em"
                                      }}
                                      type="danger"
                                      onClick={() => {
                                        that.deleteAnswer(answer.a_id);
                                      }}
                                    >
                                      Delete
                                    </Button>
                                  </div>
                                ) : null}
                              </div>
                            }
                          >
                            <div
                              className={
                                "answer-options-trigger" + that.state.dark
                              }
                            >
                              ...
                            </div>
                          </Popover>
                        </div>
                        <div className="vote-div">
                          <Vote
                            blur={that.enableMainBlur}
                            uId={that.props.cookies.get("u_id")}
                            aUid={answer.u_id}
                            aId={answer.a_id}
                            voteCount={answer.up_votes - answer.down_votes}
                          />
                        </div>
                      </Col>
                    </Row>
                  );
                });
              }
            }
            this.setState({
              qClosed: question.closed,
              qAuthor: question.u_id,
              qId: question.q_id,
              qTitle: question.title,
              qDescription: question.description,
              ans: ans,
              qn: qn,
              votes: temp,
              activity: question.activity,
              categoryId: question.category,
              catColor: this.categoryColorMap.get(
                this.getCategory(question.category)
              ),
              catDarkColor: tinycolor(
                this.categoryColorMap.get(this.getCategory(question.category))
              )
                .darken(0)
                .toHexString(),
              postLoaded: true
            });
          } else {
            if (res && res.data && res.data.msg) {
              if (res.data.msg === "You need to login to continue.") {
                let cookiesObj = this.props.cookies.getAll();
                for (var key in cookiesObj) {
                  this.props.cookies.remove(key, { path: "/" });
                }
              }
              this.setState({
                error: res.data.msg
              });
              return;
            } else {
              this.setState({
                error: "Something went wrong while fetching this post."
              });
              return;
            }
          }
        })
        .catch(e => {
          this.setState({
            ...this.state,
            error: "Something went wrong while fetching this post."
          });
          return;
        });
    } else {
      this.setState({
        ...this.state,
        error: "Something went wrong while fetching this post."
      });
      return;
    }
  }

  categoryColorMap = utils.categoryColorMap;

  getCategory = catNum => {
    var categoryList = utils.categoryList;
    return categoryList[catNum];
  };

  updateVoteCount = (i, a_id, type) => {
    this[i].callUpdate();
  };

  updatePostAndGotoNewPost() {
    var cookieKey =
      window.location.pathname.split("/")[1] +
      ":" +
      window.location.pathname.split("/")[2];
    this.props.cookies.set(cookieKey, moment.now().toString(), {
      path: "/",
      expires: moment()
        .add(610, "seconds")
        .toDate()
    });
    var findLatest = true;
    this.setState({
      qn: null,
      ans: null
    });
    this.answerUidRefs = [];
    this.answerUids = [];
    this.rateRef = [];
    this.getPost(findLatest);
  }

  reportQuestion(q_id) {
    if (this.props.cookies.get("session_id")) {
      this.setState({
        ...this.state,
        viewReportModal: true,
        qId: q_id
      });
    } else {
      message.warn("You need to login to continue.");
    }
  }

  handleReportCancel() {
    this.setState({
      viewReportModal: false,
      reportCategory: null,
      reportLoading: false
    });
  }

  handleReportCategoryChange(val) {
    this.setState({
      reportCategory: val
    });
  }

  handleReportOk() {
    this.setState({
      reportLoading: true
    });
    axios({
      url: this.backUrl + "reportPost",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id")
      },
      data: {
        qac_id: this.state.qId,
        r_type: 1,
        report_category: this.state.reportCategory
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            ...this.state,
            viewReportModal: false,
            reportCategory: null,
            reportLoading: false
          });
          message.success(res.data.msg);
        } else {
          if (res && res.data && res.data.msg) {
            if (res.data.msg === "You need to login to continue.") {
              let cookiesObj = this.props.cookies.getAll();
              for (var key in cookiesObj) {
                this.props.cookies.remove(key, { path: "/" });
              }
            }
            this.setState({
              ...this.state,
              reportLoading: false,
              reportError: res.data.msg
            });
          } else {
            this.setState({
              ...this.state,
              reportLoading: false,
              reportError:
                "Something went wrong while reporting this question. Please try again later."
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          ...this.state,
          reportLoading: false,
          reportError:
            "Something went wrong while reporting this question. Please try again later."
        });
      });
  }

  reportAnswer(a_id) {
    if (this.props.cookies.get("session_id")) {
      this.setState({
        viewAnswerReportModal: true,
        aId: a_id
      });
    } else {
      message.warn("You need to login to continue.");
    }
  }

  handleAnswerReportCancel() {
    this.setState({
      ...this.state,
      viewAnswerReportModal: false,
      reportAnswerCategory: null,
      reportAnswerLoading: false,
      aId: null
    });
  }

  handleReportAnswerCategoryChange(val) {
    this.setState({
      ...this.state,
      reportAnswerCategory: val
    });
  }

  handleAnswerReportOk() {
    this.setState({
      ...this.state,
      reportAnswerLoading: true
    });
    axios({
      url: this.backUrl + "reportPost",
      method: "POST",
      headers: {
        "Content-type": "text/plain",
        session_id: this.props.cookies.get("session_id")
      },
      data: {
        qac_id: this.state.aId,
        r_type: 2,
        report_category: this.state.reportAnswerCategory
      }
    })
      .then(res => {
        if (res && res.data && res.data.success) {
          this.setState({
            ...this.state,
            viewAnswerReportModal: false,
            reportAnswerCategory: null,
            reportAnswerLoading: false,
            aId: null
          });
          message.success(res.data.msg);
        } else {
          if (res && res.data && res.data.msg) {
            if (res.data.msg === "You need to login to continue.") {
              let cookiesObj = this.props.cookies.getAll();
              for (var key in cookiesObj) {
                this.props.cookies.remove(key, { path: "/" });
              }
            }
            this.setState({
              ...this.state,
              reportAnswerLoading: false,
              reportAnswerError: res.data.msg,
              aId: null
            });
          } else {
            this.setState({
              ...this.state,
              reportAnswerLoading: false,
              reportAnswerError:
                "Something went wrong while reporting this question. Please try again later.",
              aId: null
            });
          }
        }
      })
      .catch(e => {
        this.setState({
          ...this.state,
          reportAnswerLoading: false,
          reportAnswerError:
            "Something went wrong while reporting this question. Please try again later."
        });
      });
  }

  getNumStars = points => {
    if (points > 999999) {
      return 5;
    } else if (points > 99999) {
      return 4;
    } else if (points > 9999) {
      return 3;
    } else if (points > 999) {
      return 2;
    } else if (points > 99) {
      return 1;
    } else {
      return 0;
    }
  };

  scrollHandler = () => {
    if (this.otherQuestionRef && this.postRowColRef) {
      if (!this.otherOffsetTop) {
        this.otherOffsetTop = this.otherQuestionRef.offsetTop;
      }
      if (
        this.otherQuestionRef.clientHeight <=
        window.innerHeight - this.otherOffsetTop
      ) {
        // if(!this.setFixed) {
        this.setFixed = true;
        let tempWidth = this.otherQuestionRef.clientWidth;
        this.otherQuestionRef.style.position = "fixed";
        this.otherQuestionRef.style.width =
          "calc(" + this.otherQuestionsColRef.clientWidth + "px - 0em" + ")";
        this.otherQuestionRef.style.minWidth = 0;
        // }
      } else if (
        this.postRowColRef.clientHeight - this.postRowColRef.offsetTop >
          window.innerHeight &&
        this.postRowColRef.clientHeight > this.otherQuestionRef.clientHeight &&
        window.scrollY >=
          this.otherQuestionRef.clientHeight -
            window.innerHeight +
            this.otherOffsetTop
      ) {
        this.stageOneOther = true;
        this.stateTwoOther = false;
        let tempWidth = this.otherQuestionRef.clientWidth;
        this.otherQuestionRef.style.position = "fixed";
        this.otherQuestionRef.style.bottom = 0;
        this.otherQuestionRef.style.width =
          "calc(" + this.otherQuestionsColRef.clientWidth + "px - 0em" + ")";
        this.otherQuestionRef.style.minWidth = 0;
      } else if (
        this.postRowColRef.clientHeight - this.postRowColRef.offsetTop >
          window.innerHeight &&
        this.postRowColRef.clientHeight > this.otherQuestionRef.clientHeight &&
        window.scrollY <
          this.otherQuestionRef.clientHeight -
            window.innerHeight +
            this.otherOffsetTop
      ) {
        this.stageOneOther = false;
        this.stateTwoOther = true;
        this.otherQuestionRef.style.position = "relative";
      }
    }
    let scrollY = window.scrollY;
    // if (scrollY > 400) {
    //   if (!this.showTopScroll) {
    //     if (this.goToTopRef && this.goToTopRef.style) {
    //       this.showTopScroll = true;
    //       this.goToTopRef.style.visibility = "visible";
    //       this.goToTopRef.style.display = "block";
    //     }
    //   }
    // } else if (scrollY <= 400) {
    //   if (this.showTopScroll) {
    //     if (this.goToTopRef && this.goToTopRef.style) {
    //       this.showTopScroll = false;
    //       this.goToTopRef.style.visibility = "hidden";
    //       this.goToTopRef.style.display = "none";
    //     }
    //   }
    // }
  };

  enableMainBlur = v => {
    if (v) {
      this.setState({ mainBlur: true });
    } else {
      this.setState({ mainBlur: false });
    }
  };

  addBookmark = () => {
    if (!this.props.cookies.get("session_id")) {
      this.setState({
        loginModalVisible: true,
        mainBlur: true
      });
      return;
    }
    if (this.state.qId) {
      axios({
        url: this.backUrl + "addBookmark",
        method: "POST",
        headers: {
          "Content-type": "text/plain",
          session_id: this.props.cookies.get("session_id")
        },
        data: {
          qa_id: this.state.qId,
          type: 1
        }
      }).then(res => {
        if (res && res.data && res.data.success) {
          message.success(res.data.msg);
        } else {
          if (res && res.data && res.data.msg) {
            message.error(res.data.msg);
          } else {
            message.error(
              "Something went wrong while adding bookmark. Please try again later."
            );
          }
        }
      });
    }
  };

  render() {
    let otherUserData;
    var qn;
    var ans;
    var postPoints;
    if (this.state.activity) {
      let t_points = this.state.activity;
      if (t_points > 999999) {
        let point_string = parseInt(t_points / 1000000);

        postPoints = point_string + "M";
      } else if (t_points > 999) {
        let point_string = parseInt(t_points / 1000);

        postPoints = point_string + "K";
      } else {
        postPoints = t_points;
      }
    }
    if (this.state.error) {
      qn = (
        <Alert type="error" message={this.state.error}>
          {this.state.error}
        </Alert>
      );
    } else if (this.state.qn && this.state.ans) {
      qn = this.state.qn;
      ans = this.state.ans;
    } else {
      qn = (
        <center>
          <Spin
            indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
          />
        </center>
      );
    }
    var editor;
    if (!this.state.error && this.state.qId && this.state.qn) {
      editor = (
        <Editor
          blur={this.enableMainBlur}
          closed={this.state.qClosed}
          uId={this.state.qAuthor}
          qId={this.state.qId}
          colorScheme={"#1890ff"}
          updatePostAndGotoNewPost={this.updatePostAndGotoNewPost.bind(this)}
        />
      );
    }
    var otherQns;
    if (this.state.otherQuestions) {
      otherQns = map(this.state.otherQuestions, function(item, i) {
        let safeTitle = item.title
          .split(" ")
          .join("-")
          .toLowerCase()
          .split("?")
          .join("-")
          .split("'")
          .join("");
        if (safeTitle[safeTitle.length - 1] === "-") {
          safeTitle = safeTitle.slice(0, safeTitle.length - 1);
        }
        let t_points = item.activity,
          points;
        if (t_points > 999999) {
          let point_string = parseInt(t_points / 1000000);

          points = point_string + "M";
        } else if (t_points > 999) {
          let point_string = parseInt(t_points / 1000);

          points = point_string + "K";
        } else {
          points = t_points;
        }
        let pointColorClass = "one";
        if (t_points > 99) {
          pointColorClass = "two";
        }
        if (t_points > 999) {
          pointColorClass = "three";
        }
        return (
          <div className="other-question-div" key={i}>
            <div style={{ lineHeight: "1.2" }}>
              <div
                className={"article-other-article-points " + pointColorClass}
              >
                {points}
              </div>
              <Link to={encodeURI("/post/" + item.q_id + "/" + safeTitle)}>
                {item.title}
              </Link>
            </div>
          </div>
        );
      });
    }
    if (this.state.otherQuestions && this.state.otherQuestions.length < 1) {
      otherQns = (
        <div className="article-other-user-data-div">
          <div className={"no-articles" + this.state.dark}>
            No other questions
          </div>
        </div>
      );
    }
    let otherArticles;
    if (this.state.otherArticles && this.state.otherArticles.length > 0) {
      otherArticles = map(this.state.otherArticles, function(item, i) {
        let safeTitle = item.title
          .split(" ")
          .join("-")
          .toLowerCase()
          .split("?")
          .join("-")
          .split("'")
          .join("");
        if (safeTitle[safeTitle.length - 1] === "-") {
          safeTitle = safeTitle.slice(0, safeTitle.length - 1);
        }
        let t_points = item.points,
          points;
        if (t_points > 999999) {
          let point_string = parseInt(t_points / 1000000);

          points = point_string + "M";
        } else if (t_points > 999) {
          let point_string = parseInt(t_points / 1000);

          points = point_string + "K";
        } else {
          points = t_points;
        }
        let pointColorClass = "one";
        if (t_points > 99) {
          pointColorClass = "two";
        }
        if (t_points > 999) {
          pointColorClass = "three";
        }
        return (
          <div className="other-question-div" key={i}>
            <div style={{ lineHeight: "1.2" }}>
              <div
                className={"article-other-article-points " + pointColorClass}
              >
                {points}
              </div>
              <Link to={encodeURI("/article/" + item.a_id + "/" + safeTitle)}>
                {item.title}
              </Link>
            </div>
          </div>
        );
      });
    }
    if (this.state.otherArticles && this.state.otherArticles.length < 1) {
      otherArticles = (
        <div className="article-other-user-data-div">
          <div className={"no-articles" + this.state.dark}>No articles</div>
        </div>
      );
    }

    if (this.state.otherPublicUserData) {
      let t_points = this.state.otherPublicUserData.points,
        points;
      if (t_points > 999999) {
        let point_string = ((1.0 * t_points) / 1000000.0).toFixed(3);
        let seperateVals = point_string.split(".");
        if (seperateVals[1]) {
          points = seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "M";
        } else {
          points = seperateVals[0] + "M";
        }
      } else if (t_points > 999) {
        let point_string = ((1.0 * t_points) / 1000.0).toFixed(3);
        let seperateVals = point_string.split(".");
        if (seperateVals[1]) {
          points = seperateVals[0] + "." + seperateVals[1].slice(0, 2) + "K";
        } else {
          points = seperateVals[0] + "K";
        }
      } else {
        points = t_points;
      }

      let name = this.state.otherPublicUserData.fname;
      if (this.state.otherPublicUserData.lname) {
        name += " " + this.state.otherPublicUserData.lname;
      }
      let tier;
      if (t_points > 999999) {
        tier = 5;
      } else if (t_points > 99999) {
        tier = 4;
      } else if (t_points > 9999) {
        tier = 3;
      } else if (t_points > 999) {
        tier = 2;
      } else if (t_points > 99) {
        tier = 1;
      } else {
        tier = 0;
      }
      otherUserData = (
        <div>
          <h4 className={"other-question-heading" + this.state.dark}>
            Asked by <span style={{ textTransform: "capitalize" }}>{name}</span>
            {/* {(this.state.qAuthor+"" === this.props.cookies.get("u_id"))?null:this.state.otherPublicUserData.following?
            <div className="follow-user-div unfollow" onClick={()=> {this.unfollowUser(this.state.qAuthor)}}>
            <Button className="user-add-button" icon="user-delete">
            <div className="user-add-text">Unfollow</div>
            </Button></div>:
            <div className="follow-user-div" onClick={()=> {this.followUser(this.state.qAuthor)}} >
            <Button className="user-add-button" icon="user-add">
            <div className="user-add-text">Follow</div>
            </Button></div>} */}
          </h4>
          <div className="article-other-user-data-div">
            <div
              className="article-dp"
              onClick={() => {
                if (this.state.qAuthor) {
                  this.props.history.push("/user/" + this.state.qAuthor);
                }
              }}
            >
              {this.state.otherPublicUserData.dp_url ? (
                <Avatar size={64} src={this.state.otherPublicUserData.dp_url} />
              ) : (
                <Avatar size={64} icon="user" />
              )}
            </div>
            <div className="article-user-points">{points}</div>
            <Rate count={tier} disabled defaultValue={tier} />
          </div>
        </div>
      );
    }

    let rightBar =
      this.state.qn && this.state.qId ? (
        <div className="post-bookmark-div">
          <div className="article-points">{postPoints || 0}</div>
          <Tooltip title="Save for later" placement="left">
            <img
              onClick={this.addBookmark}
              className={"bookmark-icon-post" + this.state.dark}
              src={bookmark}
              alt="bookmark"
            />
          </Tooltip>
        </div>
      ) : null;
    return (
      <div
        className={"post-main-div" + this.state.dark}
        style={{
          backgroundColor:
            this.state.dark === " dark"
              ? "black"
              : tinycolor("#1890ff")
                  .lighten(35)
                  .toHexString()
        }}
      >
        <Helmet>
          <meta charSet="utf-8" />
          <title>{this.state.qTitle || "Clevereply"}</title>
          {this.state.qTitle?<meta property="og:title" content={this.state.qTitle} />:null}
          <meta name="description" content={this.state.qDescription} />
          <meta name="og:description" content={this.state.qDescription} />
          {this.state.error &&
          this.state.error === "This question doesn't exist." ? (
            <meta name="robots" content="noindex" />
          ) : null}
        </Helmet>
        <Modal //report modal
          wrapClassName={"report-question-modal" + this.state.dark}
          style={{ zIndex: "999" }}
          title="Report Question"
          onOk={this.handleReportOk.bind(this)}
          visible={this.state.viewReportModal}
          onCancel={this.handleReportCancel.bind(this)}
          okButtonProps={{
            disabled: this.state.reportCategory ? false : true,
            loading: this.state.reportLoading
            // onClick: () => {
            //   this.handleReportOk();
            // }
          }}
        >
          <div className="report-div">
            <div>What are you reporting?</div>
            <Select
              defaultValue={this.state.reportCategory}
              value={this.state.reportCategory}
              className="report-selector"
              onChange={this.handleReportCategoryChange.bind(this)}
            >
              <Select.Option value="Hate Speech">Hate Speech</Select.Option>
              <Select.Option value="Political Propaganda">
                Political Propaganda
              </Select.Option>
              <Select.Option value="Harmful Intent">
                Harmful Intent
              </Select.Option>
              <Select.Option value="Sexually Explicit Content">
                Sexually Explicit Content
              </Select.Option>
              <Select.Option value="Advertising/Spam">
                Advertising/Spam
              </Select.Option>
              <Select.Option value="Misleading">Misleading</Select.Option>
              <Select.Option value="Content unrelated to question">
                Content unrelated to question
              </Select.Option>
              <Select.Option value="Wrong Category">
                Wrong Category
              </Select.Option>
            </Select>
          </div>
          {this.state.reportError ? (
            <Alert
              style={{ marginTop: "1em" }}
              type="error"
              message={this.state.reportError}
            />
          ) : null}
        </Modal>
        <Modal //report modal for answer
          wrapClassName={"report-answer-modal" + this.state.dark}
          style={{ zIndex: "999" }}
          title="Report Answer"
          onOk={this.handleAnswerReportOk.bind(this)}
          visible={this.state.viewAnswerReportModal}
          onCancel={this.handleAnswerReportCancel.bind(this)}
          okButtonProps={{
            disabled: this.state.reportAnswerCategory ? false : true,
            loading: this.state.reportAnswerLoading
            // onClick: () => {
            //   this.handleReportOk();
            // }
          }}
        >
          <div className="report-div">
            <div>What are you reporting?</div>
            <Select
              defaultValue={this.state.reportAnswerCategory}
              value={this.state.reportAnswerCategory}
              className="report-selector"
              onChange={this.handleReportAnswerCategoryChange.bind(this)}
            >
              <Select.Option value="Hate Speech">Hate Speech</Select.Option>
              <Select.Option value="Political Propaganda">
                Political Propaganda
              </Select.Option>
              <Select.Option value="Harmful Intent">
                Harmful Intent
              </Select.Option>
              <Select.Option value="Sexually Explicit Content">
                Sexually Explicit Content
              </Select.Option>
              <Select.Option value="Advertising/Spam">
                Advertising/Spam
              </Select.Option>
              <Select.Option value="Misleading">Misleading</Select.Option>
              <Select.Option value="Content unrelated to question">
                Content unrelated to question
              </Select.Option>
              <Select.Option value="Wrong Category">
                Wrong Category
              </Select.Option>
            </Select>
          </div>
          {this.state.reportAnswerError ? (
            <Alert
              style={{ marginTop: "1em" }}
              type="error"
              message={this.state.reportAnswerError}
            />
          ) : null}
        </Modal>
        <Row
          style={{
            backgroundColor:
              this.state.dark === " dark"
                ? "black"
                : tinycolor("#1890ff")
                    .lighten(35)
                    .toHexString()
          }}
        >
          <Col>
            <Header blur={this.enableMainBlur} />
          </Col>
        </Row>
        <Row
          type="flex"
          style={{
            backgroundColor:
              this.state.dark === " dark"
                ? "black"
                : tinycolor("#1890ff")
                    .lighten(35)
                    .toHexString()
          }}
          className={"post-main-row" + this.state.dark}
        >
          <Col
            className="other-questions-col"
            xs={{ span: 24, order: 3 }}
            sm={{ span: 24, order: 3 }}
            md={{ span: 24, order: 3 }}
            lg={{ span: 6, order: 3 }}
            xl={6}
            xxl={8}
          >
            <div
              ref={ref => (this.otherQuestionsColRef = ref)}
              style={{ height: "100%" }}
            >
              {this.state.otherQuestions ? (
                <div
                  ref={ref => {
                    this.otherQuestionRef = ref;
                  }}
                  className={"other-questions-div" + this.state.dark}
                >
                  <div
                    className={"other-questions-inner-div" + this.state.dark}
                  >
                    <div>
                      {otherUserData}
                      <h4
                        className={"other-question-heading" + this.state.dark}
                      >
                        Other Questions from{" "}
                        {this.state.categoryId &&
                          this.getCategory(this.state.categoryId)}
                      </h4>
                    </div>
                    {otherQns}
                    <div>
                      <h4
                        className={"other-question-heading" + this.state.dark}
                      >
                        Articles from{" "}
                        <span className={"article-user-name" + this.state.dark}>
                          {this.state.categoryId &&
                            this.getCategory(this.state.categoryId)}
                        </span>
                      </h4>
                    </div>
                    {otherArticles}
                  </div>
                  <TermsMini />
                </div>
              ) : null}
            </div>
          </Col>
          <Col
            xs={{ span: 24, order: 2 }}
            sm={{ span: 24, order: 2 }}
            md={{ span: 24, order: 2 }}
            lg={{ span: 16, order: 2 }}
            xl={15}
            xxl={12}
          >
            <Col span={24}>
              <div className="post-type-row-col" />
            </Col>
            <div
              ref={ref => {
                this.postRowColRef = ref;
              }}
              className={"post-row-col" + this.state.dark}
            >
              <div>{qn}</div>
              <div>
                {editor}
                {this.state.qn && this.state.qId ? (
                  <Tooltip title="Save for later" placement="left">
                    <img
                      onClick={this.addBookmark}
                      className={"bookmark-icon-mobile" + this.state.dark}
                      src={bookmark}
                      alt="bookmark"
                    />
                  </Tooltip>
                ) : null}
              </div>
              {this.state.qn ? (
                <div
                  style={{
                    borderBottomWidth: "4px",
                    borderBottomStyle: "solid",
                    borderBottomColor: "#1890ff"
                      ? this.state.dark === " dark"
                        ? tinycolor("#1890ff")
                            .darken(0)
                            .toHexString()
                        : tinycolor("#1890ff")
                            .lighten(20)
                            .toHexString()
                      : null
                  }}
                  className="qn-ans-seperator"
                />
              ) : null}
              <div className="post-answer-list">{ans}</div>
            </div>
          </Col>
          <Col xs={0} sm={0} md={0} lg={2} xl={3} xxl={4} order={1}>
            {rightBar}
          </Col>
        </Row>
        <Row
          className={"empty-space" + this.state.dark}
          style={{
            backgroundColor:
              this.state.dark === " dark"
                ? "black"
                : tinycolor("#1890ff")
                    .lighten(35)
                    .toHexString()
          }}
        >
          <Col>
            <div className="empty-space" />
          </Col>
        </Row>
        <Modal
          visible={this.state.loginModalVisible}
          footer={null}
          onCancel={() =>
            this.setState({ loginModalVisible: false, mainBlur: false })
          }
        >
          <PopupLogin login />
        </Modal>
      </div>
    );
  }
}

export default withRouter(withCookies(Post));
