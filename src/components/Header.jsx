import React, { Component } from "react";
import { Row, Col, Modal } from "antd";
import ProfileInfo from "./ProfileInfo";
import Search from "./Search";
import { withRouter } from "react-router-dom";
import "./Header.css";
import { withCookies } from "react-cookie";
import logo from "./res/logo.png"

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  gotoHome = () => {
    this.props.history.push("/");
  };

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
  }

  render() {
    let classN;
    if (this.props.translucent) {
      classN = "header-main-div trans";
    } else {
      classN = "header-main-div"+this.state.dark
    }
    return (
      <div className={classN}>
        <Row type="flex" justify="space-between">
          <Col xs={4} sm={4} md={4} lg={4} xl={8} className="top-row-logo-col">
            <div
            className="logo-div"
              onClick={this.gotoHome}
              style={{ cursor: "pointer", width: "5em" }}
            >
              <img className="logo" src={logo}/>
            </div>
          </Col>
          <Col xs={2} sm={6} md={6} lg={6} xl={8} className="notification-col">
            <div className="notification-div">
              <Search />
            </div>
          </Col>
          <Col
            xs={18}
            sm={14}
            md={14}
            lg={14}
            xl={8}
            className="top-row-profile-col"
          >
            <ProfileInfo blur={this.props.blur}/>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(withCookies(Header));
