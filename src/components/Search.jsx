import React, { Component } from "react";
import { Row, Col, Input, Button, Icon } from "antd";
import "./Search.css";
import { withCookies } from "react-cookie";
const Search = Input.Search;


class Notif extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disabled: true
    };
  }

  componentDidMount() {
    this.setState({
      dark: this.props.cookies.get("dark") === "true" ? " dark" : ""
    });
    const myCallback = function() {
      if (document.readyState === 'complete') {
        window.google.search.cse.element.render(
          {
            div: 'g-search-box',
            tag: 'search',
            enableAutoComplete: true
          });
      } else {
        window.google.setOnLoadCallback(() => {
          window.google.search.cse.element.render(
            {
              div: 'g-search-box',
              tag: 'search',
              enableAutoComplete: true
            });
        }, true);
      }
    };

    window.__gcse = {
      parsetags: 'explicit',
      callback: myCallback,
    };

    (function() {
      const cx = '013188363743767810735:eod1yscx7mq';
      const gcse = document.createElement('script');
      gcse.type = 'text/javascript';
      gcse.async = true;
      gcse.src = 'https://cse.google.com/cse.js?cx='+cx;
      const s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(gcse, s);
    }());
  }

  render() {
    return (
      <Row>
        <Col span={24}>
          <div 
            className={"search-icon-div"+this.state.dark}
            style={this.state.mobileSearchVisible?{display:"block"}:{}}
          >
            {/* <Search
              placeholder="Search"
              onSearch={value => console.log(value)}
              name="n-search"
              autocomplete="false"
              disabled={this.state.disabled}
            /> */}
            
            <div id="g-search-box" className={this.state.mobileSearchVisible?"g-search-box-mobile":""}></div>
          </div>
          <div className="search-icon-div-min" style={this.state.mobileSearchVisible?{display: "none"}:{}} onClick={()=>{this.setState({mobileSearchVisible: true})}}>
            <span className="search-span">
              <Icon className="search-min-icon" type="search"/>
            </span>
          </div>
          <div className="mobile-search-closer" onClick={() => {this.setState({ mobileSearchVisible: false})}} style={{display:this.state.mobileSearchVisible?"block":"none"}}><Icon type="close" /></div>
        </Col>
      </Row>
    );
  }
}

export default withCookies(Notif);
